<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    //UsersManagement
        'UsersManagement'   => 'User Management',
        'AddUser'           => 'Users',
        'addrole'           => 'Roles ',
        'addpermissions'    => 'Permissions',
        'created_at'        => 'Created at',
        'updated_at'        => 'Updated at',
        'created_by'        => 'Created By',
        'updated_by'        => 'Updated By',
        'name'              => 'Name',
        'id'                => 'ID',
        'email'             => 'Email',
        'password'          => 'Password',
        'active'            => 'Status',
        'Pconfirmation'     => 'Password Confirmation',
        'Roles_List'        => 'Role',
        'Permissions_List'  => 'Permissions',
        'slug'              => 'Slug',
        'description'       => 'Description',
        'level'             => 'level',
        'permissions'       => 'Permissions',
        'model'             => 'model',

    //headcount
        'Project'           => 'Project',
        'score_sheet'       => 'Score Sheet',
        'location_id'       => 'Location',
        'location'          => 'Location',
        'score_sheet_id'    => 'Score Sheet',
        'Projects_List'     => 'Projects',
        'Managers_List'     => 'Managers',
        'Supervisors_List'  => 'Supervisors',
        'Teamleaders_List'  => 'Teamleaders',
        'Seniors_List'      => 'Seniors',
        'agent_type'        =>'Agent Type',
    //Seeting
        'Department'        => 'Department',
        'Attributeoptions_List' => 'Options',
        'Attributes_List'   => 'Attributes',
        'Sections_List'     => 'Sections',
        'Categories_List'   => 'Categories',
        'sort'              => 'Sort',
        'Deduction'         => 'Deduction',


    //employee
    'project_id'        => 'Project',
    'department_id'     => 'Department',
    'title_id'          => 'Title',
    'project_id'        => 'Project',
    'manager_id'        => 'Manager',
    'manager'           => 'Manager',
    'id_staff'          => 'Staff Id',
    'crm_user'          => 'Crm User',
    'idPbx'             => 'Pbx Id',
    'windows_account'   => 'NT Login',

    //ScoreSheet
    'ScoreSheet'        => 'Score Sheet',
    //Category
    'Category'          => 'Category',
    'Category_List'     => 'Category',
    //Section
    'Section'           => 'Section',
    'category_id'       => 'Category',
    'rank'              => 'Rank',
    'Attribute_List'    => 'Attribute',
    //Attribute
    'Attribute'         => 'Attribute',
    'section_id'        => 'Section',
    'Attributeoption_List' => 'Attribute Option',
    //AttributeOption
    'AttributeOption'   => 'Attribute Option',
    'attribute_id'      => 'Attribute',
    'score'             => 'Score',
    //Monitor
    'Monitor'           => 'Monitor',
    'supervisor_id'     => 'Supervisor',
    'teamleader'        => 'Team Leader',
    'agent'             => 'Agent',
    'monitoring_type'   => 'Monitoring Type',
    'observe_type'      => 'Observe Type',
    'call_type'         => 'Call Type',
    'customer_name'     => 'Customer Name',
    'phone_number'      => 'Phone Number',
    'call_duration'     => 'Call Duration',
    'call_nature'       => 'Call Nature',
    'hold_duration'     => 'Hold Duration',
    'call_date'         => 'Call Date',
    'call_time'         => 'Call Time',
    'fcr_reason'        => 'FCR Reason',
    'fcr'               => 'FCR',
    'senior'            => 'Senior',
    'Globalfail_count'  => 'Globalfail',
    'customer_satisfaction' => 'Customer SATISF.',
    'note'                  => 'note',
    'nce'                   => 'NCE',
    'bce'                   => 'BCE',
    'ece'                   => 'ECE',
    'total_score'           => 'Score',
    'admin'                 => 'Admin',
    'agent_id'              => 'Agent',
    'senior_id'             => 'Senior',
    'teamleader_id'         => 'TeamLeader',
    'Coaching'              => 'Coaching',
    'result'                => 'Result',
    'Coaching_count'        => 'Coachingfail',
    'globalfail'            => 'Global Fail',
    'deduction_id'          => 'Deduction',
    'observe_type_Modelrelationship' => 'Observe Type',
    'call_type_Modelrelationship'   => 'Call Type',
    'call_nature_Modelrelationship'=> 'Call Nature',
    'bce_Modelrelationship'=> 'BCE',
    'ece_Modelrelationship'=> 'ECE',
    'nce_Modelrelationship'=> 'NCE',
    'total_score_Modelrelationship'=> 'Total Score',
    'result_Modelrelationship'=> 'Result',
    
];

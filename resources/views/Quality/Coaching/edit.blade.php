@extends('layouts.app')
@section('content')
    <br/>
    <table class="table table-bordered" align="center" style ="width: 85%;">
        <tr>
          <td class="col-md-1 bg-primary">Agent name</td>
          <td class="col-md-3">{{$EditData->Monitor->agent->name}}</td>
          <td class="col-md-2 bg-primary">Date of call</td>
          <td class="col-md-2">{{$EditData->Monitor->call_date}}</td>
          <td class="col-md-2 bg-primary">Fail call #</td>
          <td class="col-md-2"></td>
        </tr>
        <tr>
          <td class="col-md-1 bg-primary">Call Scored by </td>
          <td class="col-md-3">@if(!empty($EditData->created_name->name)) {{$EditData->created_name->name}} @endif</td>
          <td class="col-md-2 bg-primary">Monitored at</td> 
          <td class="col-md-2">{{$EditData->created_at}}</td>
          <td class="col-md-2 bg-primary">Caoching at</td>
          <td class="col-md-2">{{date("Y-m-d")}}</td>
         </tr>
        <tr>
          <td class="col-md-2 bg-primary">Caoching by</td>
          <td class="col-md-2">{{Auth::user()->name}}</td>
          <td class="col-md-2" ></td>
          @if($EditData->status == 'open')
          <td colspan="3">
            <a href="{{url(route('Additional.index'))}}" class="btn btn-warning" style="width:100%;"> Additional Calls </a>
          </td>
          @else
          <td></td>
          <td></td>
          <td></td>

          @endif

        </tr>
    </table>



    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">You are working on</div>
                <div class="panel-body">
                  <div class="container-fluid">                        
                      <!-- display errors of validation -->
                      @if ($errors->any())    
                        <div class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                {{ $error }}<br>        
                            @endforeach
                        </div>
                      @endif
                      {!! Form::model($EditData,['data-toggle'=>'validator','id'=>'myForm','role'=>'form','method'=>'PUT','route'=>  array(str_replace(".edit",".update",Request::route()->getName()),$EditData->id)])!!}
                          <input id="monitor_id"  name="monitor_id"   type="hidden" value="{{$EditData->monitor_id}}"   required="required" >
                          <input id="bce"         name="bce"          type="hidden" value="{{$EditData->Monitor->bce}}"          required="required" >
                          <input id="ece"         name="ece"          type="hidden" value="{{$EditData->Monitor->ece}}"          required="required" >
                          <input id="nce"         name="nce"          type="hidden" value="{{$EditData->Monitor->nce}}"          required="required" >
                          <input id="result"      name="result"       type="hidden" value="{{$EditData->Monitor->result}}"       required="required" >
                          <input id="total_score" name="total_score"  type="hidden" value="{{$EditData->Monitor->total_score}}"  required="required" >

                          <div id="formbody">
                              <div class="row">
                                  <div class="col-md-6"> 
                                      <table class="table table-bordered">
                                        <tr class="bg-primary">
                                          <th class="col-md-1">#</th>
                                          <th class="col-md-11"><center>Positive Notes</center></th>
                                        </tr>
                                        <tr>
                                          <td>1</td>
                                          <td>{!! Form::text('positive_notes1', $EditData->positive_notes1 , ['class' => 'form-control','id' =>'positive_notes1']) !!}</td>
                                        </tr>
                                        <tr>
                                          <td>2</td>
                                          <td>{!! Form::text('positive_notes2', $EditData->positive_notes2 , ['class' => 'form-control','id' =>'positive_notes2']) !!}</td>
                                        </tr>
                                        <tr>
                                          <td>3</td>
                                          <td>{!! Form::text('positive_notes3', $EditData->positive_notes3 , ['class' => 'form-control','id' =>'positive_notes3']) !!}</td>
                                        </tr>
                                      </table>
                                  </div> 
                                  <div class="col-md-6"> 
                                    <table class="table table-bordered">
                                      <tr class="bg-primary">
                                        <th>#</th>
                                        <th><center>Negative Notes</center></th>
                                      </tr>
                                      <tr>
                                        <td>1</td>
                                          <td>{!! Form::text('negative_notes1', $EditData->negative_notes1 , ['class' => 'form-control','id' =>'negative_notes1']) !!}</td>
                                      </tr>
                                      <tr>
                                        <td>2</td>
                                          <td>{!! Form::text('negative_notes2', $EditData->negative_notes2 , ['class' => 'form-control','id' =>'negative_notes2']) !!}</td>
                                      </tr>
                                      <tr>
                                        <td>3</td>
                                          <td>{!! Form::text('negative_notes3', $EditData->negative_notes3 , ['class' => 'form-control','id' =>'negative_notes3']) !!}</td>
                                      </tr>
                                    </table>
                                  </div>
                                  <div class="col-md-6">
                                    <table class="table table-bordered">
                                      <tr class="bg-primary">
                                        <th>#</th>
                                        <th colspan="2"><center>Mistake's roote cause</center></th>
                                      </tr>
                                      <tr>
                                        <th></th>
                                        <th><center>Mistake</center></th>
                                        <th><center>Roote cause</center></th>
                                      </tr>
                                      <tr>
                                        <td>1</td>
                                          <td>{!! Form::text('mistake1', $EditData->mistake1 , ['class' => 'form-control','id' =>'mistake1']) !!}</td>
                                          <td>{!! Form::text('roote_cause1', $EditData->roote_cause1 , ['class' => 'form-control','id' =>'roote_cause1']) !!}</td>
                                      </tr>
                                      <tr>
                                        <td>2</td>
                                        <td>{!! Form::text('mistake2', $EditData->mistake2 , ['class' => 'form-control','id' =>'mistake2']) !!}</td>
                                          <td>{!! Form::text('roote_cause2', $EditData->roote_cause2 , ['class' => 'form-control','id' =>'roote_cause2']) !!}</td>
                                      </tr>
                                      <tr>
                                        <td>3</td>
                                        <td>{!! Form::text('mistake3', $EditData->mistake3 , ['class' => 'form-control','id' =>'mistake3']) !!}</td>
                                          <td>{!! Form::text('roote_cause3', $EditData->roote_cause3 , ['class' => 'form-control','id' =>'roote_cause3']) !!}</td>
                                      </tr>
                                    </table>
                                  </div>
                                  <div class="col-md-6"> 
                                    <table class="table table-bordered">
                                      <tr class="bg-primary">
                                        <th>#</th>
                                        <th colspan="2"><center>Action plan  to avoid the quality mistakes</center></th>
                                      </tr>
                                      <tr>
                                        <th></th>
                                        <th><center>Action</center></th>
                                        <th><center>Time fram</center></th>
                                      </tr>
                                      <tr>
                                        <td>1</td>
                                        <td>{!! Form::text('action1', $EditData->action1 , ['class' => 'form-control','id' =>'action1']) !!}</td>
                                        <td>{!! Form::text('time_fram1', $EditData->time_fram1 , ['class' => 'form-control','id' =>'time_fram1']) !!}</td>
                                      </tr>
                                      <tr>
                                        <td>2</td>
                                        <td>{!! Form::text('action2', $EditData->action2 , ['class' => 'form-control','id' =>'action2']) !!}</td>
                                        <td>{!! Form::text('time_fram2', $EditData->time_fram2 , ['class' => 'form-control','id' =>'time_fram2']) !!}</td>
                                      </tr>
                                      <tr>
                                        <td>3</td>
                                        <td>{!! Form::text('action3', $EditData->action3 , ['class' => 'form-control','id' =>'action3']) !!}</td>
                                        <td>{!! Form::text('time_fram3', $EditData->time_fram3 , ['class' => 'form-control','id' =>'time_fram3']) !!}</td>
                                      </tr>
                                    </table>
                                  </div>
                                  <div class="col-md-12"> 
                                    <table class="table table-bordered">
                                      <tr class="bg-primary">
                                        <th class="col-md-1">#</th>
                                        <th class="col-md-11"><center>Negative Notes</center></th>
                                      </tr>
                                      <tr>
                                        <td>1</td>
                                        <td>{!! Form::text('business_action1', $EditData->business_action1 , ['class' => 'form-control','id' =>'business_action1']) !!}</td>
                                      </tr>
                                      <tr>
                                        <td>2</td>
                                        <td>{!! Form::text('business_action2', $EditData->business_action2 , ['class' => 'form-control','id' =>'business_action2']) !!}</td>
                                      </tr>
                                    </table>
                                  </div>
                                  <div class="col-md-12"> 
                                    <table class="table table-bordered">
                                      <tr class="bg-primary">
                                        <th>#</th>
                                        <th colspan="2"><center>Additional Calls Feedback</center></th>
                                      </tr>
                                      <tr>
                                        <th></th>
                                        <th><center>Call Positive feedback</center></th>
                                        <th><center>Negative Feedback</center></th>
                                      </tr>
                                      <tr>
                                        <td>1</td>
                                        <td>{!! Form::text('call_positive_feedback1', $EditData->call_positive_feedback1 , ['class' => 'form-control','id' =>'call_positive_feedback1']) !!}</td>
                                        <td>{!! Form::text('negative_feedback1', $EditData->negative_feedback1 , ['class' => 'form-control','id' =>'negative_feedback1']) !!}</td>
                                      </tr>
                                      <tr>
                                        <td>2</td>
                                        <td>{!! Form::text('call_positive_feedback2', $EditData->call_positive_feedback2 , ['class' => 'form-control','id' =>'call_positive_feedback2']) !!}</td>
                                        <td>{!! Form::text('negative_feedback2', $EditData->negative_feedback2 , ['class' => 'form-control','id' =>'negative_feedback2']) !!}</td>
                                      </tr>
                                      <tr>
                                        <td>3</td>
                                        <td>{!! Form::text('call_positive_feedback3', $EditData->call_positive_feedback3 , ['class' => 'form-control','id' =>'call_positive_feedback3']) !!}</td>
                                        <td>{!! Form::text('negative_feedback3', $EditData->negative_feedback3 , ['class' => 'form-control','id' =>'negative_feedback3']) !!}</td>
                                      </tr>
                                      <tr>
                                        <td>4</td>
                                        <td>{!! Form::text('call_positive_feedback4', $EditData->call_positive_feedback4 , ['class' => 'form-control','id' =>'call_positive_feedback4']) !!}</td>
                                        <td>{!! Form::text('negative_feedback4', $EditData->negative_feedback4 , ['class' => 'form-control','id' =>'negative_feedback4']) !!}</td>
                                      </tr>
                                      <tr>
                                        <td>5</td>
                                        <td>{!! Form::text('call_positive_feedback5', $EditData->call_positive_feedback5 , ['class' => 'form-control','id' =>'call_positive_feedback5']) !!}</td>
                                        <td>{!! Form::text('negative_feedback5', $EditData->negative_feedback5 , ['class' => 'form-control','id' =>'negative_feedback5']) !!}</td>
                                      </tr>
                                      <tr>
                                        <td>6</td>
                                        <td>{!! Form::text('call_positive_feedback6', $EditData->call_positive_feedback6 , ['class' => 'form-control','id' =>'call_positive_feedback6']) !!}</td>
                                        <td>{!! Form::text('negative_feedback6', $EditData->negative_feedback6 , ['class' => 'form-control','id' =>'negative_feedback6']) !!}</td>
                                      </tr>
                                    </table>
                                  </div>
                                  <div class="col-md-12">
                                    <table class="table table-bordered">
                                      <tr class="bg-primary">
                                        <th colspan="2"><center>Caoching Validation</center></th>
                                      </tr>
                                      <tr>
                                        <th><center>Status</center></th>
                                        <th><center>Comment</center></th>
                                      </tr>
                                      <tr>
                                        <td>
                                            
                                          {!! Form::select('caoching_validation_status',
                                            [
                                            null=>'Please Select',
                                            'Done ON Spot'=>'Done ON Spot',
                                            'Done With Delay'=>'Done With Delay',
                                            'Missing and Not Accurate Details'=>'Missing and Not Accurate Details',
                                            'Not Done'=>'Not Done',
                                            ]
                                            ,$EditData->caoching_validation_status,['class' => 'form-control ','id' => 'caoching_validation_status','required' =>'required'])!!}
                                        </td>
                                        <td>{!! Form::text('comment', $EditData->comment , ['class' => 'form-control','id' =>'comment']) !!}</td>
                                      </tr>
                                    </table>
                                  </div>
                              </div>
                          </div>
                          <div class="col-md-12 text-right" id="footer">
                              <button type="submit" class="btn btn-primary">Save</button>
                          </div>
                  </div>
                <div id="well" class="col-md-6">
                    <div class="form-group">
                        <div id="well" class="well">
                            <!-- <i class="text-pramiray">Call Summary</i> -->
                            <!-- <hr> -->
                            <div class="row text-center">
                                <div class="col-md-2">#bce</div>
                                <div class="col-md-2">#ece</div>
                                <div class="col-md-2">#nce</div>
                                <div class="col-md-3">Total Score</div>
                                <div class="col-md-3">Result</div>
                            </div>
                            <div class="row text-center">
                                <div class="col-md-2" id="lbce">{{$EditData->Monitor->bce}}</div>
                                <div class="col-md-2" id="lece">{{$EditData->Monitor->ece}}</div>
                                <div class="col-md-2" id="lnce">{{$EditData->Monitor->nce}}</div>
                                <div class="col-md-3" id="ltotal">{{$EditData->Monitor->total_score}}</div>
                                <div class="col-md-3" id="lresult">{{$EditData->Monitor->result}}</div>
                            </div>
                        </div>
                        </div>
                    </div>    
                </div>
            </div>
    </div>
    
    <div class="col-md-12" id="sheet">
        <div class="ScoreSheet" id="ScoreSheet">
            <div class="panel panel-default">
                <div class="panel-heading">You are working on {{$DataTable->score_sheet->name}}</div>
                <div class="panel-body">
                    <ul class="nav nav-tabs" id="myTabs" role="tablist">
                        @foreach ($DataTable->score_sheet->category as $Group)
                                <li role="presentation"><a href="#pane-{{$Group->id}}" aria-controls="{{$Group->id}}" role="tab" data-toggle="tab">{{$Group->name}}</a></li>
                        @endforeach
                    </ul>
                    <div class="tab-content">
                        @foreach ($DataTable->score_sheet->category as $Groups)
                            <div role="tabpanel" class="tab-pane fade Group" id="pane-{{$Groups->id}}" name="{{$Groups->name}}"  title="{{$Groups->description}}" value="">
                                @foreach ($Groups->section as $section)
                                    <div class="col-md-6 Section" name="{{$section->id}}" id="{{$section->rank}}" title="{{$section->rank}}" >
                                        <fieldset class="scheduler-border">
                                            <legend class="scheduler-border" title="{{$section->name}}">
                                                    @php $str = wordwrap($section->name, 20);$str = explode("\n", $str);$str = $str[0] . '...';echo $str; @endphp
                                            </legend>
                                            <table id="tableToExcel" class="table table-hover table-striped table-bordered table-condensed">
                                                <tbody>
                                                    @foreach ($section->Attribute as $Attribute)
                                                    @php 
                                                        $description ='description_id_'.$Attribute->id;
                                                        $option      ='attribute_option_id_'.$Attribute->id;
                                                    @endphp
                                                    <tr>
                                                        <td class="col-md-7" title="{{$Attribute->name}}">@php $str = wordwrap($Attribute->name, 30);$str = explode("\n", $str);$str = $str[0] . '...';echo $str; @endphp</td>
                                                        @if(!empty($Sheet))
                                                            <td class="col-md-2" id="{{$Attribute->rank}}">{!! Form::select($option,$Attribute->Option,$Sheet->$option,['class' => 'form-control Option','id' => 'Option','section' => $section->id,'title' => $Attribute->rank,'GroupId'=>$Groups->id,'disabled'=>'disabled'])!!}</td>
                                                            <td class="col-md-3"><input type="text" class="form-control" name="{{$description}}" id="{{$Attribute->name}}" value="{{$Sheet->$description}}" disabled="disabled"/></td>
                                                        @else
                                                            <td class="col-md-2" id="{{$Attribute->rank}}">{!! Form::select($option,$Attribute->Option,null,['class' => 'form-control Option','id' => 'Option','section' => $section->id,'title' => $Attribute->rank,'GroupId'=>$Groups->id,'disabled'=>'disabled'])!!}</td>
                                                            <td class="col-md-3"><input type="text" class="form-control" name="{{$description}}" id="{{$Attribute->name}}" value="" disabled="disabled"/></td>
                                                        @endif
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </fieldset>
                                    </div>
                                @endforeach
                            </div>  
                        @endforeach
                    </div>
                </div>
            </div>  

        <style type="text/css">
            fieldset.scheduler-border {
            border: 1px groove #ddd !important;
            padding: 0 1.4em 1.4em 1.4em !important;
            margin: 0 0 1.5em 0 !important;
            -webkit-box-shadow:  0px 0px 0px 0px #000;
                    box-shadow:  0px 0px 0px 0px #000;
            }

            legend.scheduler-border {
                font-size: 1.2em !important;
                font-weight: bold !important;
                text-align: left !important;

            }

            legend.scheduler-border {
                width:inherit; /* Or auto */
                padding:0 10px; /* To give a bit of padding on the left and right */
                border-bottom:none;
            }
            .tab-content{
                background-color:#ffffff;
            }
        </style>                      
        </div>
    </div>

<script type="text/javascript">
    $('#myTabs a:first').tab('show');
    $( "#sheet" ).insertAfter( $( "#footer" ) );
    $( "#well" ).insertAfter( $( "#footer" ) );

</script>
@endsection

@extends('layouts.app')
@section('content')
<div class="container-fluid ">
	
	<!--Index-->
	<input type="hidden" id="key" 	 	 value="id">
	<input type="hidden" id="model"  	 value="Additional">
	<input type="hidden" id="groupby" 	 value="id">
	<input type="hidden" id="path" 	 	 value="Quality">
	<input type="hidden" id="conditions" value='{"status":"close"}' name="conditions">
 	@include('layouts.table')
	<!--End Index-->
</div>
<script type="text/javascript">
	$('.col-int').remove();
	$('.destroy').remove();
	$('.row .col-lg-1 a').remove();
</script>
@endsection

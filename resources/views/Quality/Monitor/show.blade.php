@extends('layouts.app')
@section('content')
	@inject('Project', 'App\Project')
	@php 
		$project_id     = $Project->lists('name','id')->toArray(); 
        $manager_id     = array($ShowData->manager->id    => $ShowData->manager->name);
        $projects = App\Project::lists('name','id')->toArray(); 
        $supervisor_id  = array($ShowData->supervisor->id => $ShowData->supervisor->name);
		$supervisor_id 	= array($ShowData->supervisor->id => $ShowData->supervisor->name);
        $teamleader_id  = array($ShowData->teamleader->id => $ShowData->teamleader->name);
		$senior_id 		= array($ShowData->senior->id =>	 $ShowData->senior->name);
		$agent_id 		= array($ShowData->agent->id =>		 $ShowData->agent->name);
	@endphp    
	<br/>
    <div class="container">
	    <div class="panel panel-default">
			<div class="panel-heading">You are working on</div>
			  	<div class="panel-body">
	    		@include('layouts.form')
	    		
				<div id="well" class="col-md-6">
                    <div class="form-group">
                    	<div id="well" class="well">
                    		<!-- <i class="text-pramiray">Call Summary</i> -->
                    		<!-- <hr> -->
                    		<div class="row text-center">
                    			<div class="col-md-2">#bce</div>
                    			<div class="col-md-2">#ece</div>
                    			<div class="col-md-2">#nce</div>
                    			<div class="col-md-3">Total Score</div>
                    			<div class="col-md-3">Result</div>
                    		</div>
                    		<div class="row text-center">
                    			<div class="col-md-2" id="lbce">{{$ShowData->bce}}</div>
                    			<div class="col-md-2" id="lece">{{$ShowData->ece}}</div>
                    			<div class="col-md-2" id="lnce">{{$ShowData->nce}}</div>
                    			<div class="col-md-3" id="ltotal">{{$ShowData->total_score}}</div>
                    			<div class="col-md-3" id="lresult">{{$ShowData->result}}</div>
                    		</div>
                    	</div>
		    			</div>
                    </div>    
                </div>
	    	</div>
	</div>
    
   	
   	<div class="col-md-12" id="sheet">
		<div class="ScoreSheet" id="ScoreSheet">
            <div class="panel panel-default">
                <div class="panel-heading">You are working on {{$DataTable->score_sheet->name}}</div>
                <input type="hidden" name="sheetname" value="{{$DataTable->score_sheet->name}}">
                <div class="panel-body">
                    <ul class="nav nav-tabs" id="myTabs" role="tablist">
                        @foreach ($DataTable->score_sheet->category as $Group)
                                <li role="presentation"><a href="#pane-{{$Group->id}}" aria-controls="{{$Group->id}}" role="tab" data-toggle="tab">{{$Group->name}}</a></li>
                        @endforeach
                    </ul>
                    <div class="tab-content">
                        @foreach ($DataTable->score_sheet->category as $Groups)
                            <div role="tabpanel" class="tab-pane fade Group" id="pane-{{$Groups->id}}" name="{{$Groups->name}}"  title="{{$Groups->description}}" value="">
                                @foreach ($Groups->section as $section)
                                    <div class="col-md-6 Section" name="{{$section->id}}" id="{{$section->rank}}" title="{{$section->rank}}" >
                                        <fieldset class="scheduler-border">
                                            <legend class="scheduler-border" title="{{$section->name}}">
                                                    @php $str = wordwrap($section->name, 20);$str = explode("\n", $str);$str = $str[0] . '...';echo $str; @endphp
                                            </legend>
                                            <table id="tableToExcel" class="table table-hover table-striped table-bordered table-condensed">
                                                <tbody>
                                                    @foreach ($section->Attribute as $Attribute)
                                                    <input type="hidden" value="{{$Attribute->id}}"  name="attribute_id[]" >
                                                    @php 
                                                        $description ='description_id_'.$Attribute->id;
                                                        $option      ='attribute_option_id_'.$Attribute->id;
                                                    @endphp
                                                    <tr>
                                                        <td class="col-md-7" title="{{$Attribute->name}}">@php $str = wordwrap($Attribute->name, 30);$str = explode("\n", $str);$str = $str[0] . '...';echo $str; @endphp</td>
                                                        @if(!empty($Sheet))
                                                            <td class="col-md-2" id="{{$Attribute->rank}}">{!! Form::select($option,$Attribute->Option,$Sheet->$option,['class' => 'form-control Option','id' => 'Option','section' => $section->id,'title' => $Attribute->rank,'GroupId'=>$Groups->id,'required' =>'required'])!!}</td>
                                                            <td class="col-md-3"><input type="text" class="form-control" name="{{$description}}" id="{{$Attribute->name}}" value="{{$Sheet->$description}}" /></td>
                                                        @else
                                                            <td class="col-md-2" id="{{$Attribute->rank}}">{!! Form::select($option,$Attribute->Option,null,['class' => 'form-control Option','id' => 'Option','section' => $section->id,'title' => $Attribute->rank,'GroupId'=>$Groups->id,'required' =>'required'])!!}</td>
                                                            <td class="col-md-3"><input type="text" class="form-control" name="{{$description}}" id="{{$Attribute->name}}" value="" /></td>
                                                        @endif
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </fieldset>
                                    </div>
                                @endforeach
                            </div>  
                        @endforeach
                    </div>
                </div>
            </div>  
        <script type="text/javascript">
            function QQC(){
                var Option = $('.Option').map(function(){return this;});
                var Group = $('.Group').map(function(){return this;});
                var vars = {};
                count = 0 ;

                for (i = 0; i < Option.length; i++) {
                    if (Option[i].value == 0){
                        var SectionName  = $(Option[i]).attr('section');
                        var sectionRank  = parseInt($('[name='+SectionName+']').attr('id'));
                        var QuestionRank = parseInt($(Option[i]).attr('title'));
                        $('[name='+SectionName+']').attr('id',QuestionRank - QuestionRank);
                    }
                }

                for (G = 0; G < Group.length; G++) {
                    //
                    vars[Group[G].title] = ($('#'+Group[G].id+' .Section').map(function(){if(this.id < this.title){return Number(this.title - this.id);}}).get());
                }
                var G1 = 0;
                var G2 = 0;
                var G3 = 0;
                $.each(vars['nce'],function() {G1 += this;});
                $.each(vars['ece'],function() {G2 += this;});
                $.each(vars['bce'],function() {G3 += this;});
                if ((G1+ + G2+ +G3) > 100){var total = 0;}else{var total =100 -(G1+ + G2+ +G3);}
                if(total < 92){var RESULT= 'fail'}else{var RESULT= 'success'}
                
                $('#lnce').text(vars['nce'].length);
                $('#nce').val(vars['nce'].length);
                $('#lece').text(vars['ece'].length);
                $('#ece').val(vars['ece'].length);
                $('#lbce').text(vars['bce'].length);
                $('#bce').val(vars['bce'].length);
                $('#ltotal').text(total);
                $('#total_score').val(total);
                $('#lresult').text(RESULT);
                $('#result').val(RESULT);
            }
           $(document).on('change','#Option',function(){
                //
                
                var Question    = parseInt($(this).val());
                var QuestionRank= parseInt($(this).closest('td').attr('id'));
                var sectionRank = parseInt($(this).closest('div').attr('id'));
                var MastrRank   = parseInt($(this).closest('div').attr('title'));
                
                if (Question == 0){
                $(this).closest('tr').find('td:eq(2) > input').prop('required',true);
                $(this).closest('tr').find('td:eq(2)').addClass('has-error');
                }else{
                $(this).closest('tr').find('td:eq(2) > input').prop('required',false);
                $(this).closest('tr').find('td:eq(2)').removeClass('has-error');
                }
                
                if (Question != 0 && sectionRank <=  MastrRank)  {
                    var arr = $('[name='+$(this).attr('section')+']  select.Option').map(function(){return Number(this.value);}).get();
                    var total = 0;$.each(arr,function() {total += Number(this);});
                    if(arr.length ==total){$(this).closest('div').attr('id',sectionRank + QuestionRank);}
                };
                QQC();
            });
            $( document ).ready(function() {
                //
                QQC();
            });

        </script>
        <style type="text/css">
            fieldset.scheduler-border {
            border: 1px groove #ddd !important;
            padding: 0 1.4em 1.4em 1.4em !important;
            margin: 0 0 1.5em 0 !important;
            -webkit-box-shadow:  0px 0px 0px 0px #000;
                    box-shadow:  0px 0px 0px 0px #000;
            }

            legend.scheduler-border {
                font-size: 1.2em !important;
                font-weight: bold !important;
                text-align: left !important;

            }

            legend.scheduler-border {
                width:inherit; /* Or auto */
                padding:0 10px; /* To give a bit of padding on the left and right */
                border-bottom:none;
            }
            .tab-content{
                background-color:#ffffff;
            }
        </style>                      
		</div>
    </div>

    <div id="reda" class="col-md-12">
        <table class="table table-striped">
            <thead>
                <tr>
                    <td>#</td>
                    <td>BCE</td>
                    <td>ECE</td>
                    <td>NCE</td>
                    <td>Total Score</td>
                    <td>Result</td>
                    <td>Status</td>
                </tr>
            </thead>
            <tbody>
                
                    <tr style="cursor:pointer" class="tr clickable-row" data-id="" data-href="">
                        <td> @if(!empty($ShowData->Coaching->status)) {{$ShowData->Coaching->status}} @endif </td>
                    </tr>
                
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="6">Total Coaching for this Call</td>
                  
                </tr>
            </tfoot>
        </table>
    </div>
<script type="text/javascript">
    $('#myTabs a:first').tab('show');
    $('.text-primary').remove();
    $( "#reda" ).insertAfter( $( "#footer" ) );
    $( "#sheet" ).insertAfter( $( "#footer" ) );
    jQuery(document).ready(function($) {
        $(".clickable-row").click(function() {
            window.document.location = $(this).data("href");
        });
    });

    $( "#well" ).insertAfter( $( "#formbody .customer_satisfaction" ) );
    var FCR        = $('#fcr :selected').val();    
    if (FCR == 'Done') {
        $("#fcr_reason").prop('required',false);
        $("#fcr_reason").prop('disabled',true);
    }else{
        $("#fcr_reason").prop('required',true);
        $("#fcr_reason").prop('disabled',false);
    }

	$(document).on('change','#fcr',function(){                
        var FCR        = $('#fcr :selected').val();
        //alert(FCR);
        if (FCR == 'Done') {
            $("#fcr_reason").prop('required',false);
            $("#fcr_reason").prop('disabled',true);
        }else{
            $("#fcr_reason").prop('required',true);
            $("#fcr_reason").prop('disabled',false);
        }
    });


    $(function () {
        $('#call_duration').datetimepicker({format: 'HH:mm:ss',});
        $('#hold_duration').datetimepicker({format: 'HH:mm:ss',});
        $('#call_date').datetimepicker({format: 'YYYY-MM-DD',defaultDate: ' {{date("Y-m-d")}}',useCurrent: false});
        $('#call_time').datetimepicker({format: 'hh:mm',defaultDate: ' {{date("Y-m-d")}} 00:00:00',});
    });

    $('#project_id').attr('onchange','AjaxVildation("employees","project_id","project_id","manager_id","1","supervisor_id")');
    $('#supervisor_id').attr('onchange','AjaxVildation("employees","project_id","project_id","manager_id","supervisor_id","teamleader_id")');
    $('#teamleader_id').attr('onchange','AjaxVildation("employees","project_id","project_id","manager_id","teamleader_id","senior_id")');
    $('#senior_id').attr('onchange','AjaxVildation("employees","project_id","project_id","manager_id","senior_id","agent_id")');
    $('#agent_id').attr('onchange','AjaxScoreSheet("project_id","id","projects")');
    function AjaxScoreSheet(name,table_id,table) {
        var id 		= $('#'+name+' :selected').attr( "value" );
        //alert(value1);
    	$.ajax({
                url     :"{{url('AjaxScoreSheet?')}}",
                data    :{id:id,'table_id':table_id,table:table},
                dataType:'json',
                type    :'get',
                success : function(data){
                	$('.ScoreSheet').html(data);
                }
        });
        return false;
	};
    function AjaxVildation(table,col1,search1,col2,search2,appendtolist) {
        var search1 		= $('#'+search1+' :selected').attr( "value" );
        if(search2 == 1 ){

        }else{
        	var search2 		= $('#'+search2+' :selected').attr( "value" );
        }
        $.ajax({
                url     :"{{url('AjaxVildation?')}}",
                data    :{table:table,col1:col1,'search1':search1,col2:col2,'search2':search2},
                dataType:'json',
                type    :'get',
                success : function(data){
                    var $option = $('#'+appendtolist);
                    $option.empty();
                    $option.append('<option value="" selected>Select</option>');
                    $.each(data, function(index, value) {
                        
                        $option.append('<option value="'+value.id+'" >'+value.name+'</option>');
                    });
                    $('#'+appendtolist).trigger('chosen:updated');
                }
        });
        return false;
	};
</script>
@endsection

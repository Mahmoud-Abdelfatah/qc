<div class="panel panel-default">
	<div class="panel-heading">You are working on {{$DataTable->score_sheet->name}}</div>
    <input type="hidden" name="sheetname" value="{{$DataTable->score_sheet->name}}"/>
  	<div class="panel-body">
		<ul class="nav nav-tabs" id="myTabs" role="tablist">
			@foreach ($DataTable->score_sheet->category as $Group)
					<li role="presentation"><a href="#pane-{{$Group->id}}" aria-controls="{{$Group->id}}" role="tab" data-toggle="tab">{{$Group->name}}</a></li>
			@endforeach
		</ul>
		<div class="tab-content">
			@foreach ($DataTable->score_sheet->category as $Groups)
				<div role="tabpanel" class="tab-pane fade Group" id="pane-{{$Groups->id}}" name="{{$Groups->name}}"  title="{{$Groups->description}}" value="">
					@foreach ($Groups->Section as $section)
						<div class="col-md-6 Section" name="{{$section->id}}" id="{{$section->rank}}" title="{{$section->rank}}" >						
							<fieldset class="scheduler-border">
								<legend class="scheduler-border"><i class="text-primary" title="{{$section->name}}">@php $str = wordwrap($section->name, 20);$str = explode("\n", $str);$str = $str[0] . '...';echo $str; @endphp</i></legend>
								<table id="tableToExcel" class="table table-hover table-striped table-bordered table-condensed">
									<tbody>
                                        @foreach ($section->Attribute as $Attribute)
                                        @php 
                                            $description ='description_id_'.$Attribute->id;
                                            $option      ='attribute_option_id_'.$Attribute->id;
                                        @endphp
                                        <input type="hidden" value="{{$Attribute->id}}"  name="attribute_id[]" >
                                        <tr>
                                            <td class="col-md-7" title="{{$Attribute->name}}">@php $str = wordwrap($Attribute->name, 30);$str = explode("\n", $str);$str = $str[0] . '...';echo $str; @endphp</td>
                                            
                                            @if(!empty($Sheet->$option))
                                                <td class="col-md-2" id="{{$Attribute->rank}}">{!! Form::select($option,$Attribute->Option,$Sheet->$option,['class' => 'form-control Option','id' => 'Option','section' => $section->id,'title' => $Attribute->rank,'GroupId'=>$Groups->id,'required' =>'required'])!!}</td>
                                            @else
                                                <td class="col-md-2" id="{{$Attribute->rank}}">{!! Form::select($option,$Attribute->Option,null,['class' => 'form-control Option','id' => 'Option','section' => $section->id,'title' => $Attribute->rank,'GroupId'=>$Groups->id,'required' =>'required'])!!}</td>
                                            @endif
                                            
                                            @if(!empty($Sheet->$description))
                                                <td class="col-md-3"><input type="text" class="form-control" name="{{$description}}" id="{{$Attribute->name}}" value="{{$Sheet->$description}}" /></td>
                                            @else
                                                <td class="col-md-3"><input type="text" class="form-control" name="{{$description}}" id="{{$Attribute->name}}" value="" /></td>
                                            @endif

                                        </tr>
                                        @endforeach
                                    </tbody>
								</table>
							</fieldset>
						</div>
					@endforeach
				</div>	
			@endforeach
		</div>
	</div>
</div>	

<script type="text/javascript">
$('#myTabs a:first').tab('show');
</script>

<script type="text/javascript">
	 function QQC(){
                var Option = $('.Option').map(function(){return this;});
                var Group = $('.Group').map(function(){return this;});
                var vars = {};
                count = 0 ;
                
                for (i = 0; i < Option.length; i++) {
                    if (Option[i].value == 0){
                        var SectionName  = $(Option[i]).attr('section');
                        var sectionRank  = parseInt($('[name='+SectionName+']').attr('id'));
                        var QuestionRank = parseInt($(Option[i]).attr('title'));
                        $('[name='+SectionName+']').attr('id',QuestionRank - QuestionRank);
                    }
                }

                for (G = 0; G < Group.length; G++) {
                    //
                    vars[Group[G].title] = ($('#'+Group[G].id+' .Section').map(function(){if(this.id < this.title){return Number(this.title - this.id);}}).get());
                }
                var G1 = 0;
                var G2 = 0;
                var G3 = 0;
                $.each(vars['nce'],function() {G1 += this;});
                $.each(vars['ece'],function() {G2 += this;});
                $.each(vars['bce'],function() {G3 += this;});
                if ((G1+ + G2+ +G3) > 100){var total = 0;}else{var total =100 -(G1+ + G2+ +G3);}
                if(total < 92){var RESULT= 'fail'}else{var RESULT= 'success'}
                
                $('#lnce').text(vars['nce'].length);
                $('#nce').val(vars['nce'].length);
                $('#lece').text(vars['ece'].length);
                $('#ece').val(vars['ece'].length);
                $('#lbce').text(vars['bce'].length);
                $('#bce').val(vars['bce'].length);
                $('#ltotal').text(total);
                $('#total_score').val(total);
                $('#lresult').text(RESULT);
                $('#result').val(RESULT);
            }
            $(document).on('change','#Option',function(){
                //
                
                var Question    = parseInt($(this).val());
                var QuestionRank= parseInt($(this).closest('td').attr('id'));
                var sectionRank = parseInt($(this).closest('div').attr('id'));
                var MastrRank   = parseInt($(this).closest('div').attr('title'));
                
                if (Question == 0){
                $(this).closest('tr').find('td:eq(2) > input').prop('required',true);
                $(this).closest('tr').find('td:eq(2)').addClass('has-error');
                }else{
                $(this).closest('tr').find('td:eq(2) > input').prop('required',false);
                $(this).closest('tr').find('td:eq(2)').removeClass('has-error');
                }
                
                if (Question != 0 && sectionRank <=  MastrRank)  {
                    var arr = $('[name='+$(this).attr('section')+']  select.Option').map(function(){return Number(this.value);}).get();
                    var total = 0;$.each(arr,function() {total += Number(this);});
                    if(arr.length ==total){$(this).closest('div').attr('id',sectionRank + QuestionRank);}
                };
                QQC();
            });
            $( document ).ready(function() {
                //
                QQC();
            });

            
</script>
<style type="text/css">
	fieldset.scheduler-border {
    border: 1px groove #ddd !important;
    padding: 0 1.4em 1.4em 1.4em !important;
    margin: 0 0 1.5em 0 !important;
    -webkit-box-shadow:  0px 0px 0px 0px #000;
            box-shadow:  0px 0px 0px 0px #000;
	}

	legend.scheduler-border {
	    font-size: 1.2em !important;
	    font-weight: bold !important;
	    text-align: left !important;

	}

	legend.scheduler-border {
	    width:inherit; /* Or auto */
	    padding:0 10px; /* To give a bit of padding on the left and right */
	    border-bottom:none;
	}
	</style>

	<style type="text/css">
	.tab-content{
	    background-color:#ffffff;

	/*  
		border-color: red;
	    border: 1px solid black;
	    border-top: 0px;
	    border-bottom-right-radius	: 24px;
	    border-bottom-left-radius: 24px;
	*/
	}
	/*.nav-tabs > li > a{
	  border: medium none;
	}
	.nav-tabs > li > a:hover{
	  background-color: #303136 !important;
	    border: medium none;
	    border-radius: 0;
	    color:#fff;
	}*/
</style>

<!-- Table -->
<input type="hidden" id="ordertype" name="ordertype" value="{{Input::get('ordertype')}}">
<input type="hidden" id="orderby"   name="orderby"   value="{{Input::get('orderby')}}">
<table id="tableToExcel" class="table table-hover table-striped table-bordered">
	<thead id="header">
		<tr>
		@foreach($myModal->getCasts() as $value)
			<th class="col-{{$value}}">{{ trans('form.'.$value) }}<span name="{{$value}}" type="desc" class="sort glyphicon glyphicon-sort pull-right"></span></th>
		@endforeach
		<th class="col-lg-1 col-action text-center">/</th>
		</tr>
	</thead>
	<tbody id="data">
		@foreach($DataTable as $row )
				<tr style="cursor:pointer" class="tr clickable-row" data-href="{{URL::route($myModal->model_name.'.edit',   $row->id)}}">
					@foreach($myModal->getCasts() as $key => $column)
						@if (strpos($column, '_id') !== false) 
							@php $COLUMN	=str_replace('_id','',$column); @endphp
							<td class="col-{{$column}}">
								@if(!empty($row->$COLUMN->name)){{$row->$COLUMN->name}}@endif
							</td>

						@elseif (strpos($column, '_Modelrelationship') !== false) 
							@php $COLUMN	=str_replace('_Modelrelationship','',$column); @endphp
							<td class="col-{{$column}}">{{$row->Monitor->$COLUMN}}</td>
						@elseif (strpos($column, '_by') !== false) 
								@php $COLUMN	=str_replace('_by','_name',$column); @endphp
								<td class="col-{{$column}}">
									@if(!empty($row->$COLUMN->name)){{$row->$COLUMN->name}}@endif
								</td>
						@elseif (strpos($column, 'count_') !== false)
							@php $COLUMN	=str_replace('count_','',$column); @endphp
								<td class="col-{{$column}}">
									{{$row->count($COLUMN)}}
								</td>
						@elseif (strpos($column, 'sum_') !== false)
							@php $COLUMN	=str_replace('sum_','',$column); @endphp
								<td class="col-{{$column}}">
									{{$row->sum($COLUMN)}}
								</td>
						@elseif (strpos($column, 'counttime_') !== false)
							@php $COLUMN	=str_replace('counttime_','s',$column); @endphp
								<td class="col-{{$column}}">
									{{$row->$COLUMN}}
								</td>
						@elseif (strpos($column, '_count') !== false) 
								@php $COLUMN	=str_replace('_count','',$column); @endphp
								<td class="col-{{$column}}">
									@if(!empty($row->$COLUMN->count())){{$row->$COLUMN->count()}}@endif
								</td>
						@elseif (strpos($column, '_List') !== false)
							<td class="col-{{$column}}">
								@if(!empty($row->$column))
									@foreach ($row->$column as $key => $value)
										<span class="label label-warning" title="{{$key}}">@php $str = wordwrap($key, 20);$str = explode("\n", $str);$str = $str[0];echo $str; @endphp</span>
									@endforeach
								@endif
							</td>
						@else
							<td class="col-{{$column}}" title="{{$row->$column}}">@php $str = wordwrap($row->$column, 70);$str = explode("\n", $str);$str = $str[0];echo $str; @endphp</td>
						@endif
					@endforeach
					<td class="text-center col-action col-md-1">
				   		<!-- <button data-route="{{URL::route($myModal->model_name.'.show',   $row->id)}}" id ="={{$row->id}}"  type="button" class="Show btn btn-info btn-xs"><i class="fa fa-eye"></i></button> -->
				   		<a            href="{{URL::route($myModal->model_name.'.edit',   $row->id)}}" class="" ><i class="glyphicon glyphicon-pencil"></i></a>
				   		<a data-route="{{URL::route($myModal->model_name.'.destroy',$row->id)}}" id="{{$row->id}}" data-token="{{ csrf_token() }}" type="button" class="destroy"><i class="glyphicon glyphicon-remove"></i></a>
				    </td>
				</tr>
			@endforeach
	</tbody>
	<tfooter class="tfooter">
			<tr id="tfooter">
				@foreach($myModal->getCasts() as $key => $value)
					@if (strpos($value, '_id') !== false) 
						<!-- <th class="col-{{$value}} hidden"> -->
							<select onchange='pageid();' name="{{$value}}" id="{{$value}}" class="hidden">
							<option value=''>All</option>
							</select>
						<!-- </th> -->
					@else
						<!-- <th class="col-{{$value}}"> -->
						<input name="{{$value}}" id="{{$value}}" onchange='pageid();' value="" class="form-control input-sm hidden" style="width: 100%;padding: 3px;box-sizing: border-box;"></input>
						<!-- </th> -->
					@endif
				@endforeach
				<th class="col-action text-center hidden">Action</th>
			</tr>
		</tfooter>
</table>
<div class="row">
	<div class="col-md-8 text-left">Show {!! $DataTable->count() !!} of {!! $DataTable->total() !!} {!! str_replace('/?','?',$DataTable->appends(['search' => Input::get('search')])->appends(['sort' => Input::get('sort')])->render())!!}</div>
	<!-- <div class="col-md-4 text-right"><button class="btn btn-primary" onclick="saveAsExcel('tableToExcel', '{{$myModal->model_name}}.xls')"><i class="fa fa-file-excel-o"></i> Export </button></div> -->
</div>



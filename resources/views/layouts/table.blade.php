<h2 class="text-primary">{{$myModal->model_name}}</h2>

<div class="row">
	<div class="col-lg-1 addbtn">
		<a href="{{URL::route($myModal->model_name.'.create')}}" class="btn btn-default"><!-- <span class="glyphicon glyphicon-star" aria-hidden="true"></span> -->Add</a>
		<div class="input-group hidden">
			<span class="input-group-btn">
				<a class="btn btn-primary" href="{{URL::route($myModal->model_name.'.create')}}"><i class="fa fa-plus-square"></i></a>
			</span>
			<select class="form-control" id="rows" onchange="pageid();">
				@for ($i = 10; $i <= 200; $i=$i+10) 
				<option>{{$i}}</option>
				@endfor
			</select>
		</div>		
	</div>
	
@if($myModal->model_name == 'Additional' || $myModal->model_name == 'Coaching')
	
	@else  

	<div class="col-lg-2">
	   @role('admin || Sadmin || vodadmin')
		<div class="input-group"> 
			<input type="text" class="form-control" placeholder="Search for..." name="search" id="search"> 
			<span class="input-group-btn"> 
				<button class="btn btn-default glyphicon glyphicon-search" onclick="pageid();" type="button"></button> 
			</span> 
		</div>
		@endrole
	</div>
@endif
</div>
<br/>
<div class="classPageID">
	<!-- Table -->
	<input type="hidden" id="ordertype" name="ordertype" value="desc">
	<input type="hidden" id="orderby" 	name="orderby"   value="id">

	<table id="tableToExcel"class="table table-hover table-striped table-bordered table-condensed">
		<thead id="header">
			<tr>
				@foreach($myModal->getCasts() as $key => $value)
				<th class="col-{{$value}}">{{ trans('form.'.$value) }}<span name="{{$value}}" type="asc" class="sort glyphicon glyphicon-sort pull-right"></span></th>
				@endforeach

				@if($myModal->model_name == 'Monitor'|| $myModal->model_name == 'Additional')
				@role('admin || qmanger || qsuper ||qspecia || qobserver || oleader || oagent || vodadmin || Sadmin')
				<th class="col-action text-center">/</th>
				@endrole

				@else
				
				<th class="col-action text-center">/</th>

				@endif
			</tr>
		</thead>
		<tbody id="data">
			@foreach($DataTable as $row )
			<tr style="cursor:pointer" class="tr" data-href="{{URL::route($myModal->model_name.'.edit',   $row->id)}}">  <!-- clickable-row -->
				@foreach($myModal->getCasts() as $key => $column)
				@if (strpos($column, '_id') !== false) 
				@php $COLUMN	=str_replace('_id','',$column); @endphp
				<td class="col-{{$column}}">
					@if(!empty($row->$COLUMN->name)){{$row->$COLUMN->name}}@endif
				</td>

				@elseif (strpos($column, '_Modelrelationship') !== false) 
				@php $COLUMN	=str_replace('_Modelrelationship','',$column); @endphp
				<td class="col-{{$column}}">{{$row->Monitor->$COLUMN}}</td>
				@elseif (strpos($column, '_by') !== false) 
				@php $COLUMN	=str_replace('_by','_name',$column); @endphp
				<td class="col-{{$column}}">
					@if(!empty($row->$COLUMN->name)){{$row->$COLUMN->name}}@endif
				</td>
				@elseif (strpos($column, 'count_') !== false)
				@php $COLUMN	=str_replace('count_','',$column); @endphp
				<td class="col-{{$column}}">
					{{$row->count($COLUMN)}}
				</td>
				@elseif (strpos($column, 'sum_') !== false)
				@php $COLUMN	=str_replace('sum_','',$column); @endphp
				<td class="col-{{$column}}">
					{{$row->sum($COLUMN)}}
				</td>
				@elseif (strpos($column, 'counttime_') !== false)
				@php $COLUMN	=str_replace('counttime_','s',$column); @endphp
				<td class="col-{{$column}}">
					{{$row->$COLUMN}}
				</td>
				@elseif (strpos($column, '_count') !== false) 
				@php $COLUMN	=str_replace('_count','',$column); @endphp
				<td class="col-{{$column}}">
					@if(!empty($row->$COLUMN->count())){{$row->$COLUMN->count()}}@endif
				</td>
				@elseif (strpos($column, '_List') !== false)
				<td class="col-{{$column}}">
					@if(!empty($row->$column))
					@foreach ($row->$column as $key => $value)
					<span class="label label-warning" title="{{$key}}">@php $str = wordwrap($key, 20);$str = explode("\n", $str);$str = $str[0];echo $str; @endphp</span>
					@endforeach
					@endif
				</td>
				@else
				<td class="col-{{$column}}" title="{{$row->$column}}">@php $str = wordwrap($row->$column, 70);$str = explode("\n", $str);$str = $str[0];echo $str; @endphp</td>
				@endif
				@endforeach

				@if($myModal->model_name == 'Monitor'|| $myModal->model_name == 'Additional')
				@role('admin || qmanger || qsuper ||qspecia || qobserver || Sadmin || vodadmin')
				<td class="text-center col-action col-md-1">
					<!-- <button data-route="{{URL::route($myModal->model_name.'.show',   $row->id)}}" id ="={{$row->id}}"  type="button" class="Show btn btn-info btn-xs"><i class="fa fa-eye"></i></button> -->
					<a            href="{{URL::route($myModal->model_name.'.edit',   $row->id)}}" class="" ><i class="glyphicon glyphicon-pencil"></i></a>
					<a data-route="{{URL::route($myModal->model_name.'.destroy',$row->id)}}" id="{{$row->id}}" data-token="{{ csrf_token() }}" type="button" class="destroy"><i class="glyphicon glyphicon-remove"></i></a>
				</td>
				@endrole

				 @role('oleader || oagent')
				 <td class="text-center col-action col-md-1">
				   
				 <button data-route="{{URL::route($myModal->model_name.'.show',   $row->id)}}" id ="={{$row->id}}"  type="button" class="Show btn btn-info btn-xs"><i class="fa fa-eye"></i></button> 
                                 @endrole

				@else
				
				<td class="text-center col-action col-md-1">
					<!-- <button data-route="{{URL::route($myModal->model_name.'.show',   $row->id)}}" id ="={{$row->id}}"  type="button" class="Show btn btn-info btn-xs"><i class="fa fa-eye"></i></button> -->
					<a            href="{{URL::route($myModal->model_name.'.edit',   $row->id)}}" class="" ><i class="glyphicon glyphicon-pencil"></i></a>
					<a data-route="{{URL::route($myModal->model_name.'.destroy',$row->id)}}" id="{{$row->id}}" data-token="{{ csrf_token() }}" type="button" class="destroy"><i class="glyphicon glyphicon-remove"></i></a>
				</td>

				@endif


			</tr>
			@endforeach
		</tbody>
		<tfooter class="tfooter">
			<tr id="tfooter">
				@foreach($myModal->getCasts() as $key => $value)
				@if (strpos($value, '_id') !== false) 
				<!-- <th class="col-{{$value}} hidden"> -->
				<select onchange='pageid();' name="{{$value}}" id="{{$value}}" class="hidden">
					<option value=''>All</option>
				</select>
				<!-- </th> -->
				@else
				<!-- <th class="col-{{$value}}"> -->
				<input name="{{$value}}" id="{{$value}}" onchange='pageid();' value="" class="form-control input-sm hidden" style="width: 100%;padding: 3px;box-sizing: border-box;"></input>
				<!-- </th> -->
				@endif
				@endforeach
				<th class="col-action text-center hidden">Action</th>
			</tr>
		</tfooter>
	</table>
	<div class="row">
		<div class="col-md-8 text-left">Show {!! $DataTable->count() !!} of {!! $DataTable->total() !!} {!! str_replace('/?','?',$DataTable->appends(['search' => Input::get('search')])->appends(['sort' => Input::get('sort')])->render())!!}</div>
		<!-- <div class="col-md-4 text-right"><button class="btn btn-primary" onclick="saveAsExcel('tableToExcel', '{{$myModal->model_name}}.xls')"><i class="fa fa-file-excel-o"></i> Export </button></div> -->
	</div>
</div>
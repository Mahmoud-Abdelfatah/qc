<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="icon" href="{{asset('public/img/CallCenter24.png')}}" type="image/x-icon">
    <link rel="shortcut icon" href="{{asset('public/img/CallCenter24.png')}}" type="image/x-icon">
    <link rel="shortcut icon" href="{{asset('public/img/CallCenter24.png')}}" type="image/vnd.microsoft.icon">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Quality SYSTEM</title>
    <!-- Fonts -->
        <link href="{{asset('public/css/font-awesome.min.css')}}" rel="stylesheet"  type='text/css'>
        <link href="{{asset('public/css/familyLato.css')}}" rel="stylesheet"  type='text/css'>
    <!-- Styles -->
        <!-- <link href="{{asset('public/css/css/bootstrap-arabic.min.css')}}" rel="stylesheet"> -->
        <link href="{{asset('public/css/bootstrap.min.css')}}" rel="stylesheet">

        <link href="{{asset('public/css/jquery-confirm.min.css')}}" rel="stylesheet">
        <link href="{{asset('public/css/bootstrap-datetimepicker.css')}}" rel="stylesheet">
        <link href="{{asset('public/css/animate.min.css')}}" rel="stylesheet">
        <link href="{{asset('public/css/chosen.css')}}" rel="stylesheet" >
        <link href="{{asset('public/css/QC.css')}}" rel="stylesheet" >
        <link href="{{asset('public/css/select2.min.css')}}" rel="stylesheet" >
        {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}
    <!--script -->

        <script src="{{asset('public/js/jquery.min.js')}}"></script>
        <script src="{{asset('public/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('public/js/jquery-confirm.min.js')}}"></script>
        <script src="{{asset('public/js/jquery.validate.js')}}"></script>
        <script src="{{asset('public/js/moment-with-locales.js')}}"></script>
        <script src="{{asset('public/js/bootstrap-datetimepicker.js')}}"></script>
        <script src="{{asset('public/js/chosen.jquery.js')}}"></script>
        <script src="{{asset('public/js/main.js')}}"></script>
        <script src="{{asset('public/js/wow.min.js')}}"></script>
        <script src="{{asset('public/js/select2.min.js')}}"></script>
        {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
</head>
<body id="app-layout">
 @if (Auth::guest())
    @yield('content')    
 @else
    <nav class="navbar navbar-default navbar-fixed-top"> 
        <div class="container-fluid"> 
            <div class="navbar-header"> 
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-4" aria-expanded="false"> 
                    <span class="sr-only">Toggle navigation</span> 
                    <span class="icon-bar"></span> 
                    <span class="icon-bar"></span> 
                    <span class="icon-bar"></span> 
                </button> 
                <a class="navbar-brand" href="{{URL::route('Dashboard.index')}}"><img src="{{asset('public/img/CallCenter24.png')}}" /></a> 
            </div> 
            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <p class="navbar-text">Quality Management System</p>
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">Login</a></li>
                    @else
                    @role('Sadmin | admin | omanger | qmanger | osuper | qsuper | oleader | qspecia | osenior |  qobserver | oagent | vodadmin')
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Quality<span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{URL::route('Monitor.index')}}">{{trans('form.Monitor')}}</a></li>
                                <li><a href="{{URL::route('Monitor.test')}}">Monitor Search</a></li>
                                    <li><a href="{{URL::route('Coaching.index')}}">Open Coaching</a></li>
                                    <li><a href="{{URL::route('Additional.index')}}">Open Additional</a></li>
                                    <li><a href="{{URL::route('Coaching.close')}}">Close Coaching</a></li>
                                    <li><a href="{{URL::route('Additional.close')}}">Close Additional</a></li>
                            </ul>
                        </li>
                        @endrole
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Report<span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{URL::route('Report.index')}}">Report</a></li>
                                <li><a href="{{URL::route('Vreport.index')}}">View Report</a></li>
                            </ul>
                        </li>

                        @role('Sadmin')
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Seeting<span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{URL::route('Department.index')}}">{{trans('form.Department')}}</a></li>
                                
                                <li><a href="{{URL::route('Deduction.index')}}">{{trans('form.Deduction')}}</a></li>
                                <li><a href="{{URL::route('Location.index')}}">{{trans('form.location')}}</a></li>
                                <li><a href="{{URL::route('AttributeOption.index')}}">{{trans('form.AttributeOption')}}</a></li>
                                <li><a href="{{URL::route('Attribute.index')}}">{{trans('form.Attribute')}}</a></li>
                                <li><a href="{{URL::route('Section.index')}}">{{trans('form.Section')}}</a></li>
                                <li><a href="{{URL::route('Category.index')}}">{{trans('form.Category')}}</a></li>
                                <li><a href="{{URL::route('ScoreSheet.index')}}">{{trans('form.ScoreSheet')}}</a></li>
                            </ul>
                            @endrole
                        @role('admin || qmanger || qsuper || qobserver||Sadmin || vodadmin')    
                        </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Headcount<span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{URL('/Project')}}">      {{trans('form.Project')}}</a></li>
                                    <li><a href="{{URL('/Manager')}}">      Manager</a></li>
                                    <li><a href="{{URL('/Supervisor')}}">   Supervisor</a></li>
                                    <li><a href="{{URL('/Teamleader')}}">   Teamleader</a></li>
                                    <li><a href="{{URL('/Senior')}}">       Senior</a></li>
                                    <li><a href="{{URL('/Agent')}}">        Agent</a></li>
                                </ul>
                            </li>
                        @endrole
                        
                        @role('admin||Sadmin || vodadmin')
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{trans('form.UsersManagement')}}<span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{ url('/User') }}"><i class="fa fa-btn fa-user-plus"></i> {{trans('form.AddUser')}}</a></li>
                                </ul>
                            </li>
                        @endrole
                        <li class="dropdown btn-warning">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/') }}"><i class="fa fa-btn fa-sign-out"></i>Change password</a></li>
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div> 
    </nav>
    @yield('content')
 @endif
</body>
<script type="text/javascript">   

    function pageid(){
        //Get Table information
        var rows        = $('#rows :selected').text();
        var model       = $('#model').val();
        var search      = $('#search').val();
        var orderby     = $('#orderby').val();
        var ordertype   = $('#ordertype').val();
        var groupby     = $('#groupby').val();
        var key         = $('#key').val();
        var path        = $('#path').val();

        if ($('.pagination > .active span').text() != null){var page= $('.pagination > .active span').text();}else {var page = null;}
        if ($('#conditions') != null){var conditions= JSON.parse($('#conditions').val());}else {var conditions = null;}
        //filtratoin
        var columns = new Object();
        var div     = document.getElementById("tfooter");
       
        var selects = div.getElementsByTagName("select");
        for ( var i = 0; i < selects.length; i++ ) {
            var name    = $(selects[i]).attr("id");
            var value   = $("#"+name).find(":selected").val();
            if (value.length > 0) {columns[name] = value;}
        }
        
        var inputs  = div.getElementsByTagName("input");
        for ( var i = 0; i < inputs.length; i++ ) {
            var name    = $(inputs[i]).attr("id");
            var value   = $(inputs[i]).val();
            if (value.length > 0) {columns[name] = value;}
        }
        // Page loading Icon
        $(".table").html("<center><img src='{{asset('public/img/ajax-loader.gif')}}'></center>");
        //Go to Ajax Controller
        $.ajax({
            url     :"{{url('Ajaxtable?')}}",
            data    :{path:path,key:key,columns:columns,groupby:groupby,conditions:conditions,model:model,page:page,search:search,ordertype:ordertype,orderby:orderby,rows:rows},
            dataType:'json',
            type    :'get',
            success : function(data){$('.classPageID').html(data);}
        });
        return false;
    }

   

    $(document).on('click','.sort',function(e){                
        var sort = $('#ordertype').val();
        if (sort == 'desc') {
            $('#ordertype').val('asc');
        }else{
            $('#ordertype').val('desc');
        }
        $('#orderby').val($(this).attr('name'));
        pageid();
    });

    function saveAsExcel(id, fileName){
        var table_text="<table border='2px'><tr>"; //Table Intialization, CSS included
        var textRange; 
        var index=0; 
        var table = document.getElementById(id); // Read table using id
        /*
            Read Table Data and append to table_text
        */
        
        for(index = 0 ; index < table.rows.length ; index++) 
          {     
                table_text=table_text+table.rows[index].innerHTML+"</tr>";
                
          }

          table_text=table_text+"</table>"; // table close
          table_text= table_text.replace(/<a[^>]*>|<\/a>/g, ""); //removes links embedded in <td>
          table_text= table_text.replace(/<img[^>]*>/gi,"");  //removes images embeded in <td>
          table_text= table_text.replace(/<input[^>]*>|<\/input>/gi, ""); //removes input tag elements

          var userAgent = window.navigator.userAgent; //check client user agent to determine browser
          var msie = userAgent.indexOf("MSIE "); // If it is Internet Explorer user Aget will have string MSIE
          
         if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
          {
              //Since IE > 10 supports blob, check for blob support and use if we can
          if (typeof Blob !== "undefined") {
                //Bolb Data is ArrayStorage, convert to array
                table_text = [table_text];
                var blob = new Blob(table_text);
                window.navigator.msSaveBlob(blob, ''+fileName);
            }
            else{
                //If Blob is unsupported, create an iframe in HTML Page, and call that blank iframe
                
                textArea.document.open("text/html", "replace");
                textArea.document.write(table_text);
                textArea.document.close();
                textArea.focus();
                textArea.document.execCommand("SaveAs", true, fileName); 

            }
          }
          
            //Other Browsers         
           else  
               //Can use below statement if client machine has Excel Application installed
               //window.open('data:application/vnd.ms-excel,' + encodeURIComponent(table_text));  
               var a = document.createElement('a');
                //getting data from our div that contains the HTML table
                var data_type = 'data:application/vnd.ms-excel';
                var table_div = document.getElementById(id);
                var table_html = table_div.outerHTML.replace(/ /g, '%20');
                table_html = table_html.replace(/<a[^>]*>|<\/a>/g, "");
                a.href = data_type + ', ' + table_html;

        //setting the file name
        a.download = ''+fileName;
        //triggering the function
        a.click();
    }

    function DropDownlist(name,table_id,table,appendtolist,parameter1) {
        var id = $('#'+name+' :selected').attr( "id" );
        $.ajax({
                url     :"{{url('AjaxDropDownList?')}}",
                data    :{id:id,'table_id':table_id,table:table,appendtolist:appendtolist},
                dataType:'json',
                type    :'get',
                success : function(data){
                    //$('.classComplaint').html(data);
                    //alert(appendtolist);
                    var $option = $('#'+appendtolist);
                    $option.empty();
                    $option.append('<option value="" selected>Select</option>');
                    $.each(data, function(index, value) {
                        
                        $option.append('<option name="'+value[parameter1]+'" id="'+value.id+'">'+value.name+'</option>');
                    });
                    $('#'+appendtolist).trigger('chosen:updated');
                }
        });
        return false;
    };
    
    function GetRow(name,model,append0,value0,append1,value1,append2,value2) {
        var id = $('#'+name+' :selected').val();
        var model   = model;
        var CSRF_TOKEN = $('input[name="_token"]').val();
        $.ajax({
                url     :"{{url('/Ajaxrow')}}",
                data    :{id:id,'model':model,_token: CSRF_TOKEN},
                dataType:'json',
                type    :'post',
                success : function(data){
                    $('#'+append0).val(data[value0]);
                    $('#'+append1).val(data[value1]);
                    $('#'+append2).val(data[value2]);
                    total();
                }
        });
        return false;
    };

    var config = {
      '.chosen-select'           : {},
      '.chosen-select-deselect'  : {allow_single_deselect:true},
      '.chosen-select-no-single' : {disable_search_threshold:10},
      '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
      '.chosen-select-width'     : {width:"95%"}
    }
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    }

    $('#ShowEdit').on('show.bs.modal', function (event) {
        var button  = $(event.relatedTarget) // Button that triggered the modal
        var linkto  = button.data('link') // Extract info from data-* attributes
        var employee= button.data('employee') // Extract info from data-* attributes
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this)
        modal.find('.modal-title').text(employee)
        document.getElementById("myFrame").src = linkto;
        //modal.find('iframe').setAttribute("src", linkto)
        //modal.find('iframe').attr( "src", linkto );
    })

    $(document).on('click','.destroy',function(){ 
        var route   = $(this).data('route');
        var token   = $(this).data('token');
        $.confirm({
            icon                : 'glyphicon glyphicon-floppy-remove',
            animation           : 'rotateX',
            closeAnimation      : 'rotateXR',
            title               : 'OOps Delete Action',
            autoClose           : 'cancel|6000',
            content             : 'Are you sure you want to delete!',
            confirmButtonClass  : 'btn-danger',
            cancelButtonClass   : 'btn-primary',
            confirmButton       : 'Yes i agree',
            cancelButton        : 'NO never !',
            confirm: function () {
                $.ajax({
                    url     : route,
                    type    : 'post',
                    data    : {_method: 'delete', _token :token},
                    dataType:'json',           
                    success : function(data){
                        if(data == "لن يسمح لك بالحذف"){
                            alert(data);
                        }else{
                            $("#"+data).parents("tr").remove();
                        }
                    },
                });
            },
        });
    });

    $(document).on('click','.Show',function(){
        var route   = $(this).data('route');
        //var title   = $(this).data('title');
        $.confirm({
            title           : '@if(!empty($title)){{$title}}@endif',
            columnClass     : 'col-md-12',
            closeIcon       : true,
            content         : 'url:'+route,
            //animation       : 'top',
            closeAnimation  : 'bottom',
            animation       : 'zoom',
            
        });
    });

    $(".number").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    function limitText(field, maxChar)
    {
        //
        $(field).attr('maxlength',maxChar);
    }
</script>
</html>

<div class="container-fluid">
    @if (strpos(Request::route()->getName(), '.create') !== false)
            <h2 class="text-primary">Add {{$myModal->model_name}}</h2>
        @elseif(strpos(Request::route()->getName(), '.edit') !== false)
            <h2 class="text-primary">{{$myModal->model_name}} Details</h2>
    @elseif(strpos(Request::route()->getName(), '.show') !== false)
            <h2 class="text-primary">{{$myModal->model_name}} Details</h2>
        @else
    @endif
    <!-- display errors of validation -->
    @if ($errors->any())
      <div class="alert alert-danger">
          @foreach ($errors->all() as $error)
              {{ $error }}<br>        
          @endforeach
      </div>
    @endif

    @if (strpos(Request::route()->getName(), '.create') !== false)
       {!! Form::model($myModal,['data-toggle'=>'validator','id'=>'myForm','role'=>'form','method'=>'POST','route'=>str_replace(".create",".store",Request::route()->getName())])!!}
    @elseif(strpos(Request::route()->getName(), '.edit') !== false)
        {!! Form::model($EditData,['data-toggle'=>'validator','id'=>'myForm','role'=>'form','method'=>'PUT','route'=>  array(str_replace(".edit",".update",Request::route()->getName()),$EditData->id)])!!}
@elseif(strpos(Request::route()->getName(), '.show') !== false)
        {!! Form::model($ShowData,['data-toggle'=>'validator','id'=>'myForm','role'=>'form','method'=>'PUT','route'=>  array(str_replace(".show",".update",Request::route()->getName()),$ShowData->id)])!!}
    @else
    @endif
            <div id="formbody">

                @foreach($myModal->getTablColumns() as $key => $value)
                    @if     ($value->Comment    == 'disable')
                    @elseif ($value->Comment    == 'hidden')
                        <!--  -->
                        {!! Form::hidden($value->Field, null , ['class' => 'form-control','id' => $value->Field,'required' => 'required']) !!}
                    @elseif ($value->Comment    == 'relationship')
                        <div class="{{$value->Field}} col-md-3">
                            <div class="form-group">
                                <label for="{{$value->Field}}">{{ trans('form.'.$value->Field) }}</label>                              
                                <div class="check_{{$value->Field}}">
                                @if($value->Null == 'YES')
                                    {!! Form::select($value->Field,[null=>'Please Select'] + eval('return $'. $value->Field . ';'),null,['class' => 'form-control ','id' => $value->Field])!!}
                                @else
                                    {!! Form::select($value->Field,[null=>'Please Select'] + eval('return $'. $value->Field . ';'),null,['class' => 'form-control , ssearch','id' => $value->Field,'required' =>'required'])!!}
                                @endif
                                </div>

                                <script type="text/javascript">
                                    $(document).ready(function() {
                                            $('.ssearch').select2();
                                      });
                                </script>
                            </div>
                        </div>                    
                    @elseif ($value->Comment    == 'list')
                        <div class="{{$value->Field}} col-md-3">
                            <div class="form-group">
                                <label for="{{$value->Field}}">{{ trans('form.'.$value->Field) }}</label>                              
                                <div class="check_{{$value->Field}}">
                                    @if($value->Null == 'YES')
                                        {!! Form::select(''.$value->Field.'[]',eval('return $'. $value->Field . ';'),null,['class' => 'form-control chosen-select','id' => $value->Field,'multiple'=>'multiple'])!!}
                                    @else
                                        {!! Form::select(''.$value->Field.'[]',eval('return $'. $value->Field . ';'),null,['class' => 'form-control chosen-select','id' => $value->Field,'multiple'=>'multiple','required' =>'required'])!!}
                                    @endif
                                </div>
                            </div>
                        </div>                    
                    @elseif ($value->Comment    == 'file')
                        <div class="{{$value->Field}} col-md-3">
                            <div class="form-group">
                                <label for="{{$value->Field}}">{{ trans('form.'.$value->Field) }}</label>
                                <div class="check_{{$value->Field}}">
                                    @if($value->Null == 'YES')
                                        {!! Form::file($value->Field , ['class' => 'form-control','id' => $value->Field]) !!}
                                    @else
                                        {!! Form::file($value->Field , ['class' => 'form-control','id' => $value->Field,'required' => 'required']) !!}
                                    @endif
                                </div>
                            </div>
                        </div>  
                    @elseif ($value->Comment    =='password')
                        <div class="{{$value->Field}} col-md-3">
                            <div class="form-group">
                                <label for="{{$value->Field}}">{{ trans('form.'.$value->Field) }}</label>
                                <div class="check_{{$value->Field}}">
                                @if($value->Null == 'YES')
                                    {!! Form::password($value->Field, ['class' => 'form-control','id' => $value->Field]) !!}
                                @else
                                    {!! Form::password($value->Field, ['class' => 'form-control','id' => $value->Field,'required' => 'required']) !!}
                                @endif
                                </div>
                            </div>
                        </div>
                    @elseif ($value->Type       == 'date')
                        <div class="{{$value->Field}} col-md-3">
                            <div class="form-group">
                                <label for="{{$value->Field}}">{{ trans('form.'.$value->Field) }} </label>
                                <div class='check_{{$value->Field}} input-group' id='{{$value->Field}}'>
                                    @if($value->Null == 'YES')
                                        {!! Form::text($value->Field, null , ['class' => 'form-control','id' => 'date-'.$value->Field]) !!}
                                    @else
                                        {!! Form::text($value->Field, null , ['class' => 'form-control','id' => 'date-'.$value->Field,'required' => 'required']) !!}
                                    @endif
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            <script type="text/javascript">$(function () {$('#{{$value->Field}}').datetimepicker({format: 'YYYY-MM-DD',viewMode: 'years'});});</script>
                        </div>
                    @elseif ($value->Type       == 'text')
                        <div class="{{$value->Field}} col-md-3">
                            <div class="form-group">
                                <label for="{{$value->Field}}">{{ trans('form.'.$value->Field) }}</label>
                                <div class="check_{{$value->Field}}">
                                @if($value->Null == 'YES')
                                    {{ Form::textarea($value->Field, null, ['class' => 'form-control','id' => $value->Field,'size' => '30x5']) }}
                                @else                                    
                                    {{ Form::textarea($value->Field, null, ['class' => 'form-control','id' => $value->Field,'size' => '30x5','required' =>'required']) }}
                                @endif
                                </div>
                            </div>
                        </div> 
                    @elseif (strpos($value->Type, 'enum') !== false)
                        <?php
                        $remove = str_replace("enum(", "",$value->Type );
                        $remove = str_replace(")", "",$remove );
                        $remove = str_replace("'", "",$remove );
                        //$array  = explode(",",$remove);
                        $array = explode(',', $remove);
                        $array = array_combine($array, $array);
                        ?>
                        <div class="{{$value->Field}} col-md-3">
                            <div class="form-group">
                                <label for="{{$value->Field}}">{{ trans('form.'.$value->Field) }}</label>                              
                                <div class="check_{{$value->Field}}">
                                    @if($value->Null == 'YES')
                                        {!! Form::select($value->Field,[null=>'Please Select'] + $array,null,['class' => 'form-control ','id' => $value->Field])!!}
                                    @else
                                        @role('vodadmin | Sadmin')
                                        <?php
                                        if ($value->Field=='call_type') {
                                            unset($array);
                                            $array=array();
                                            $array['*211# Offer'] ='*211# Offer';
                                            $array['@Home USB'] = '@Home USB';

$array['14 PT rate plan'] ='    14 PT rate plan ';
$array['2012 Bouquet'] ='   2012 Bouquet    ';
$array['2013 Business ElAsly'] ='   2013 Business ElAsly    ';
$array['2014 ElAsly plus'] ='   2014 ElAsly plus    ';
$array['365 Offers Program'] =' 365 Offers Program  ';
$array['3al Nota'] ='   3al Nota    ';
$array['3ala 7esaby Service'] ='    3ala 7esaby Service ';
$array['4G'] =' 4G  ';
$array['ACP'] ='    ACP ';
$array['vADSL'] ='  ADSL    ';
$array['ADSL Billing'] ='   ADSL Billing    ';
$array['ADSL Deactivation'] ='  ADSL Deactivation   ';
$array['ADSL inquiry'] ='   ADSL inquiry    ';
$array['ADSL Slow Performance'] ='  ADSL Slow Performance   ';
$array['ADSL Wifi'] ='  ADSL Wifi   ';
$array['Advanced Credit service'] ='    Advanced Credit service     ';
$array['Alerting Service'] ='   Alerting Service    ';
$array['Ana Vodafone App'] ='   Ana Vodafone App    ';
$array['Anghami Plus'] ='   Anghami Plus    ';
$array['App Problem'] ='    App Problem ';
$array['Authorized'] =' Authorized  ';
$array['Authorized Request'] =' Authorized Request  ';
$array['Balance Dispute'] ='    Balance Dispute ';
$array['Balance Inq'] ='    Balance Inq ';
$array['Balance Transfer'] ='   Balance Transfer    ';
$array['Barring'] ='    Barring ';
$array['Behavior'] ='   Behavior    ';
$array['Bill Amount'] ='    Bill Amount ';
$array['Bill by Email'] ='  Bill by Email   ';
$array['Bill Cycle'] =' Bill Cycle  ';
$array['Bill Explanation'] ='   Bill Explanation    ';
$array['Bill Payment'] ='   Bill Payment    ';
$array['Bill payment through Recharge'] ='  Bill payment through Recharge   ';
$array['Bill Problem'] ='   Bill Problem    ';
$array['Bill request'] ='   Bill request    ';
$array['BlackBerry'] =' BlackBerry  ';
$array['Blacklist / Whitelist'] ='  Blacklist / Whitelist   ';
$array['Business Add-ons'] ='   Business Add-ons    ';
$array['Business Bouquet'] ='   Business Bouquet    ';
$array['Business flex'] ='  Business flex   ';
$array['Business Small Bundles'] =' Business Small Bundles  ';
$array['Buy Validity'] ='   Buy Validity    ';
$array['Call Divert'] ='    Call Divert     ';
$array['Call Tone'] ='  Call Tone   ';
$array['Call Waiting'] ='   Call Waiting    ';
$array['Cash back offer'] ='    Cash back offer ';
$array['Cash Refund'] ='    Cash Refund ';
$array['OCC'] ='    OCC ';
$array['c-cat Disconnection'] ='    c-cat Disconnection ';
$array['CCAT Reload'] ='    CCAT Reload ';
$array['Churn/conversion to pre'] ='    Churn/conversion to pre ';
$array['Churn/Deactivation'] =' Churn/Deactivation  ';
$array['Churn-Request Not done'] =' Churn-Request Not done  ';
$array['Collection SMS'] =' Collection SMS  ';
$array['Complaint'] ='  Complaint   ';
$array['Confidentiality '] ='   Confidentiality     ';
$array['Confirm payment'] ='    Confirm payment ';
$array['Confirming Data'] ='    Confirming Data ';
$array['Confirming Data For Sim Swap'] ='   Confirming Data For Sim Swap    ';
$array['Content service '] ='   Content service     ';
$array['Conversion'] =' Conversion  ';
$array['Conversion to Buss'] =' Conversion to Buss  ';
$array['Customer Satisfaction'] ='  Customer Satisfaction   ';
$array['Data Collection Campaign'] ='   Data Collection Campaign    ';
$array['Data Entry'] =' Data Entry  ';
$array['Data Entry new process '] ='    Data Entry new process  ';
$array['Data Inquiry'] ='   Data Inquiry    ';
$array['Data Lines'] =' Data Lines  ';
$array['Data Roaming Bundles'] ='   Data Roaming Bundles    ';
$array['Deactivation Request'] ='   Deactivation Request    ';
$array['Delay Activation'] ='   Delay Activation    ';
$array['Delay conversion'] ='   Delay conversion    ';
$array['Deny of Usage'] ='  Deny of Usage   ';
$array['Disconnection'] ='  Disconnection   ';
$array['Divert '] ='    Divert  ';
$array['Due Amount'] =' Due Amount  ';
$array['Due Date '] ='  Due Date    ';
$array['E- SIM'] =' E- SIM  ';
$array['El Fakka Card'] ='  El Fakka Card   ';
$array['El Mared card'] ='  El Mared card   ';
$array['El3alam been edek'] ='  El3alam been edek   ';
$array['End user authority'] =' End user authority  ';
$array['Engezly App'] ='    Engezly App ';
$array['Ent. EOCN'] ='  Ent. EOCN   ';
$array['Enterprise New USB'] =' Enterprise New USB  ';
$array['Enterprise Sallefny Recharge'] ='   Enterprise Sallefny Recharge    ';
$array['E-Payment'] ='  E-Payment   ';
$array['Erase HL for Deal'] ='  Erase HL for Deal   ';
$array['Escalation to complaint'] ='    Escalation to complaint ';
$array['Eshra7ly Raseedi Service'] ='   Eshra7ly Raseedi Service    ';
$array['Extend deal'] ='    Extend deal ';
$array['Extreme Internet Bundles'] ='   Extreme Internet Bundles    ';
$array['Fawry'] ='  Fawry   ';
$array['Flex Extra'] =' Flex Extra  ';
$array['Follow Up'] ='  Follow Up   ';
$array['Follow up Request'] ='  Follow up Request   ';
$array['Fraud'] ='  Fraud   ';
$array['Fraud Sim Swap'] =' Fraud Sim Swap  ';
$array['Free Calls For 10 Years'] ='    Free Calls For 10 Years ';
$array['Handset'] ='    Handset ';
$array['Harassment Calls'] ='   Harassment Calls    ';
$array['HLR'] ='    HLR ';
$array['Hotline'] ='    Hotline ';
$array['International'] ='  International   ';
$array['IP Cam'] =' IP Cam  ';
$array['IP Cam Offer'] ='   IP Cam Offer    ';
$array['Itemized Bill'] ='  Itemized Bill   ';
$array['IVR/USSD errors'] ='    IVR/USSD errors ';
$array['IVR/USSD Inq'] ='   IVR/USSD Inq    ';
$array['MCK'] ='    MCK ';
$array['MI Bundles /Addons'] =' MI Bundles /Addons  ';
$array['MI Connection'] ='  MI Connection   ';
$array['MI Deny Usage'] ='  MI Deny Usage   ';
$array['MI FI '] =' MI FI   ';
$array['MI Targeted offer'] ='  MI Targeted offer   ';
$array['MIFI'] ='   MIFI    ';
$array['MIFI Offer'] =' MIFI Offer  ';
$array['MNP'] ='    MNP ';
$array['Mobile Tracing'] =' Mobile Tracing  ';
$array['Multi SIM'] ='  Multi SIM   ';
$array['Multiparty'] =' Multiparty  ';
$array['My 010'] =' My 010  ';
$array['Network complaint'] ='  Network complaint   ';
$array['New 2012 Business Bouquet'] ='  New 2012 Business Bouquet   ';
$array['New Business Bouquet'] ='   New Business Bouquet    ';
$array['New Line activation'] ='    New Line activation ';
$array['New Pass Bundles'] ='   New Pass Bundles    ';
$array['New Roaming bundles'] ='    New Roaming bundles ';
$array['OCC'] ='    OCC ';
$array['Offers / Promotions'] ='    Offers / Promotions ';
$array['Others'] =' Others  ';
$array['Overscratch'] ='    Overscratch ';
$array['Own The Case'] ='   Own The Case    ';
$array['Payment Deal'] ='   Payment Deal    ';
$array['Payment Methods'] ='    Payment Methods ';
$array['Pending Balance '] ='   Pending Balance     ';
$array['PIN / PUK'] ='  PIN / PUK   ';
$array['Policy/Lost\Stolen'] =' Policy/Lost\Stolen  ';
$array['Prepaid line'] ='   Prepaid line    ';
$array['Promo problem'] ='  Promo problem   ';
$array['Promotions / Campaigns'] =' Promotions / Campaigns  ';
$array['Rate Plan'] ='  Rate Plan   ';
$array['Ready Business'] =' Ready Business  ';
$array['Ready business complaint'] ='   Ready business complaint    ';
$array['Ready Digital'] ='  Ready Digital   ';
$array['Recharge'] ='   Recharge    ';
$array['Recharge Cards'] =' Recharge Cards  ';
$array['Recharge on bill'] ='   Recharge on bill    ';
$array['Recharge Problem'] ='   Recharge Problem    ';
$array['Red Business'] ='   Red Business    ';
$array['Red offers'] ='     Red offers  ';
$array['Relase data'] ='    Relase data ';
$array['Releasing Dialed Calls'] =' Releasing Dialed Calls  ';
$array['Releasing Received Calls'] ='   Releasing Received Calls    ';
$array['Remove HL after payment'] ='    Remove HL after payment ';
$array['Retail'] =' Retail  ';
$array['Retail wrong transaction'] ='   Retail wrong transaction    ';
$array['Risk'] ='   Risk    ';
$array['Roaming'] ='    Roaming ';
$array['Sales Issue/ inquiry'] ='   Sales Issue/ inquiry    ';
$array['Salfeny sevices'] ='    Salfeny sevices ';
$array['Service Charges / Fees'] =' Service Charges / Fees  ';
$array['Sherkety Points & Offers Prog'] ='  Sherkety Points & Offers Prog   ';
$array['SIM Problem '] ='   SIM Problem     ';
$array['SIM Swap'] ='   SIM Swap    ';
$array['SMS'] ='    SMS ';
$array['Static IP'] ='  Static IP   ';
$array['Store Location and working hou'] =' Store Location and working hou  ';
$array['Suspension '] ='    Suspension  ';
$array['Taxes'] ='  Taxes   ';
$array['Telcome Egypt Offer '] ='   Telcome Egypt Offer     ';
$array['Transfer of ownership Request/Dealy'] ='    Transfer of ownership Request/Dealy ';
$array['USB Billing'] ='    USB Billing ';
$array['USB Browsing'] ='   USB Browsing    ';
$array['USB Buying ADD-ONs'] =' USB Buying ADD-ONs  ';
$array['USB Connection'] =' USB Connection  ';
$array['USB Extreme Bundles'] ='    USB Extreme Bundles ';
$array['USB modem'] ='  USB modem   ';
$array['USB Renewal'] ='    USB Renewal ';
$array['USB Speed problem'] ='  USB Speed problem   ';
$array['VBP'] ='    VBP ';
$array['VF Advertising SMS'] =' VF Advertising SMS  ';
$array['VF alerting service'] ='    VF alerting service ';
$array['VF ElSanawy Rate Plan'] ='  VF ElSanawy Rate Plan   ';
$array['Video Call'] =' Video Call  ';
$array['Vodafone Cash'] ='  Vodafone Cash   ';
$array['Vodafone Red Business'] ='  Vodafone Red Business   ';
$array['Vodafone Red Consumer'] ='  Vodafone Red Consumer   ';
$array['Vodafone Red Points'] ='    Vodafone Red Points ';
$array['Voice Mail '] ='    Voice Mail  ';
$array['Voice reminder'] =' Voice reminder  ';
$array['Waiting'] ='    Waiting ';
$array['Website/App'] ='    Website/App ';
$array['WI-FI'] ='  WI-FI   ';
$array['Wrong Data'] =' Wrong Data  ';
$array['Wrong information'] ='  Wrong information   ';
$array['Wrong Recharge'] =' Wrong Recharge  ';
$array['Wrong Transaction'] ='  Wrong Transaction   ';



                                        }
                                        
                                        ?>
                                        @endrole
                                        {!! Form::select($value->Field,[null=>'Please Select'] + $array,null,['class' => 'form-control','id' => $value->Field,'required' =>'required'])!!}
                                    @endif
                                </div>
                            </div>
                        </div>
                    @elseif (strpos($value->Type, 'int' ) !== false  || strpos($value->Type, 'decimal' ) !== false || strpos($value->Type, 'bigint' ) !== false)
                       <div class="{{$value->Field}} col-md-3">
                            <div class="form-group">
                                <label for="{{$value->Field}}">{{ trans('form.'.$value->Field) }}</label>
                                <div class="check_{{$value->Field}}">
                                    @if($value->Null == 'YES')
                                        {!! Form::text($value->Field, null , ['class' => 'number form-control','id' => $value->Field]) !!}
                                    @else
                                        {!! Form::text($value->Field, null , ['class' => 'number form-control','id' => $value->Field,'required' => 'required']) !!}
                                    @endif
                                </div>
                            </div>
                        </div>                                       
                    @elseif (1==1)
                        <div class="{{$value->Field}} col-md-3">
                            <div class="form-group">
                                <label for="{{$value->Field}}">{{ trans('form.'.$value->Field) }}</label>
                                <div class="check_{{$value->Field}}">
                                    @if($value->Null == 'YES')
                                        {!! Form::text($value->Field, null , ['class' => 'form-control','id' => $value->Field]) !!}
                                    @else
                                        {!! Form::text($value->Field, null , ['class' => 'form-control','id' => $value->Field,'required' => 'required']) !!}
                                    @endif

                                </div>
                            </div>
                        </div> 
                    @endif
                @endforeach  
            </div>
            <div class="col-md-12 text-right" id="footer">
                @if (strpos(Request::route()->getName(), '.create') !== false || strpos(Request::route()->getName(), '.edit') !== false)
                <button type="submit" class="btn btn-primary">{{ucfirst(substr(Request::route()->getName(), strpos(Request::route()->getName(),'.')+strlen('.')))}}</button>
                @endif
            </div>

</div>
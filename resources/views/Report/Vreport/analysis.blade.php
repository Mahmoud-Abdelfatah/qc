<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
    table{
        width:100%; 
        border-collapse:collapse; 
    }
    /* provide some minimal visual accomodation for IE8 and below */
    table th{
    background:#366092;
    color: #FFFFFF;
    }

    /*  Define the background color for all the ODD background rows  */
    table td:nth-child(odd){ 
        background:#CCCCCC;
    }
    /*  Define the background color for all the EVEN background rows  */
    table td:nth-child(even){
        background:#FFFFFF;
    }
</style>
</head>

<body>
<?php
echo '<table border="1">';
    echo '<theader><tr>';
        echo '<th rowspan="3">id</th>';
        echo '<th rowspan="3">projects</th>';
        echo '<th rowspan="3">supervisor</th>';
        echo '<th rowspan="3">teamleader</th>';
        echo '<th rowspan="3">id_staff</th>';
        echo '<th rowspan="3">CRM_User</th>';
        echo '<th rowspan="3">Agent</th>';
        echo '<th rowspan="3">Standard</th>';
        echo '<th rowspan="3">additional</th>';
        echo '<th rowspan="3">Kind_of_observe</th>';
        echo '<th rowspan="3">Nature_of_the_call</th>';
        echo '<th rowspan="3">Call_Type</th>';
        echo '<th rowspan="3">Customer_Name</th>';
        echo '<th rowspan="3">Phone_Number</th>';
        echo '<th rowspan="3">Call_Date</th>';
        echo '<th rowspan="3">call_time</th>';          
        echo '<th rowspan="3">call_duration</th>';
        echo '<th rowspan="3">hold_duration</th>';
        echo '<th rowspan="3">fcr</th>';
        echo '<th rowspan="3">fcr_reason</th>';
        echo '<th rowspan="3">Observer_Name</th>';
        echo '<th rowspan="3">BCE</th>';
        echo '<th rowspan="3">ECE</th>';
        echo '<th rowspan="3">NCE</th>';
        echo '<th rowspan="3">Results_of_calls</th>';
        echo '<th rowspan="3">Overall</th>';
        foreach ($DataTable->score_sheet->category as $Groups){
            foreach ($Groups->section as $section){
                $countsection = $section->Attribute()->count() * 2;
                echo '<th colspan="'.$countsection.'">'.$Groups->name.'</th>';
            }
        }
    echo '</tr><tr>';
        foreach ($DataTable->score_sheet->category as $Groups){
            foreach ($Groups->section as $section){
                $countsection = $section->Attribute()->count() * 2;
                echo '<th colspan="'.$countsection.'">'.$section->name.'</th>';
            }
        }
    echo '</tr><tr>';
        foreach ($DataTable->score_sheet->category as $Groups){
            foreach ($Groups->section as $section){
                foreach($section->Attribute as $attribute){
                    print_r ("<th colspan='2'>$attribute->name</th>");
                }
            }
        }
    echo '</tr></theader>';
    echo '<tboody><tr>';
        foreach ($Report  as $key => $value) {
            echo '<td>'.$value->id.'</td>';
            echo '<td>'.$value->project.'</td>';
            echo '<td>'.$value->supervisor.'</td>';
            echo '<td>'.$value->teamleader.'</td>';
            echo '<td>'.$value->id_staff.'</td>';
            echo '<td>'.$value->CRM_User.'</td>';
            echo '<td>'.$value->agent.'</td>';
            echo '<td>'.$value->Standard.'</td>';
            echo '<td>'.$value->additional.'</td>';
            echo '<td>'.$value->Kind_of_observe.'</td>';
            echo '<td>'.$value->Nature_of_the_call.'</td>';
            echo '<td>'.$value->Call_Type.'</td>';
            echo '<td>'.$value->Customer_Name.'</td>';
            echo '<td>'.$value->Phone_Number.'</td>';
            echo '<td>'.$value->Call_Date.'</td>';
            echo '<td>'.$value->call_time.'</td>';            
            echo '<td>'.$value->call_duration.'</td>';
            echo '<td>'.$value->hold_duration.'</td>';
            echo '<td>'.$value->fcr.'</td>';
            echo '<td>'.$value->fcr_reason.'</td>';
            echo '<td>'.$value->Observer_Name.'</td>';
            echo '<td>'.$value->BCE.'</td>';
            echo '<td>'.$value->ECE.'</td>';
            echo '<td>'.$value->NCE.'</td>';
            echo '<td>'.$value->Results_of_calls.'</td>';
            echo '<td>'.$value->Overall.'</td>';
            $result = $value->id;
                foreach ($DataTable->score_sheet->category as $Groups){
                    foreach ($Groups->section as $section){
                        foreach ($section->Attribute as $Attribute){
                            $results    = \App\Monitor::find($result);
                            $TableName  = $DataTable->score_sheet->name.'_'.$results->project_id;
                            $option     ='attribute_option_id_'.$Attribute->id;
                            $description='description_id_'.$Attribute->id;
                            $ScoreSheet = DB::table($TableName)->where('monitor_id',$value->id)->get();
                            //echo $option;
                           
                            echo '<td title="'.$Attribute->name.'">'.$ScoreSheet[0]->$option.'</td>';
                             echo '<td>'.$ScoreSheet['0']->$description.'</td>';
                        }
                    }
                }
                
        echo "</tr></tboody>";
        }
echo '</table>';
header('Content-type: application/vnd.ms-excel');
//header("Content-Disposition: attachment; filename='Analysis.xls'");
//$objWriter->save('php://output'); 
?>
</body>
</html>
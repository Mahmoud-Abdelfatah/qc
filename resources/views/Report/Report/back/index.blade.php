@extends('layouts.app')
@section('content')
<br/>
<div class="container-fluid">
	<!--Index-->
	<form action="{{URL::route('Report.store')}}" data-toggle="validator"  id='myForm' role="form" method="POST">
		{!! csrf_field() !!}
		<div class="col-md-8 col-md-push-2">
			<div class="panel panel-default">
				<!-- Default panel contents -->
				<div class="panel-heading">
					<div class="row">
						<div class="col-lg-6"><h5>Adherence Report</h5></div>
					</div>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-6">
					        <div class="form-group">
					            <div class='input-group date' id='startdate'>
					                <input type='text' class="form-control" name="startdate" required="required"/>
					                <span class="input-group-addon">
					                    <span class="glyphicon glyphicon-calendar"></span>
					                </span>
					            </div>
					        </div>
					    </div>
					    <div class="col-md-6">
					        <div class="form-group">
					            <div class='input-group date' id='enddate'>
					                <input type='text' class="form-control" name="enddate" required="required"/>
					                <span class="input-group-addon">
					                    <span class="glyphicon glyphicon-calendar"></span>
					                </span>
					            </div>
					        </div>
					    </div>
			        </div>
			        <div class="row">
			        	<div class="col-md-4 col-md-push-4">
			        		<div class="form-group">
					                <select name="TypeOfReport" id="TypeOfReport" class="chosen-select form-control" required="required" onchange="DropDown('TypeOfReport','searchvalue')">
					                	<option></option>
					                	@foreach($array as $key => $value)
											<option id="{{$key}}">{{$value}}</option>
										@endforeach
					                </select>
					        </div>
					        <div class="form-group">
				                <select name="searchvalue" id="searchvalue"  data-placeholder="Select Your Interval" class="chosen-select form-control" required="required">
				                </select>
					        </div>
			        	</div>
			        </div>
				</div>
				<div class="panel-footer text-right">
					<button type="submit" class="btn btn-primary">Generate Report</button>
				</div>
			</div>
		</div>	
	</form>
	<!--End Index-->
</div>
<script type="text/javascript">
    $(function () {
        $('#startdate').datetimepicker({
        	format: 'YYYY-MM-DD HH:mm:ss',
        	defaultDate: ' {{date("Y-m-d")}} 00:00:00'
        });
        $('#enddate').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            defaultDate: ' {{date("Y-m-d")}} 23:59:59',
            useCurrent: false //Important! See issue #1075
        });
        $("#startdate").on("dp.change", function (e) {
            $('#enddate').data("DateTimePicker").minDate(e.date);
        });
        $("#enddate").on("dp.change", function (e) {
            $('#startdate').data("DateTimePicker").maxDate(e.date);
        });
    });

    function DropDown(name,appendtolist) {
        //var table = $('#'+name+' :selected').val();
        var table = $('#'+name+' :selected').attr( "id" );
        //remove _i from value
        $.ajax({
                url     :"{{url('AjaxDropDown?')}}",
                data    :{table:table,appendtolist:appendtolist},
                dataType:'json',
                type    :'get',
                success : function(data){
                    //$('.classComplaint').html(data);
                    //alert(appendtolist);
                    var $option = $('#'+appendtolist);
                    $option.empty();
                    $option.append('<option value="" selected>Select</option>');
                    $.each(data, function(index, value) {
                        
                        $option.append('<option value="'+value.id+'" id="'+value.id+'">'+value.name+'</option>');
                    });
                    $('#'+appendtolist).trigger('chosen:updated');
                }
        });
        return false;
    };
</script>
@endsection







<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
	table{
		width:100%; 
		border-collapse:collapse; 
	}
	/* provide some minimal visual accomodation for IE8 and below */
	table th{
	background:#366092;
	color: #FFFFFF;
	}

	/*  Define the background color for all the ODD background rows  */
	table td:nth-child(odd){ 
		background:#CCCCCC;
	}
	/*  Define the background color for all the EVEN background rows  */
	table td:nth-child(even){
		background:#FFFFFF;
	}
</style>
</head>

<body>
<?php
echo '<table border="1">';
	echo '<tr>';
		echo '<th rowspan="3">id</th>';
        echo '<th rowspan="3">projects</th>';
        echo '<th rowspan="3">supervisor</th>';
        echo '<th rowspan="3">teamleader</th>';
        echo '<th rowspan="3">id_staff</th>';
        echo '<th rowspan="3">CRM_User</th>';
        echo '<th rowspan="3">Agent</th>';
        echo '<th rowspan="3">Agent Type</th>';        
        echo '<th rowspan="3">Standard</th>';
        echo '<th rowspan="3">additional</th>';
        echo '<th rowspan="3">Kind_of_observe</th>';
        echo '<th rowspan="3">Nature_of_the_call</th>';
        echo '<th rowspan="3">Call_Type</th>';
        echo '<th rowspan="3">Customer_Name</th>';
        echo '<th rowspan="3">Phone_Number</th>';
        echo '<th rowspan="3">Call_Date</th>';
        echo '<th rowspan="3">call_time</th>';                    
        echo '<th rowspan="3">call_duration</th>';
        echo '<th rowspan="3">hold_duration</th>';
        echo '<th rowspan="3">fcr</th>';
        echo '<th rowspan="3">fcr_reason</th>';
        echo '<th rowspan="3">customer_satisfaction</th>';
        echo '<th rowspan="3">Observer_Name</th>';
        echo '<th rowspan="3">BCE</th>';
        echo '<th rowspan="3">ECE</th>';
        echo '<th rowspan="3">NCE</th>';
        echo '<th rowspan="3">Results_of_calls</th>';
        echo '<th rowspan="3">Overall</th>';
        foreach ($DataTable->score_sheet->category as $Groups){
            foreach ($Groups->section as $section){
                $countsection = $section->Attribute()->count() * 2;
                echo '<th colspan="'.$countsection.'">'.$Groups->name.'</th>';
            }
        }
    echo '</tr><tr>';
        foreach ($DataTable->score_sheet->category as $Groups){
            foreach ($Groups->section as $section){
                $countsection = $section->Attribute()->count() * 2;
                echo '<th colspan="'.$countsection.'">'.$section->name.'</th>';
            }
        }
    echo '</tr><tr>';
        foreach ($DataTable->score_sheet->category as $Groups){
            foreach ($Groups->section as $section){
                foreach($section->Attribute as $attribute){
                    print_r ("<th colspan='2'>$attribute->name</th>");
                }
            }
        }
    echo '</tr></theader>';

               
     $agent_name='';    
     $call_count=1;      
foreach ($results as $key => $value) {
        if($agent_name=='')
        {
            $agent_name=$value->name;

        }
   if($agent_name==$value->name)
   {
       if($value->agent_type==1 && $call_count<=4)
       {
        echo '<tr>';
        echo '<td>'.$value->id.'</td>';
        echo '<td>'.$value->project.'</td>';
        echo '<td>'.$value->super.'</td>';
        echo '<td>'.$value->leader.'</td>';
        echo '<td>'.$value->id_staff.'</td>';
        echo '<td>'.$value->crm_user.'</td>';
        echo '<td>'.$value->name.'</td>';
        echo '<td>'."New_comer".'</td>';   
        if ($value->monitoring_type=='Standard')
 {
     echo '<td>'."1".'</td>';
     echo '<td>'."0".'</td>';
 }else{

      echo '<td>'."0".'</td>';
      echo '<td>'."1".'</td>';   
 }           
     echo '<td>'.$value->observe_type.'</td>';  
     echo '<td>'.$value->call_nature.'</td>';  
     echo '<td>'.$value->call_type.'</td>';
     echo '<td>'.$value->customer_name.'</td>';
     echo '<td>'.$value->phone_number.'</td>';
     echo '<td>'.$value->call_date.'</td>';
     echo '<td>'.$value->call_time.'</td>';              
     echo '<td>'.$value->call_duration.'</td>';
     echo '<td>'.$value->hold_duration.'</td>';
     echo '<td>'.$value->fcr.'</td>';
     echo '<td>'.$value->fcr_reason .'</td>';
     echo '<td>'.$value->customer_satisfaction .'</td>'; 
     echo '<td>'.$value->Observer_Name .'</td>';    
    echo '<td>'.$value->bce.'</td>';   
    echo '<td>'.$value->ece.'</td>';
    echo '<td>'.$value->nce.'</td>'; 
    echo '<td>'.$value->total_score.'</td>';
    echo '<td>'.$value->result.'</td>';     
        $call_count++;
            $monitor = $value->id;
                foreach ($DataTable->score_sheet->category as $Groups){
                    foreach ($Groups->section as $section){
                        foreach ($section->Attribute as $Attribute){
                            $monitor_results    = \App\Monitor::find($monitor);
                            $TableName  = $DataTable->score_sheet->name.'_'.$monitor_results->project_id;
                            $option     ='attribute_option_id_'.$Attribute->id;
                            $description='description_id_'.$Attribute->id;
                            $ScoreSheet = DB::table($TableName)->where('monitor_id',$value->id)->get();
                            //echo $option;
                           
                            echo '<td title="'.$Attribute->name.'">'.$ScoreSheet[0]->$option.'</td>';
                            echo '<td>'.$ScoreSheet['0']->$description.'</td>';
                        }
                    }
                }

       }
       elseif($value->agent_type!=1)
       {
        echo '<tr>';
        echo '<td>'.$value->id.'</td>';
        echo '<td>'.$value->project.'</td>';
        echo '<td>'.$value->super.'</td>';
        echo '<td>'.$value->leader.'</td>';
        echo '<td>'.$value->id_staff.'</td>';
        echo '<td>'.$value->crm_user.'</td>';
        echo '<td>'.$value->name.'</td>'; 
        if($value->agent_type==2){
                echo '<td>'."Staff".'</td>'; 
            }else{
               echo '<td>'.$value->agent_type.'</td>';   
            }
if ($value->monitoring_type=='Standard')
 {
     echo '<td>'."1".'</td>';
     echo '<td>'."0".'</td>';
 }else{

      echo '<td>'."0".'</td>';
      echo '<td>'."1".'</td>';   
 }           
     echo '<td>'.$value->observe_type.'</td>';  
     echo '<td>'.$value->call_nature.'</td>';  
     echo '<td>'.$value->call_type.'</td>';
     echo '<td>'.$value->customer_name.'</td>';
     echo '<td>'.$value->phone_number.'</td>';
     echo '<td>'.$value->call_date.'</td>';
     echo '<td>'.$value->call_time.'</td>';
     echo '<td>'.$value->call_duration.'</td>';
     echo '<td>'.$value->hold_duration.'</td>';
     echo '<td>'.$value->fcr.'</td>';
     echo '<td>'.$value->fcr_reason .'</td>';
     echo '<td>'.$value->customer_satisfaction .'</td>'; 
     echo '<td>'.$value->Observer_Name .'</td>';    
    echo '<td>'.$value->bce.'</td>';   
    echo '<td>'.$value->ece.'</td>';
    echo '<td>'.$value->nce.'</td>'; 
    echo '<td>'.$value->total_score.'</td>';
    echo '<td>'.$value->result.'</td>';
            $monitor = $value->id;
                foreach ($DataTable->score_sheet->category as $Groups){
                    foreach ($Groups->section as $section){
                        foreach ($section->Attribute as $Attribute){
                            $monitor_results    = \App\Monitor::find($monitor);
                            $TableName  = $DataTable->score_sheet->name.'_'.$monitor_results->project_id;
                            $option     ='attribute_option_id_'.$Attribute->id;
                            $description='description_id_'.$Attribute->id;
                            $ScoreSheet = DB::table($TableName)->where('monitor_id',$value->id)->get();
                            //echo $option;
                           
                            echo '<td title="'.$Attribute->name.'">'.$ScoreSheet[0]->$option.'</td>';
                            echo '<td>'.$ScoreSheet['0']->$description.'</td>';
                        }
                    }
                }
       }

    }
    else{
          
          $agent_name=$value->name;
           $call_count=1;


     if($value->agent_type==1 && $call_count<=4)
       {
        echo '<tr>';
        echo '<td>'.$value->id.'</td>';
        echo '<td>'.$value->project.'</td>';
        echo '<td>'.$value->super.'</td>';
        echo '<td>'.$value->leader.'</td>';
        echo '<td>'.$value->id_staff.'</td>';
        echo '<td>'.$value->crm_user.'</td>';
        echo '<td>'.$value->name.'</td>';
        echo '<td>'."New_comer".'</td>';
if ($value->monitoring_type=='Standard')
 {
     echo '<td>'."1".'</td>';
     echo '<td>'."0".'</td>';
 }else{

      echo '<td>'."0".'</td>';
      echo '<td>'."1".'</td>';   
 }           
     echo '<td>'.$value->observe_type.'</td>';  
     echo '<td>'.$value->call_nature.'</td>';  
     echo '<td>'.$value->call_type.'</td>';
     echo '<td>'.$value->customer_name.'</td>';
     echo '<td>'.$value->phone_number.'</td>';
     echo '<td>'.$value->call_date.'</td>';
     echo '<td>'.$value->call_time.'</td>';
     echo '<td>'.$value->call_duration.'</td>';
     echo '<td>'.$value->hold_duration.'</td>';
     echo '<td>'.$value->fcr.'</td>';
     echo '<td>'.$value->fcr_reason .'</td>';
     echo '<td>'.$value->customer_satisfaction .'</td>'; 
     echo '<td>'.$value->Observer_Name .'</td>';    
    echo '<td>'.$value->bce.'</td>';   
    echo '<td>'.$value->ece.'</td>';
    echo '<td>'.$value->nce.'</td>'; 
    echo '<td>'.$value->total_score.'</td>';
    echo '<td>'.$value->result.'</td>';        
        $call_count++;
            $monitor = $value->id;
                foreach ($DataTable->score_sheet->category as $Groups){
                    foreach ($Groups->section as $section){
                        foreach ($section->Attribute as $Attribute){
                            $monitor_results    = \App\Monitor::find($monitor);
                            $TableName  = $DataTable->score_sheet->name.'_'.$monitor_results->project_id;
                            $option     ='attribute_option_id_'.$Attribute->id;
                            $description='description_id_'.$Attribute->id;
                            $ScoreSheet = DB::table($TableName)->where('monitor_id',$value->id)->get();
                            //echo $option;
                           
                            echo '<td title="'.$Attribute->name.'">'.$ScoreSheet[0]->$option.'</td>';
                            echo '<td>'.$ScoreSheet['0']->$description.'</td>';
                        }
                    }
                }

       }
       elseif($value->agent_type!=1)
       {
        echo '<tr>';
        echo '<td>'.$value->id.'</td>';
        echo '<td>'.$value->project.'</td>';
        echo '<td>'.$value->super.'</td>';
        echo '<td>'.$value->leader.'</td>';
        echo '<td>'.$value->id_staff.'</td>';
        echo '<td>'.$value->crm_user.'</td>';
        echo '<td>'.$value->name.'</td>'; 
        if($value->agent_type==2){
                echo '<td>'."Staff".'</td>'; 
            }else{
               echo '<td>'.$value->agent_type.'</td>';   
            } 
if ($value->monitoring_type=='Standard')
 {
     echo '<td>'."1".'</td>';
     echo '<td>'."0".'</td>';
 }else{

      echo '<td>'."0".'</td>';
      echo '<td>'."1".'</td>';   
 }           
     echo '<td>'.$value->observe_type.'</td>';  
     echo '<td>'.$value->call_nature.'</td>';  
     echo '<td>'.$value->call_type.'</td>';
     echo '<td>'.$value->customer_name.'</td>';
     echo '<td>'.$value->phone_number.'</td>';
     echo '<td>'.$value->call_date.'</td>';
     echo '<td>'.$value->call_time.'</td>';
     echo '<td>'.$value->call_duration.'</td>';
     echo '<td>'.$value->hold_duration.'</td>';
     echo '<td>'.$value->fcr.'</td>';
     echo '<td>'.$value->fcr_reason .'</td>';
     echo '<td>'.$value->customer_satisfaction .'</td>'; 
     echo '<td>'.$value->Observer_Name .'</td>';    
    echo '<td>'.$value->bce.'</td>';   
    echo '<td>'.$value->ece.'</td>';
    echo '<td>'.$value->nce.'</td>'; 
    echo '<td>'.$value->total_score.'</td>';
    echo '<td>'.$value->result.'</td>';

                $monitor = $value->id;
                foreach ($DataTable->score_sheet->category as $Groups){
                    foreach ($Groups->section as $section){
                        foreach ($section->Attribute as $Attribute){
                            $monitor_results    = \App\Monitor::find($monitor);
                            $TableName  = $DataTable->score_sheet->name.'_'.$monitor_results->project_id;
                            $option     ='attribute_option_id_'.$Attribute->id;
                            $description='description_id_'.$Attribute->id;
                            $ScoreSheet = DB::table($TableName)->where('monitor_id',$value->id)->get();
                            //echo $option;
                           
                            echo '<td title="'.$Attribute->name.'">'.$ScoreSheet[0]->$option.'</td>';
                            echo '<td>'.$ScoreSheet['0']->$description.'</td>';
                        }
                    }
                }                   
       }       
    }


 
        

       echo "</tr></tboody>";
}

      
        
echo '</table>';
 header('Content-type: application/vnd.ms-excel');
 header("Content-Disposition: attachment; filename=Analysis.xls");
//$objWriter->save('php://output');     
?>
</body>
</html>



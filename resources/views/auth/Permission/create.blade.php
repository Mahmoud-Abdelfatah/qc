@extends('layouts.app')
@section('content')
    @include('layouts.form')
	@php $model = array(); $name = array(); @endphp
	@foreach (Route::getRoutes() as $value)
	        @php 
	        	array_push($model,substr($value->getName(), 0, strpos($value->getName(), '.')));
	        	array_push($name,substr($value->getName(), strpos($value->getName(),'.')+strlen('.')));
	        @endphp
	@endforeach
	<script type="text/javascript">
			$('#name').attr('onchange','Slug()');
			$('#model').attr('onchange','Slug()');
			function Slug(){
				var name  = $('#name :selected').text();
				var model = $('#model :selected').text();
				$('#slug').val(model+'.'+name);
			}
			$('#model').append('@foreach(array_unique($model) as $value) <option>{{$value}}</option>@endforeach')
			$('#name').append('@foreach(array_unique($name) as $value)  <option>{{$value}}</option>@endforeach')
	</script>
@endsection
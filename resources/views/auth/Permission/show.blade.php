<hr>
<style type="text/css">
	.showDate	 {
    background-color: blue;
    color: white;
} 
</style>
<table class="table table-hover table-striped table-bordered">
	@foreach($myModal->getCasts() as $column)
		<tr>
			@if (strpos($column, 'password') !== false)
			@elseif (strpos($column, 'int') !== false)

			@elseif (strpos($column, '_id') !== false) 
				<?php $COLUMN	=str_replace('_id','',$column);?>
				<th class="col-md-2 bg-info header-{{$column}}">{{ trans('form.'.$column) }}</th>
				<td class="col-md-10 {{$column}}">{{$ShowData->$COLUMN->name}}</td>
			@elseif (strpos($column, '_List') !== false)
				<th class="col-md-2 bg-info header-{{$column}}">{{ trans('form.'.$column) }}</th>
				<td class="col-md-10 {{$column}}">
					@foreach ($ShowData->$column as $key => $value)
						<span class="label label-warning"> {{$key}}</span>
					@endforeach
				</td>
			@else
				<th class="col-md-2 bg-info header-{{$column}}">{{ trans('form.'.$column) }}</th>
				<td class="col-md-10 {{$column}}">{{$ShowData->$column}}</td>
			@endif
		</tr>
	@endforeach
	
</table>
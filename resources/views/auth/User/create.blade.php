@extends('layouts.app')
@section('content')
	<!-- inject roles object to this view -->
    @php 

    $roles = Bican\Roles\Models\Role::lists('name','id')->toArray();   
     unset($roles[11]); // remove sadmin rfom droupdownlist
    $active 		= App\Active::lists('stats','id')->toArray(); 
    @endphp
    @include('layouts.form')
	<script type='text/javascript'>
		$('#formbody').append('<div class="col-md-3"><div class="form-group"><label for="role">{{ trans("form.Roles_List") }}</label><div class="role">{{ Form::select("roles_list[]",$roles,null,["class" => "form-control chosen-select","id" => "role","required" =>"required"])}}</div></div></div>');
	</script>
@endsection
@extends('layouts.app')
@section('content')
<div class="container-fluid ">
	
	<!--Index-->
	<input type="hidden" id="key" 	 	 value="id">
	<input type="hidden" id="model"  	 value="Project">
	<input type="hidden" id="groupby" 	 value="id">
	<input type="hidden" id="path" 	 	 value="Headcount">
	<input type="hidden" id="conditions" value='{}' name="conditions"  >
 	@include('layouts.table')
	<!--End Index-->
</div>
<script type="text/javascript">
	$('.col-int').remove();
</script>
@endsection

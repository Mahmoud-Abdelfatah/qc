@extends('layouts.app')
@php
$projects   =  \App\Project::all();
$supers     =  \App\Supervisor::all();
$teamleaders=  \App\Teamleader::all();
$agents     =  \App\Agent::all();
@endphp

@section('content')





<br/>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <label for="project">project</label>                                                                        <!--project-->
                
                @role('admin')
                <select name="project" id="project" class="form-control" onchange="loaddivajax()">
                   <option></option>
                   @foreach($projects as $value)
                   <option value="{{$value->id}}">{{$value->name}}</option>
                   @endforeach
               </select>
               @endrole

               @role('omanger | qmanger')
               <select name="project" id="project" class="form-control"   onchange="loaddivajax()">
                   @foreach($manger as $mang)
                   @foreach($mang->Project as $value)
                   <option value="{{$value->id}}">{{$value->name}}</option>
                   @endforeach
                   @endforeach
               </select>
               @endrole

               @role('osuper | qsuper')
               <select name="project" id="project" class="form-control"  select disabled onchange="loaddivajax()">
                   @foreach($supervisor as $super)
                   @foreach($super->Manager as $manger)
                   @foreach($manger->Project as $value)
                   <option value="{{$value->id}}">{{$value->name}}</option>
                   @endforeach
                   @endforeach
                   @endforeach
               </select>
               @endrole

               @role('oleader')
               <select name="project" id="project" class="form-control"  select disabled onchange="loaddivajax()">
                   @foreach($teamleader as $tleader)
                   @foreach($tleader->supervisor as $super)
                   @foreach($super->Manager as $manger)
                   @foreach($manger->Project as $value)
                   <option value="{{$value->id}}">{{$value->name}}</option>
                   @endforeach
                   @endforeach
                   @endforeach
                   @endforeach
               </select>
               @endrole

               @role('osenior')
               <select name="project" id="project" class="form-control"  select disabled onchange="loaddivajax()">
                   @foreach($senior as $seniorr)
                   @foreach($seniorr->teamleader as $tleader)
                   @foreach($tleader->supervisor as $super)
                   @foreach($super->Manager as $manger)
                   @foreach($manger->Project as $value)
                   <option value="{{$value->id}}">{{$value->name}}</option>
                   @endforeach
                   @endforeach
                   @endforeach
                   @endforeach
                   @endforeach
               </select>
               @endrole

               @role('oagent')
               <select name="project" id="project" class="form-control"  select disabled onchange="loaddivajax()">
                @foreach($agent as $agentt)
                @foreach($agentt->Senior as $seniorr)
                @foreach($seniorr->teamleader as $tleader)
                @foreach($tleader->supervisor as $super)
                @foreach($super->Manager as $manger)
                @foreach($manger->Project as $value)
                <option value="{{$value->id}}">{{$value->name}}</option>
                @endforeach
                @endforeach
                @endforeach
                @endforeach
                @endforeach
                @endforeach
            </select>
            @endrole


        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="super">super</label>                                                                               <!--super-->
            @role('admin')
            <select name="super" id="super" class="form-control" onchange="loaddivajax()">
                <option></option>
                @foreach($supers as $value)
                <option value="{{$value->id}}">{{$value->name}}</option>
                @endforeach
            </select>
            @endrole

            @role('omanger | qmanger')
            <select name="super" id="super" class="form-control" onchange="loaddivajax()">
                <option></option>
                @foreach($manger as $mange)
                @foreach($mange->supervisor as $value)
                <option value="{{$value->id}}">{{$value->name}}</option>
                @endforeach
                @endforeach
            </select>
            @endrole

            @role('osuper | qsuper')
            <select name="super" id="super" class="form-control" select disabled onchange="loaddivajax()">
                @foreach($supervisor as $value)
                <option value="{{$value->id}}">{{$value->name}}</option>
                @endforeach
            </select>
            @endrole

            @role('oleader')
            <select name="super" id="super" class="form-control" select disabled onchange="loaddivajax()">
                @foreach($teamleader as $tleader)
                @foreach($tleader->supervisor as $value)
                <option value="{{$value->id}}">{{$value->name}}</option>
                @endforeach
                @endforeach
            </select>
            @endrole

            @role('osenior')
            <select name="super" id="super" class="form-control" select disabled onchange="loaddivajax()">
             @foreach($senior as $seniorr)
             @foreach($seniorr->teamleader as $tleader)
             @foreach($tleader->supervisor as $value)
             <option value="{{$value->id}}">{{$value->name}}</option>
             @endforeach
             @endforeach
             @endforeach
         </select>
         @endrole


         @role('oagent')
         <select name="super" id="super" class="form-control" select disabled onchange="loaddivajax()">
          @foreach($agent as $agentt)
          @foreach($agentt->Senior as $seniorr)
          @foreach($seniorr->teamleader as $tleader)
          @foreach($tleader->supervisor as $value)
          <option value="{{$value->id}}">{{$value->name}}</option>
          @endforeach
          @endforeach
          @endforeach
          @endforeach
      </select>
      @endrole




  </div>
</div>
<div class="col-md-3">
    <div class="form-group">
        <label for="leader">leader</label>                                                                                     <!--Leader-->
        @role('admin')
        <select name="leader" id="leader" class="form-control" onchange="loaddivajax()">
            <option></option>
            @foreach($teamleaders as $value)
            <option value="{{$value->id}}">{{$value->name}}</option>
            @endforeach
        </select>
        @endrole

        @role('omanger | qmanger')
        <select name="leader" id="leader" class="form-control" onchange="loaddivajax()">
            <option></option>
            @foreach($manger as $mange)
            @foreach($mange->supervisor as $super)
            @foreach($super->teamleader as $value)
            <option value="{{$value->id}}">{{$value->name}}</option>
            @endforeach
            @endforeach
            @endforeach
        </select>
        @endrole

        @role('osuper | qsuper')
        <select name="leader" id="leader" class="form-control" onchange="loaddivajax()">
            <option></option>
            @foreach($supervisor as $super)
            @foreach($super->teamleader as $value)
            <option value="{{$value->id}}">{{$value->name}}</option>
            @endforeach
            @endforeach
        </select>
        @endrole

        @role('oleader')
        <select name="leader" id="leader" class="form-control"  select disabled onchange="loaddivajax()">
            @foreach($teamleader as $value)
            <option value="{{$value->id}}">{{$value->name}}</option>
            @endforeach
        </select>
        @endrole

        @role('osenior')
        <select name="leader" id="leader" class="form-control" select disabled onchange="loaddivajax()">
            @foreach($senior as $seniorr)
            @foreach($seniorr->teamleader as $value)
            <option value="{{$value->id}}">{{$value->name}}</option>
            @endforeach
            @endforeach
        </select>
        @endrole

        @role('oagent')
        <select name="leader" id="leader" class="form-control" select disabled onchange="loaddivajax()">
           @foreach($agent as $agentt)
           @foreach($agentt->Senior as $seniorr)
           @foreach($seniorr->teamleader as $value)
           <option value="{{$value->id}}">{{$value->name}}</option>
           @endforeach
           @endforeach
           @endforeach
       </select>
       @endrole


   </div>
</div>
<div class="col-md-3">
    <div class="form-group">
        <label for="agent">agent</label>                                                                     <!--agent-->
        @role('admin')
        <select name="agent" id="agent" class="form-control" onchange="loaddivajax()">
            <option></option>
            @foreach($agents as $value)
            <option value="{{$value->id}}">{{$value->name}}</option>
            @endforeach
        </select>
        @endrole

        @role('omanger | qmanger')
        <select name="agent" id="agent" class="form-control" onchange="loaddivajax()">
            <option></option>
            @foreach($manger as $mange)
            @foreach($mange->supervisor as $super)
            @foreach($super->teamleader as $tleader)
            @foreach($tleader->Senior as $senior)
            @foreach($senior->Agent as $value)
            <option value="{{$value->id}}">{{$value->name}}</option>
            @endforeach
            @endforeach
            @endforeach
            @endforeach
            @endforeach
        </select>
        @endrole



        @role('osuper | qsuper')
        <select name="agent" id="agent" class="form-control" onchange="loaddivajax()">
            <option></option>
            @foreach($supervisor as $super)
            @foreach($super->teamleader as $tleader)
            @foreach($tleader->Senior as $senior)
            @foreach($senior->Agent as $value)
            <option value="{{$value->id}}">{{$value->name}}</option>
            @endforeach
            @endforeach
            @endforeach
            @endforeach
        </select>
        @endrole



        @role('oleader')
        <select name="agent" id="agent" class="form-control" onchange="loaddivajax()">
            <option></option>
            @foreach($teamleader as $tleader)
            @foreach($tleader->Senior as $senior)
            @foreach($senior->Agent as $value)
            <option value="{{$value->id}}">{{$value->name}}</option>
            @endforeach
            @endforeach
            @endforeach
        </select>
        @endrole


        @role('osenior')
        <select name="agent" id="agent" class="form-control" onchange="loaddivajax()">
            <option></option>
            @foreach($senior as $seniorr)
            @foreach($seniorr->Agent as $value)
            <option value="{{$value->id}}">{{$value->name}}</option>
            @endforeach
            @endforeach
        </select>
        @endrole

        @role('oagent')
        <select name="agent" id="agent" class="form-control"  onchange="loaddivajax()">
            <option></option>
            @foreach($agent as $value)
            <option value="{{$value->id}}">{{$value->name}}</option>
            @endforeach
        </select>
        @endrole

    </div>
</div>
</div>
<div id="divajax" class="divajax row">
    <div class="col-md-3">
        <div class="well">
            <h4>Total Monitor</h4>
            <span id="spanMonitor">{{$Monitor}}</span>
        </div>
    </div>
    <div class="col-md-3">
        <div class="well">
            <h4>Total Coaching</h4>
            <span id="spanCoaching">{{$Coaching}}</span>
        </div>
    </div>
    <div class="col-md-3">
        <div class="well">
            <h4>Total success</h4>
            <span id="spansuccess">{{$success}}</span>
        </div>
    </div>
    <div class="col-md-3">
        <div class="well">
            <h4>Total fail</h4>
            <span id="spanfail">{{$fail}}</span>
        </div>
    </div>
</div>
</div>

<script type="text/javascript">
    function loaddivajax(){
        var project     = $('#project :selected').val();
        var supervisor  = $('#super   :selected').val();
        var leader      = $('#leader  :selected').val();
        var agent       = $('#agent   :selected').val();
        var CSRF_TOKEN  = '{{ csrf_token() }}';

        $.ajax({
            url     :"{{url('divajax?')}}",
            data    :{project:project,supervisor:supervisor,leader:leader,agent:agent,_token: CSRF_TOKEN},
            dataType:'json',
            type    :'post',
            success : function(data){
                $('#spanMonitor').text(data['Monitor']);
                $('#spanCoaching').text(data['Coaching']);
                $('#spanfail').text(data['fail']);
                $('#spansuccess').text(data['success']);

                //$('.divajax').html(data);


            }
        });
        return false;
        
        console.log(project);

        
    }    
</script>
@endsection

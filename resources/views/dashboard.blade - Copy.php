@extends('layouts.app')

@section('content')
<br/>
<div class="container">
    <div class="row">
    <div class="col-md-3">
        <div class="well">
            <h4>Total Monitor</h4>
            <span>{{$Monitor}}</span>
        </div>
    </div>
    <div class="col-md-3">
        <div class="well">
            <h4>Total Coaching</h4>
            <span>{{$Coaching}}</span>
        </div>
    </div>
    <div class="col-md-3">
        <div class="well">
            <h4>Total success</h4>
            <span>{{$success}}</span>
        </div>
    </div>
    <div class="col-md-3">
        <div class="well">
            <h4>Total fail</h4>
            <span>{{$fail}}</span>
        </div>
    </div>
    
</div>
</div>
@endsection

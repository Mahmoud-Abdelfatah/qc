<?php

use Illuminate\Database\Seeder;

class A_08_02_008_CategorySectionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('category_section')->insert([
            ['category_id' => '1','section_id'   => '2'],
            ['category_id' => '1','section_id'   => '3'],
            ['category_id' => '1','section_id'   => '1'],
            ['category_id' => '1','section_id'   => '4'],
            ['category_id' => '1','section_id'   => '5'],
            ['category_id' => '1','section_id'   => '6'],
            ['category_id' => '2','section_id'   => '10'],
            ['category_id' => '2','section_id'   => '7'],
            ['category_id' => '2','section_id'   => '11'],
            ['category_id' => '2','section_id'   => '12'],
            ['category_id' => '2','section_id'   => '8'],
            ['category_id' => '2','section_id'   => '13'],
            ['category_id' => '3','section_id'   => '23'],
            ['category_id' => '3','section_id'   => '21'],
            ['category_id' => '3','section_id'   => '15'],
            ['category_id' => '3','section_id'   => '14'],
            ['category_id' => '3','section_id'   => '17'],
            ['category_id' => '3','section_id'   => '16'],
            ['category_id' => '3','section_id'   => '19'],
            ['category_id' => '3','section_id'   => '18'],
            ['category_id' => '3','section_id'   => '24'],
            ['category_id' => '3','section_id'   => '26'],
            ['category_id' => '3','section_id'   => '22'],
            ['category_id' => '3','section_id'   => '20'],
            ['category_id' => '3','section_id'   => '25'],
            ['category_id' => '2','section_id'   => '9']
        ]);
    }
}

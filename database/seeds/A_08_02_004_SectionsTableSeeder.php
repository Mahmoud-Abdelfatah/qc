<?php

use Illuminate\Database\Seeder;

class A_08_02_004_SectionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('sections')->insert([
            ['name' => 'Survey','rank'   => 70,'sort'   => 0],
            ['name' => 'Call duration','rank'   => 70,'sort'   => 13],
            ['name' => 'Company name','rank'   => '70','sort'   => '0'],
            ['name' => 'BC-CRM which reflects on the business workflow to avoid re-work and wrong reporting','rank'   => '70','sort'   => '0'],
            ['name' => 'Major updates','rank'   => '70','sort'   => '0'],
            ['name' => 'Unidentified sales opportunity/Reason for call','rank'   => '70','sort'   => '0'],
            ['name' => 'EU-CRM','rank'   => '70','sort'   => '0'],
            ['name' => 'Used appropriate style for the customer','rank'   => '70','sort'   => '0'],
            ['name' => 'Agent Behavior','rank'   => '70','sort'   => '0'],
            ['name' => 'Disconnect call','rank'   => '70','sort'   => '0'],
            ['name' => 'Information','rank'   => '70','sort'   => '0'],
            ['name' => 'Troubleshooting','rank'   => '70','sort'   => '0'],
            ['name' => 'Verified Customer\'s data','rank'   => '70','sort'   => '0'],
            ['name' => 'Call Sequence','rank'   => '4','sort'   => '14'],
            ['name' => 'Breaking the ICE','rank'   => '4','sort'   => '12'],
            ['name' => 'Customer Handling','rank'   => '4','sort'   => '8'],
            ['name' => 'Company image , Policy and professionalism','rank'   => '4','sort'   => '10'],
            ['name' => 'Slangs & Terminologies','rank'   => '4','sort'   => '7'],
            ['name' => 'Phone etiquettes','rank'   => '4','sort'   => '9'],
            ['name' => 'Using customer name','rank'   => '4','sort'   => '11'],
            ['name' => 'Answering customer\'s inquiries','rank'   => '4','sort'   => '6'],
            ['name' => 'Used the proper Hold & Transfer processes','rank'   => '4','sort'   => '3'],
            ['name' => 'Active listening','rank'   => '4','sort'   => '5'],
            ['name' => 'Used proper Closing','rank'   => '4','sort'   => '4'],
            ['name' => 'Voice','rank'   => '4','sort'   => '22'],
            ['name' => 'Used proper greeting','rank'   => '4','sort'   => '2']
        ]);
    }
}

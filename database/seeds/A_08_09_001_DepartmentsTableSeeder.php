<?php

use Illuminate\Database\Seeder;

class A_08_09_001_DepartmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('departments')->insert([
            ['name'=>'Quality'],
            ['name'=>'Opration']
        ]);
    }
}

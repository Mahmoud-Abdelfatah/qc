<?php

use Illuminate\Database\Seeder;

class A_08_03_003_SeniorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('seniors')->insert([

            ['name'    =>'Seniors Zone A_1_1_1'],
            ['name'    =>'Seniors Zone A_1_1_2'],
            ['name'    =>'Seniors Zone A_1_2_1'],
            ['name'    =>'Seniors Zone A_1_2_2'],
            ['name'    =>'Seniors Zone A_2_1_1'],
            ['name'    =>'Seniors Zone A_2_1_2'],
            ['name'    =>'Seniors Zone A_2_2_1'],
            ['name'    =>'Seniors Zone A_2_2_2'],
            ['name'    =>'Seniors Zone B_1_1_1'],
            ['name'    =>'Seniors Zone B_1_1_2'],
            ['name'    =>'Seniors Zone B_1_2_1'],
            ['name'    =>'Seniors Zone B_1_2_2'],
            ['name'    =>'Seniors Zone B_2_1_1'],
            ['name'    =>'Seniors Zone B_2_1_2'],
            ['name'    =>'Seniors Zone B_2_2_1'],
            ['name'    =>'Seniors Zone B_2_2_2']
        ]);
    }
}

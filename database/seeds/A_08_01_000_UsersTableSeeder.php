<?php

use Illuminate\Database\Seeder;

class A_08_01_000_UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
            ['name'=>'Admin'                    ,'email'=>'admin@icall.com.eg'              ,'password'=> bcrypt('welcome')],
            ['name'=>'Operation Manger'         ,'email'=>'operation_manger@icall.com.eg'   ,'password'=> bcrypt('welcome')],
            ['name'=>'Quality Manger'           ,'email'=>'quality_manger@icall.com.eg'     ,'password'=> bcrypt('welcome')],
            ['name'=>'Operation Supervisor'     ,'email'=>'operation_super@icall.com.eg'    ,'password'=> bcrypt('welcome')],
            ['name'=>'Quality Supervisor'       ,'email'=>'quality_super@icall.com.eg'      ,'password'=> bcrypt('welcome')],
            ['name'=>'Operation Team Leaders'   ,'email'=>'operation_leader@icall.com.eg'   ,'password'=> bcrypt('welcome')],
            ['name'=>'Quality Specialist'       ,'email'=>'quality_specia@icall.com.eg'     ,'password'=> bcrypt('welcome')],
            ['name'=>'Operation Senior'         ,'email'=>'operation_senior@icall.com.eg'   ,'password'=> bcrypt('welcome')],
            ['name'=>'Quality observers'        ,'email'=>'quality_observer@icall.com.eg'   ,'password'=> bcrypt('welcome')],
            ['name'=>'Agent'                    ,'email'=>'operation_agent@icall.com.eg'    ,'password'=> bcrypt('welcome')]
        ]);
    }
}

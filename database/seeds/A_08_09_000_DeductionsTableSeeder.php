<?php

use Illuminate\Database\Seeder;

class A_08_09_000_DeductionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('deductions')->insert([
            ['level'   =>1, 'name'=>'warning'],
            ['level'   =>2, 'name'=>'1 day deduction'],
            ['level'   =>3, 'name'=>'2 day deduction'],
            ['level'   =>4, 'name'=>'HR Investigation'],
            ['level'   =>5, 'name'=>'Termination']
            
        ]);
    }
}

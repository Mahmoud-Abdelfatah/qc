<?php

use Illuminate\Database\Seeder;

class A_08_01_001_RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('roles')->insert([
            ['name'=>'Admin'                    ,'slug'=>'admin'        ,'description'=>'admin'],
            ['name'=>'Operation Manger'         ,'slug'=>'omanger'      ,'description'=>'Operation Manger'],
            ['name'=>'Quality Manger'           ,'slug'=>'qmanger'      ,'description'=>'Quality Manger'],
            ['name'=>'Operation Supervisor'     ,'slug'=>'osuper'       ,'description'=>'Operation Supervisor'],
            ['name'=>'Quality Supervisor'       ,'slug'=>'qsuper'       ,'description'=>'Quality Supervisor'],
            ['name'=>'Operation Team Leaders'   ,'slug'=>'oleader'      ,'description'=>'Operation Team Leaders'],
            ['name'=>'Quality Specialist'       ,'slug'=>'qspecia'      ,'description'=>'Quality Specialist'],
            ['name'=>'Operation Senior'         ,'slug'=>'osenior'      ,'description'=>'Operation Senior'],
            ['name'=>'Quality observers'        ,'slug'=>'qobserver'    ,'description'=>'Quality observers'],
            ['name'=>'Agent'                    ,'slug'=>'oagent'       ,'description'=>'Agent']            

        ]);
    }
}

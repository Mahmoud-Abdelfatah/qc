<?php

use Illuminate\Database\Seeder;

class A_08_02_010_AttributeAttributeOptionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('attribute_attribute_option')->insert([
            ['attribute_id' => '1','attribute_option_id'   => '1'],
            ['attribute_id' => '1','attribute_option_id'   => '1'],
            ['attribute_id' => '2','attribute_option_id'   => '1'],
            ['attribute_id' => '3','attribute_option_id'   => '1'],
            ['attribute_id' => '4','attribute_option_id'   => '1'],
            ['attribute_id' => '5','attribute_option_id'   => '1'],
            ['attribute_id' => '6','attribute_option_id'   => '1'],
            ['attribute_id' => '7','attribute_option_id'   => '1'],
            ['attribute_id' => '8','attribute_option_id'   => '1'],
            ['attribute_id' => '9','attribute_option_id'   => '1'],
            ['attribute_id' => '10','attribute_option_id'   => '1'],
            ['attribute_id' => '11','attribute_option_id'   => '1'],
            ['attribute_id' => '12','attribute_option_id'   => '1'],
            ['attribute_id' => '13','attribute_option_id'   => '1'],
            ['attribute_id' => '14','attribute_option_id'   => '1'],
            ['attribute_id' => '15','attribute_option_id'   => '1'],
            ['attribute_id' => '16','attribute_option_id'   => '1'],
            ['attribute_id' => '17','attribute_option_id'   => '1'],
            ['attribute_id' => '18','attribute_option_id'   => '1'],
            ['attribute_id' => '19','attribute_option_id'   => '1'],
            ['attribute_id' => '20','attribute_option_id'   => '1'],
            ['attribute_id' => '21','attribute_option_id'   => '1'],
            ['attribute_id' => '22','attribute_option_id'   => '1'],
            ['attribute_id' => '23','attribute_option_id'   => '1'],
            ['attribute_id' => '24','attribute_option_id'   => '1'],
            ['attribute_id' => '25','attribute_option_id'   => '1'],
            ['attribute_id' => '26','attribute_option_id'   => '1'],
            ['attribute_id' => '27','attribute_option_id'   => '1'],
            ['attribute_id' => '28','attribute_option_id'   => '1'],
            ['attribute_id' => '29','attribute_option_id'   => '1'],
            ['attribute_id' => '30','attribute_option_id'   => '1'],
            ['attribute_id' => '31','attribute_option_id'   => '1'],
            ['attribute_id' => '32','attribute_option_id'   => '1'],
            ['attribute_id' => '33','attribute_option_id'   => '1'],
            ['attribute_id' => '34','attribute_option_id'   => '1'],
            ['attribute_id' => '35','attribute_option_id'   => '1'],
            ['attribute_id' => '36','attribute_option_id'   => '1'],
            ['attribute_id' => '37','attribute_option_id'   => '1'],
            ['attribute_id' => '38','attribute_option_id'   => '1'],
            ['attribute_id' => '39','attribute_option_id'   => '1'],
            ['attribute_id' => '1','attribute_option_id'   => '2'],
            ['attribute_id' => '2','attribute_option_id'   => '2'],
            ['attribute_id' => '3','attribute_option_id'   => '2'],
            ['attribute_id' => '4','attribute_option_id'   => '2'],
            ['attribute_id' => '5','attribute_option_id'   => '2'],
            ['attribute_id' => '6','attribute_option_id'   => '2'],
            ['attribute_id' => '7','attribute_option_id'   => '2'],
            ['attribute_id' => '8','attribute_option_id'   => '2'],
            ['attribute_id' => '9','attribute_option_id'   => '2'],
            ['attribute_id' => '10','attribute_option_id'   => '2'],
            ['attribute_id' => '11','attribute_option_id'   => '2'],
            ['attribute_id' => '12','attribute_option_id'   => '2'],
            ['attribute_id' => '13','attribute_option_id'   => '2'],
            ['attribute_id' => '14','attribute_option_id'   => '2'],
            ['attribute_id' => '15','attribute_option_id'   => '2'],
            ['attribute_id' => '16','attribute_option_id'   => '2'],
            ['attribute_id' => '17','attribute_option_id'   => '2'],
            ['attribute_id' => '18','attribute_option_id'   => '2'],
            ['attribute_id' => '19','attribute_option_id'   => '2'],
            ['attribute_id' => '20','attribute_option_id'   => '2'],
            ['attribute_id' => '21','attribute_option_id'   => '2'],
            ['attribute_id' => '22','attribute_option_id'   => '2'],
            ['attribute_id' => '23','attribute_option_id'   => '2'],
            ['attribute_id' => '24','attribute_option_id'   => '2'],
            ['attribute_id' => '25','attribute_option_id'   => '2'],
            ['attribute_id' => '26','attribute_option_id'   => '2'],
            ['attribute_id' => '27','attribute_option_id'   => '2'],
            ['attribute_id' => '28','attribute_option_id'   => '2'],
            ['attribute_id' => '29','attribute_option_id'   => '2'],
            ['attribute_id' => '30','attribute_option_id'   => '2'],
            ['attribute_id' => '31','attribute_option_id'   => '2'],
            ['attribute_id' => '32','attribute_option_id'   => '2'],
            ['attribute_id' => '33','attribute_option_id'   => '2'],
            ['attribute_id' => '34','attribute_option_id'   => '2'],
            ['attribute_id' => '35','attribute_option_id'   => '2'],
            ['attribute_id' => '36','attribute_option_id'   => '2'],
            ['attribute_id' => '37','attribute_option_id'   => '2'],
            ['attribute_id' => '38','attribute_option_id'   => '2'],
            ['attribute_id' => '39','attribute_option_id'   => '2']
        ]);
    }
}

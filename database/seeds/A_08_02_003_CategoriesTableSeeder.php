<?php

use Illuminate\Database\Seeder;

class A_08_02_003_CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('categories')->insert([
            ['name' => 'Business Critical Error','description'   => 'bce'],
            ['name' => 'End-User Critical Error','description'   => 'ece'],
            ['name' => 'Non-Crirical Error'     ,'description'   => 'nce']
        ]);
    }
}

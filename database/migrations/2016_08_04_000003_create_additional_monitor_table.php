<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdditionalMonitorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('additional_monitor', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('additional_id')->unsigned()->index();
            $table->foreign('additional_id')->references('id')->on('additionals')->onDelete('cascade');
            $table->integer('monitor_id')->unsigned()->index();
            $table->foreign('monitor_id')->references('id')->on('monitors')->onDelete('cascade');
            $table->integer('created_by')->comment('disable');
            $table->integer('updated_by')->comment('disable');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
         Schema::drop('additional_monitor');
    }
}

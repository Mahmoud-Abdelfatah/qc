<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id')->comment('disable');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password', 60)->comment('password');
            $table->integer('active')->unsigned()->index()->comment('relationship');
            $table->foreign('active')->references('id')->on('Actives')->onDelete('RESTRICT');
            $table->rememberToken();
            $table->integer('created_by')->comment('disable');
            $table->integer('updated_by')->comment('disable');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMonitorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('monitors', function (Blueprint $table) {
            $table->increments('id')->comment('disable');
            $table->integer('project_id')->unsigned()->index()->comment('relationship');
            $table->integer('manager_id')->unsigned()->index()->comment('relationship');
            $table->integer('supervisor_id')->unsigned()->index()->comment('relationship');
            $table->integer('teamleader_id')->unsigned()->index()->comment('relationship');
            $table->integer('senior_id')->unsigned()->index()->comment('relationship');
            $table->integer('agent_id')->unsigned()->index()->comment('relationship');
            $table->enum('monitoring_type', ['Standard', 'additional']);
            $table->enum('observe_type', ['Remotely','Side-by-Side']);
            $table->enum('call_nature', ['In-Bound','Out-Bound']);
            $table->enum('call_type', ['Maintenance','Purchase','Complain','order Taking','Survey','Installation','Confirmation','Exchange','Cancelation','Modification','Maintenance Inquiry','Sales Inquiry','Other']);
            $table->string('customer_name');
            $table->integer('phone_number');
            $table->time('call_duration');
            $table->time('hold_duration');
            $table->date('call_date');
            $table->time('call_time');
            $table->enum('fcr', ['Done','Agent','Customer','Client']);
            $table->string('fcr_reason');
            $table->enum('customer_satisfaction', ['Very satisfied','Satisfied','Neutral','Dissatisfied','Very dissatisfied']);
            $table->integer('bce')->comment('hidden');
            $table->integer('ece')->comment('hidden');
            $table->integer('nce')->comment('hidden');
            $table->integer('total_score')->comment('hidden');
            $table->string('result')->comment('hidden');
            $table->integer('globalfail')->comment('disable');
            $table->text('description')->nullable()->comment('disable');
            $table->integer('created_by')->comment('disable');
            $table->integer('updated_by')->comment('disable');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('monitors');
    }
}

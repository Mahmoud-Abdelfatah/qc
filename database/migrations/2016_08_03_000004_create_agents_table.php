<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgentsTable extends Migration

{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agents', function (Blueprint $table) {
            $table->increments('id')->comment('disable');
            $table->string('name');

            $table->string('id_staff')->unique();
            $table->string('crm_user')->unique();
            $table->string('idPbx')->unique();
            $table->string('windows_account')->unique();
            
            $table->integer('created_by')->comment('disable');
            $table->integer('updated_by')->comment('disable');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
         Schema::drop('agents');
    }
}

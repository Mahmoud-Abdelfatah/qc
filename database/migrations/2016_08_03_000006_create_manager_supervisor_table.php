<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManagerSupervisorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manager_supervisor', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('manager_id')->unsigned()->index();
            $table->foreign('manager_id')->references('id')->on('managers')->onDelete('cascade');
            $table->integer('supervisor_id')->unsigned()->index();
            $table->foreign('supervisor_id')->references('id')->on('supervisors')->onDelete('cascade');
            $table->integer('created_by')->comment('disable');
            $table->integer('updated_by')->comment('disable');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
         Schema::drop('manager_supervisor');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id')->comment('disable');
            $table->string('name')->unique();
            $table->integer('location_id')->unsigned()->index()->comment('relationship');
            $table->foreign('location_id')->references('id')->on('locations')->onDelete('RESTRICT');
            $table->integer('score_sheet_id')->unsigned()->index()->comment('relationship');
            $table->foreign('score_sheet_id')->references('id')->on('score_sheets')->onDelete('RESTRICT');
            $table->text('description')->nullable();
            $table->integer('created_by')->comment('disable');
            $table->integer('updated_by')->comment('disable');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('projects');
    }
}

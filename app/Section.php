<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;
class Section extends Model
{
    protected $table    = 'sections';
    protected $casts    = ['id','name','rank','Attributes_List','created_by','updated_by',];
    protected $fillable = ['name','rank','sort','created_by','updated_by',];
    protected $appends  = ['model_name'];
    //get table and columns
    function getModelNameAttribute(){return 'Section';}
    function getTablColumns()       { 
        $array      =   DB::select( DB::raw('SHOW full COLUMNS FROM sections'));
        $array[]    =   (object) array('Field' => 'Attributes_List','Comment'=>'list','Type'=>'','Null'=>'NO',);
        return $array;
    }
    //relationship
    function created_name(){ return $this->hasOne('App\User', 'id', 'created_by');}
    function updated_name(){ return $this->hasOne('App\User', 'id', 'updated_by');}    
    
    function Attribute(){return $this->belongsToMany('App\Attribute');}
    function getAttributesListAttribute(){return $this->Attribute()->lists('attributes.id','name')->toArray();}
        
}

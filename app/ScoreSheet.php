<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;
class ScoreSheet extends Model
{
    protected $table    = 'score_sheets';
    protected $casts    = ['id','name','Categories_List','created_by','updated_by',];
    protected $fillable = ['name','created_by','updated_by',];
    protected $appends  = ['model_name'];
    //get table and columns
    function getModelNameAttribute(){return 'ScoreSheet';}
    function getTablColumns()       { 
        $array      =   DB::select( DB::raw('SHOW full COLUMNS FROM score_sheets'));
        $array[]    =   (object) array('Field' => 'Categories_List','Comment'=>'list','Type'=>'','Null'=>'NO',);
        return $array;
    }
    //relationship
    function created_name(){ return $this->hasOne('App\User', 'id', 'created_by');}
    function updated_name(){ return $this->hasOne('App\User', 'id', 'updated_by');}
    function Category(){return $this->belongsToMany('App\Category');}
    function getCategoriesListAttribute(){return $this->Category()->lists('categories.id','name')->toArray();}    
}

<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;
class SheetCoaching extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table    = 'sheet_coachings';
    protected $fillable = ['coaching_id','attribute_id','attribute_option_id','description',];
    
    public function Coaching()
    {
        return $this->belongsTo('App\Coaching');
    }
}

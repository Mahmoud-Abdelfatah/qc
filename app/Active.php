<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Active extends Model
{
    protected $table    = 'actives';
    protected $casts    = ['id','stats','created_at','created_by','updated_at','updated_by',];
     protected $fillable = ['name','score','created_by','updated_by',];
    protected $appends  = ['model_name'];
    //get table and columns
    function getModelNameAttribute(){return 'active';}
    function getTablColumns(){return DB::select( DB::raw('show full columns from actives'));}
    function created_name(){ return $this->hasOne('App\User', 'id', 'created_by');}
    function updated_name(){ return $this->hasOne('App\User', 'id', 'updated_by');}    
}

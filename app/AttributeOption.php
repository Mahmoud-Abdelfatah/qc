<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;
class AttributeOption extends Model
{
    protected $table    = 'attribute_options';
    protected $casts    = ['id','name','score','created_at','created_by','updated_at','updated_by',];
    protected $fillable = ['name','score','created_by','updated_by',];
    protected $appends  = ['model_name'];
    //get table and columns
    function getModelNameAttribute(){return 'AttributeOption';}
    function getTablColumns(){return DB::select( DB::raw('show full columns from attribute_options'));}
    //relationship
    function created_name(){ return $this->hasOne('App\User', 'id', 'created_by');}
    function updated_name(){ return $this->hasOne('App\User', 'id', 'updated_by');}    
}
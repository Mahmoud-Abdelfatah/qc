<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;
class Location extends Model
{
    protected $table        = 'locations';
    protected  $casts       = ['id','name','created_by',];
    protected $fillable     = ['name','created_by','updated_by','description'];
    protected $appends      = ['model_name'];
    //get table and columns
    function getModelNameAttribute(){return 'Location';}
    function getTablColumns(){return DB::select( DB::raw('show full columns from locations'));}
    //relationship
    function created_name(){ return $this->hasOne('App\User', 'id', 'created_by');}
    function updated_name(){ return $this->hasOne('App\User', 'id', 'updated_by');}

}


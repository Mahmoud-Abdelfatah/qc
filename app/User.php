<?php
namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Bican\Roles\Traits\HasRoleAndPermission;
use Bican\Roles\Contracts\HasRoleAndPermission as HasRoleAndPermissionContract;
use DB;
use Illuminate\Database\Eloquent\SoftDeletes;
class User extends Model implements AuthenticatableContract, CanResetPasswordContract, HasRoleAndPermissionContract
{
    use Authenticatable, CanResetPassword, HasRoleAndPermission;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table        = 'users';
    protected $casts        = ['id','name','email','active','Roles_List'];
    protected $fillable     = ['name','email','password','active','created_at','updated_at','created_by','updated_by'];
    protected $appends      = ['model_name'];
    use SoftDeletes;
    protected $dates    = ['deleted_at'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token',];

    function getModelNameAttribute(){return 'User';}
    function getTablColumns(){return DB::select( DB::raw('show full columns from users'));}
    public function setPasswordAttribute($password){$this->attributes['password'] = bcrypt($password);}
    public function getRolesListAttribute($value){return $this->getRoles()->lists('id','name')->toArray();}
    public function getPermissionsListAttribute($value){return $this->getPermissions()->lists('id','slug')->toArray();}
    public function active(){return $this->belongsTo('App\Active');}
    function getactiveList(){return $this->active()->lists('actives.id','stats')->toArray();}
    function getOptionAttribute(){return $this->active()->lists('stats')->toArray();}


    
}

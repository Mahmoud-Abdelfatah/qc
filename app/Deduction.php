<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;
class Deduction extends Model
{
    protected $table    = 'deductions';
    protected $casts    = ['id','name','level'];
    protected $fillable = ['name','level','created_by','updated_by',];
    protected $appends  = ['model_name'];
    //get table and columns
    function getModelNameAttribute(){return 'Deduction';}
    function getTablColumns(){return DB::select( DB::raw('show full columns from deductions'));}
    //relationship
    function created_name(){ return $this->hasOne('App\User', 'id', 'created_by');}
    function updated_name(){ return $this->hasOne('App\User', 'id', 'updated_by');}
}
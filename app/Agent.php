<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Agent extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    //Table Name
        protected $table    = 'agents';
    //COLUMNS which show to Tables
        protected $casts    = ['id','name','id_staff','idPbx','crm_user','windows_account','Seniors_List','created_by'];
    //this COLUMNS to search, insert and update
        protected $fillable = ['id','name','id_staff','idPbx','crm_user','windows_account','agent_type','created_by','updated_by'];
    //get model name
        protected $appends  = ['model_name'];
    //SoftDeletes
        use SoftDeletes;
        protected $dates    = ['deleted_at'];
        
    //Stander for any models get name and COLUMNS
        function getModelNameAttribute(){ return 'Agent'; }
        function getTablColumns()       { 
            $array      =   DB::select( DB::raw('SHOW full COLUMNS FROM agents'));
            $array[]    =   (object) array('Field' => 'Seniors_List','Comment'=>'list','Type'=>'','Null'=>'NO',);
            return $array;

        }
    //User relationship
        function created_name(){ return $this->hasOne('App\User', 'id', 'created_by');}
        function updated_name(){ return $this->hasOne('App\User', 'id', 'updated_by');}
    //Other relationship
    public function Senior(){return $this->belongsToMany('App\Senior')->withPivot('agent_id','senior_id');}
    public function getSeniorsListAttribute($value){return $this->Senior()->lists('seniors.id','name')->toArray();}
}
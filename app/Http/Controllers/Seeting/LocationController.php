<?php

namespace App\Http\Controllers\Seeting;
use Illuminate\Http\Request;
use Auth;
use Validator;
use App\Location;
use DB;
use App\Http\Controllers\Controller;
class LocationController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $myModal    = new Location;
        $DataTable  = Location::orderBy('id','desc')->paginate(10);
        return view('Seeting.Location.index',compact('myModal','DataTable'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $myModal    = new Location;
        return view('Seeting.Location.create',compact('myModal'));
    }

    /**
     * Seeting a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, ['name' => 'required|unique:locations']);
        $request->request->add(['created_by' => Auth::user()->id]);
        $Location = Location::create($request->all());
        return redirect()->route('Location.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $myModal    = new Location;
        $ShowData = Location::find($id);
        return view('Seeting.Location.show',compact('ShowData','myModal'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $myModal    = new Location;
        $EditData   = Location::find($id);
        return view('Seeting.Location.edit',compact('myModal','EditData'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, ['name' => 'required|unique:locations,name,'.$id]);
        $location = Location::findOrFail($id);
        $request->request->add(['updated_by' => Auth::user()->id]);
        $location = $location->update($request->all());
        return redirect()->route('Location.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Location::whereId($id)->delete($id);
        return Response()->json($id);
    }
}

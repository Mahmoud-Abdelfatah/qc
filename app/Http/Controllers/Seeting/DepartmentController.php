<?php

namespace App\Http\Controllers\Seeting;
use Illuminate\Http\Request;
use Auth;
use Validator;
use App\Department;
use DB;
use App\Http\Controllers\Controller;

class DepartmentController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $myModal    = new Department;
        $DataTable  = Department::orderBy('id','desc')->paginate(10);
        return view('Seeting.Department.index',compact('myModal','DataTable'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $myModal    = new Department;
        return view('Seeting.Department.create',compact('myModal'));
    }

    /**
     * Seeting a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, ['name' => 'required|unique:departments']);
        $request->request->add(['created_by' => Auth::user()->id]);
        $Department = Department::create($request->all());
        return redirect()->route('Department.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $myModal    = new Department;
        $ShowData = Department::find($id);
        return view('Seeting.Department.show',compact('ShowData','myModal'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $myModal    = new Department;
        $EditData   = Department::find($id);
        return view('Seeting.Department.edit',compact('myModal','EditData'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, ['name' => 'required|unique:departments,name,'.$id]);
        $location = Department::findOrFail($id);
        $request->request->add(['updated_by' => Auth::user()->id]);
        $location = $location->update($request->all());
        return redirect()->route('Department.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Department::whereId($id)->delete($id);
        return Response()->json($id);
    }
}

<?php

namespace App\Http\Controllers\Seeting;
use Illuminate\Http\Request;
use Auth;
use Validator;
use App\Deduction;
use DB;
use App\Http\Controllers\Controller;

class DeductioncOntroller extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $myModal    = new Deduction;
        $DataTable  = Deduction::orderBy('id','desc')->paginate(10);
        return view('Seeting.Deduction.index',compact('myModal','DataTable'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $myModal    = new Deduction;
        return view('Seeting.Deduction.create',compact('myModal'));
    }

    /**
     * Seeting a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, ['name' => 'required|unique:deductions','level' => 'required|unique:deductions']);
        $request->request->add(['created_by' => Auth::user()->id]);
        $Deduction = Deduction::create($request->all());
        return redirect()->route('Deduction.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $myModal    = new Deduction;
        $ShowData = Deduction::find($id);
        return view('Seeting.Deduction.show',compact('ShowData','myModal'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $myModal    = new Deduction;
        $EditData   = Deduction::find($id);
        return view('Seeting.Deduction.edit',compact('myModal','EditData'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, ['name' => 'required|unique:deductions,name,'.$id,'level' => 'required|unique:deductions,level,'.$id]);
        $location = Deduction::findOrFail($id);
        $request->request->add(['updated_by' => Auth::user()->id]);
        $location = $location->update($request->all());
        return redirect()->route('Deduction.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Deduction::whereId($id)->delete($id);
        return Response()->json($id);
    }
}

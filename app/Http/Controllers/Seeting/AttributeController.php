<?php

namespace App\Http\Controllers\Seeting;
use Illuminate\Http\Request;
use Auth;
use Validator;
use App\Attribute;
use DB;
use App\Http\Controllers\Controller;

class AttributeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $myModal    = new Attribute;
        $DataTable  = Attribute::orderBy('id','desc')->paginate(10);
        return view('Seeting.Attribute.index',compact('myModal','DataTable'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $myModal    = new Attribute;
        return view('Seeting.Attribute.create',compact('myModal'));
    }

    /**
     * Seeting a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, ['name' => 'required|unique:attributes']);
        $request->request->add(['created_by' => Auth::user()->id]);
        $Attribute = Attribute::create($request->all());
        $Attribute->AttributeOption()->attach($request->Attributeoptions_List);
        return redirect()->route('Attribute.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $myModal    = new Attribute;
        $ShowData = Attribute::find($id);
        return view('Seeting.Attribute.show',compact('ShowData','myModal'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $myModal    = new Attribute;
        $EditData   = Attribute::find($id);
        return view('Seeting.Attribute.edit',compact('myModal','EditData'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, ['name' => 'required|unique:attributes,name,'.$id]);
        $Attribute  = Attribute::findOrFail($id);
        $request->request->add(['updated_by' => Auth::user()->id]);
        $update     = $Attribute->update($request->all());
        $Attribute->AttributeOption()->sync($request->Attributeoptions_List);
        return redirect()->route('Attribute.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Attribute::whereId($id)->delete($id);
        return Response()->json($id);
    }
}

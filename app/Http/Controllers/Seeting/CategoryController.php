<?php

namespace App\Http\Controllers\Seeting;
use Illuminate\Http\Request;
use Auth;
use Validator;
use App\Category;
use DB;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $myModal    = new Category;
        $DataTable  = Category::orderBy('id','desc')->paginate(10);
        return view('Seeting.Category.index',compact('myModal','DataTable'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $myModal    = new Category;
        return view('Seeting.Category.create',compact('myModal'));
    }

    /**
     * Seeting a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, ['name' => 'required|unique:categories']);
        $request->request->add(['created_by' => Auth::user()->id]);
        $Category = Category::create($request->all());
        $Category->Section()->attach($request->Sections_List);
        return redirect()->route('Category.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $myModal    = new Category;
        $ShowData = Category::find($id);
        return view('Seeting.Category.show',compact('ShowData','myModal'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $myModal    = new Category;
        $EditData   = Category::find($id);
        return view('Seeting.Category.edit',compact('myModal','EditData'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, ['name' => 'required|unique:categories,name,'.$id]);
        $Category = Category::find($id);
        $request->request->add(['updated_by' => Auth::user()->id]);
        $update   = $Category->update($request->all());
        $Category->Section()->sync($request->Sections_List);
        return redirect()->route('Category.index');


        
        

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Category::whereId($id)->delete($id);
        return Response()->json($id);
    }
}

<?php

namespace App\Http\Controllers\Seeting;
use Illuminate\Http\Request;
use Auth;
use Validator;
use App\AttributeOption;
use DB;
use App\Http\Controllers\Controller;

class AttributeOptionController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $myModal    = new AttributeOption;
        $DataTable  = AttributeOption::orderBy('id','desc')->paginate(10);
        return view('Seeting.AttributeOption.index',compact('myModal','DataTable'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $myModal    = new AttributeOption;
        return view('Seeting.AttributeOption.create',compact('myModal'));
    }

    /**
     * Seeting a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, ['name' => 'required|unique:attribute_options']);
        $request->request->add(['created_by' => Auth::user()->id]);
        $AttributeOption = AttributeOption::create($request->all());
        return redirect()->route('AttributeOption.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $myModal    = new AttributeOption;
        $ShowData = AttributeOption::find($id);
        return view('Seeting.AttributeOption.show',compact('ShowData','myModal'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $myModal    = new AttributeOption;
        $EditData   = AttributeOption::find($id);
        return view('Seeting.AttributeOption.edit',compact('myModal','EditData'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, ['name' => 'required|unique:attribute_options,name,'.$id]);
        $AttributeOption = AttributeOption::findOrFail($id);
        $request->request->add(['updated_by' => Auth::user()->id]);
        $update = $AttributeOption->update($request->all());
        return redirect()->route('AttributeOption.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        AttributeOption::whereId($id)->delete($id);
        return Response()->json($id);
    }
}

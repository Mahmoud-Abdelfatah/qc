<?php

namespace App\Http\Controllers\Quality;
use Illuminate\Http\Request;
use Auth;
use Validator;
use App\Monitor;
use App\Additional;
use App\Sheet;
use App\SheetAdditional;

use DB;
use App\Http\Controllers\Controller;
    


    use App\Project;
    use App\Manager;
    use App\Supervisor;
    use App\Teamleader;
    use App\Senior;
    use App\Agent;
    use App\User;
    use App\Coaching;

    class AdditionalController extends Controller
    {
        /**
         * Create a new controller instance.
         *
         * @return void
         */
        public function __construct()
        {
            //
            $this->middleware('auth');
        }
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {


         $user_role = \Auth::user()->getRoles()->lists('name');
         $username = \Auth::user()->name;
         $manger = Manager::with('supervisor')->where('name',$username)->get();
         $supervisor = Supervisor::with('teamleader')->where('name',$username)->get();
         $teamleader  = Teamleader::with('senior')->where('name',$username)->get();
         $senior  = Senior::with('agent')->where('name',$username)->get();
         $agent = Agent::with('senior')->where('name',$username)->get();
         $array = array();
     //  user as Admin   
         if($user_role == '["Admin"]')
         {
            
          $myModal    = new Additional;
          $DataTable  = Additional::orderBy('id','desc')->where('status','open')->paginate(10);
          return view('Quality.Additional.index',compact('myModal','DataTable'));
      }

     // user as Quality Manger
      if($user_role == '["Quality Manger"]')
      {
        
          $array = array();
          foreach ($manger as  $maneger) 
          {
            foreach ($maneger->Supervisor as $super) 
            {
                foreach ($super->Teamleader as $value) 
                {
                    $array[] = $maneger->name;
                    $array[] = $super->name;
                    $array[] =$value->name;  
                }

            }
        }
    }

     // user as Quality Supervisor
    if($user_role == '["Quality Supervisor"]')
    {
        
      $array = array();
      foreach ($supervisor as  $super) 
      {
        foreach ($super->Teamleader as $value) 
        {
            $array[] = $super->name;
            $array[] =$value->name; 
        }
    }
    }
    // user as Operation Team Leaders 
    if($user_role == '["Operation Team Leaders"]')
    {
      $teamleader  = Teamleader::with('senior')->where('name',$username)->get();
      $array = array();
      foreach ($teamleader as  $leader) 
      {
        foreach ($leader->Senior as $value) 
        {
            $array[] = $value->name;
            $array[] =$leader->name; 
        }
    }
    }

           // user as Quality Specialist
    if($user_role == '["Quality Specialist"]')
    {
        $array = array();
        $array[] = $username; 
    }
    // user as Quality observers
    if($user_role == '["Quality observers"]')
    {
        $array = array();
        $array[] = $username; 
    }

             // print_r($array);
    $users = User::whereIn('name',$array)->get();
    $array = array();
    foreach ($users as  $value) 
    {
      $array[] = $value->id;
    }


      //print_r($array);
    $myModal    = new Additional;
    $DataTable  = Additional::orderBy('id','desc')->whereIn('created_by',$array)->where('status','open')->paginate(6); 

    return view('Quality.Monitor.index',compact('myModal','DataTable'));





    }

    public function close()
    {
        $myModal    = new Additional;
        $DataTable  = Additional::orderBy('id','desc')->where('status','close')->paginate(10);
        return view('Quality.Additional.close',compact('myModal','DataTable'));
    }
        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            $myModal    = new Additional;
            $user_role = \Auth::user()->getRoles()->lists('name');
            $username = \Auth::user()->name;
            $project = Project::with('manager')->where('name',$username)->lists('name','id')->toArray();
            $manger = Manager::with('supervisor')->where('name',$username)->get();
            $supervisor = Supervisor::with('teamleader')->where('name',$username)->get();
            $teamleader  = Teamleader::with('senior')->where('name',$username)->get();
            $senior  = Senior::with('agent')->where('name',$username)->get();
            $agent = Agent::with('senior')->where('name',$username)->get();

            return view('Quality.Additional.create',compact('myModal','manger','supervisor','teamleader','senior','agent','username','user_role'));
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
           //globalfail
            if($request->result == 'fail'){
                $globalfail = Additional::where('agent_id','=',$request->agent_id)->where('created_at','<=','now()')->where('result','=','fail')->whereRaw('created_at >= DATE_SUB(CURDATE(), INTERVAL 90 DAY)')->count();
                $globalfail++;
            }else{
                $globalfail =0;
                $globalfail;
            }
            //create Monitor row
            $request->request->add(['created_by' => Auth::user()->id]);
            $request->request->add(['globalfail' => $globalfail]);
            $Additional= Additional::create($request->all());
            //Coaching
            if($request->result == 'fail'){
                $Coaching = Coaching::create(array('monitor_id' => $Additional->monitor_id,'created_by' => Auth::user()->id));
            }
            //ScoreSheet details
            $TableName  = $request->sheetname.'_'.$request->project_id;
            $request->request->add(['additional_id' =>$Additional->id]);
            $ScoreSheet = DB::table('additional_'.$TableName)->insert([$request->except('_token','_method','manager_id','project_id','supervisor_id','teamleader_id','senior_id','agent_id','monitoring_type','observe_type','call_nature','call_type','customer_name','phone_number','call_duration','hold_duration','call_date','call_time','fcr','customer_satisfaction','bce','ece','nce','total_score','result','attribute_id','created_by','globalfail','sheetname','fcr_reason','updated_by','monitor_id','status')]);
            return redirect()->route('Additional.index');
        }

        /**
         * Display the specified resource.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
     //

        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            //
            
             $myModal    = new Additional;
            $EditData   = Additional::find($id);
            $DataTable  = \App\Project::find($EditData->Monitor->project_id);
            $tabel = 'Additional_'.$DataTable->score_sheet->name.'_'.$DataTable->id;
            $Sheet = DB::table($tabel)->where('additional_id',$id)->first();
            return view('Quality.Additional.edit',compact('myModal','EditData','DataTable','Sheet'));
        }
        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            //
            $request->request->add(['updated_by' => Auth::user()->id]);
            $request->request->add(['status' => 'close']);
            $Additional= Additional::find($id);
            $update = $Additional->update($request->all());
            if($request->result == 'success')
            {
                
            }else{
                $request->request->add(['monitor_id' => $Additional->monitor_id]);
                $request->request->add(['created_by' => Auth::user()->id]);
                $request->request->add(['status' => 'open']);
                $create  = Additional::create($request->except(['bce','ece','nce','total_score','result']));
            }
                //
            $request->request->remove('_method');
            $request->request->remove('monitor_id');
            $request->request->remove('status');
            $request->request->remove('updated_by');
            $request->request->remove('globalfail');
            $request->request->add(['additional_id' => $id]);

            $TableName  = 'additional_'.$request->sheetname.'_'.$request->project_id;                
            $ScoreSheetRow = DB::table($TableName)->where('additional_id', $id)->get();                

            if (empty($ScoreSheetRow)) {
                $ScoreSheet = DB::table($TableName)->insert([$request->except('_token','_method','manager_id','project_id','supervisor_id','teamleader_id','senior_id','agent_id','monitoring_type','observe_type','call_nature','call_type','customer_name','phone_number','call_duration','hold_duration','call_date','call_time','fcr','customer_satisfaction','bce','ece','nce','total_score','result','attribute_id','created_by','globalfail','sheetname','fcr_reason','updated_by','monitor_id','status','add_additional')]);
            }else{
                $ScoreSheet = DB::table($TableName)->where('additional_id', $id)->update($request->except('_method','_token','manager_id','project_id','supervisor_id','teamleader_id','senior_id','agent_id','monitoring_type','observe_type','call_nature','call_type','customer_name','phone_number','call_duration','hold_duration','call_date','call_time','fcr','customer_satisfaction','bce','ece','nce','total_score','result','attribute_id','created_by','fcr_reason','sheetname','updated_by','monitor_id','status','add_additional'));
                
            }
            
            $request->request->add(['additional_id' =>$id]);
            return redirect()->route('Additional.index');
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            //
        }
    }

<?php

namespace App\Http\Controllers\Headcount;

use Illuminate\Http\Request;
use DB;
use Auth;
use Validator;
use Schema;
use App\Agent;
use Input;
use Excel;
use Carbon;
use App\Http\Controllers\Controller;

class AgentcoNtroller extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $myModal    = new Agent;
        $DataTable  = Agent::orderBy('id','desc')->paginate(10);
        return view('Headcount.Agent.index',compact('myModal','DataTable'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $myModal    = new Agent;
        return view('Headcount.Agent.create',compact('myModal'));
    }

    /**
     * Quality a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, ['name' => 'required|unique:agents','id_staff'=>'required|unique:agents','crm_user'=>'required|unique:agents','idPbx'=>'required|unique:agents','windows_account'=>'required|unique:agents']);
        $request->request->add(['created_by' => Auth::user()->id]);
        $Agent = Agent::create($request->except('Seniors_List'));
        $Agent->Senior()->attach($request->Seniors_List);
        return redirect()->route('Agent.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return redirect()->route('Agent.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $myModal    = new Agent;
        $EditData   = Agent::find($id);
        return view('Headcount.Agent.edit',compact('myModal','EditData'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, ['name' => 'required|unique:agents,name,'.$id]);
        $Agent = Agent::find($id);
        $request->request->add(['updated_by' => Auth::user()->id]);
        $update  = $Agent->update($request->except('Seniors_LIst'));
        $Agent->Senior()->sync($request->Seniors_List);
        return redirect()->route('Agent.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Agent::whereId($id)->delete($id);
        return Response()->json($id);
    }
    public function upload()
    {
        //
        return view('Headcount.Agent.upload');
    }
    public function uploaded()
    {
        if (Input::hasFile('file')) 
        {
            $file = Input::file('file');
            Excel::load($file, function ($reader) use (&$counter,&$allarray){
                $results = $reader->get();
                $counter=0;
                $allarray = array();

                //return dd($results->all());
                foreach ($results as $result) 
                {
                    //validator
                    $validator = Validator::make($result->all(), [      
                        'senior_id'         => 'required',
                        'name'              => 'required',
                        'id_staff'          => 'required|unique:agents,id_staff',
                        'crm_user'          => 'required|unique:agents,crm_user',
                        'idpbx'             => 'required|unique:agents,idpbx',
                        'windows_account'   => 'required|unique:agents,windows_account',
                    ]);
                    if ($validator->fails()) {
                        $array = array
                        (
                            'senior_id'         =>  $result->senior_id,
                            'name'              =>  $result->name,
                            'id_staff'          =>  $result->id_staff,
                            'crm_user'          =>  $result->crm_user,
                            'idpbx'             =>  $result->idpbx,
                            'windows_account'   =>  $result->windows_account,
                            'Error'             =>  $validator->errors()->all()[0],
                        );
                        array_push ($allarray, $array);
                    }else{
                        //insert
                        $counter++;
                        $record_id = DB::table('agents')->insertGetId(array(
                            'name'              =>  $result->name,
                            'id_staff'          =>  $result->id_staff,
                            'crm_user'          =>  $result->crm_user,
                            'idpbx'             =>  $result->idpbx,
                            'windows_account'   =>  $result->windows_account,
                            'created_by'        =>  Auth::user()->id,
                            'created_at'        =>  Carbon\Carbon::now()
                        ));
                        $relationship = DB::table('agent_senior')->insertGetId(array(
                            'agent_id'          =>  $record_id,
                            'senior_id'         =>  $result->senior_id,
                            'created_by'        =>  Auth::user()->id,
                            'created_at'        =>  Carbon\Carbon::now()
                        ));
                    }
                }
            },'UTF-8');
            if(!empty($allarray)){
                Excel::create('Errors', function($excel) use($allarray) 
                {
                    $excel->sheet('Errors', function($sheet) use($allarray) 
                    {
                        $sheet->fromArray($allarray);
                    });
                })->export('xls');
            }else{
                return redirect()->route('Agent.index');
            }
        }
    }
}

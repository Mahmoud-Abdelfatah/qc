<?php

namespace App\Http\Controllers\Headcount;

use Illuminate\Http\Request;
use DB;
use Auth;
use Validator;
use Schema;
use App\Teamleader;
use App\Http\Controllers\Controller;

class TeamleaderController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $myModal    = new Teamleader;
        $DataTable  = Teamleader::orderBy('id','desc')->paginate(10);
        return view('Headcount.Teamleader.index',compact('myModal','DataTable'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $myModal    = new Teamleader;
        return view('Headcount.Teamleader.create',compact('myModal'));
    }

    /**
     * Quality a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, ['name' => 'required|unique:teamleaders']);
        $request->request->add(['created_by' => Auth::user()->id]);
        $Teamleader = Teamleader::create($request->except('Supervisors_List'));
        $Teamleader->Supervisor()->attach($request->Supervisors_List);
        return redirect()->route('Teamleader.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return redirect()->route('Teamleader.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $myModal    = new Teamleader;
        $EditData   = Teamleader::find($id);
        return view('Headcount.Teamleader.edit',compact('myModal','EditData'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, ['name' => 'required|unique:teamleaders,name,'.$id]);
        $Teamleader = Teamleader::find($id);
        $request->request->add(['updated_by' => Auth::user()->id]);
        $update  = $Teamleader->update($request->except('Supervisors_List'));
        $Teamleader->Supervisor()->sync($request->Supervisors_List);
        return redirect()->route('Teamleader.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Teamleader::whereId($id)->delete($id);
        return Response()->json($id);
    }
}

<?php

namespace App\Http\Controllers\Headcount;

use Illuminate\Http\Request;
use DB;
use Auth;
use Validator;
use Schema;
use App\Supervisor;
use App\Http\Controllers\Controller;

class SupervisorController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $myModal    = new Supervisor;
        $DataTable  = Supervisor::orderBy('id','desc')->paginate(10);
        return view('Headcount.Supervisor.index',compact('myModal','DataTable'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $myModal    = new Supervisor;
        return view('Headcount.Supervisor.create',compact('myModal'));
    }

    /**
     * Quality a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, ['name' => 'required|unique:supervisors']);
        $request->request->add(['created_by' => Auth::user()->id]);
        $Supervisor = Supervisor::create($request->except('Managers_List'));
        $Supervisor->Manager()->attach($request->Managers_List);
        return redirect()->route('Supervisor.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return redirect()->route('Supervisor.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $myModal    = new Supervisor;
        $EditData   = Supervisor::find($id);
        return view('Headcount.Supervisor.edit',compact('myModal','EditData'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, ['name' => 'required|unique:supervisors,name,'.$id]);
        $Supervisor = Supervisor::find($id);
        $request->request->add(['updated_by' => Auth::user()->id]);
        $update  = $Supervisor->update($request->except('Managers_List'));
        $Supervisor->Manager()->sync($request->Managers_List);
        return redirect()->route('Supervisor.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Supervisor::whereId($id)->delete($id);
        return Response()->json($id);
    }
}

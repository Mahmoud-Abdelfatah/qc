<?php

namespace App\Http\Controllers\Headcount;

use Illuminate\Http\Request;
use DB;
use Auth;
use Validator;
use Schema;
use App\Project;
use App\Http\Controllers\Controller;

class ProjectController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $myModal    = new Project;
        $DataTable  = Project::orderBy('id','desc')->paginate(10);
        return view('Headcount.Project.index',compact('myModal','DataTable'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $myModal    = new Project;
        return view('Headcount.Project.create',compact('myModal'));
    }

    /**
     * Quality a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, ['name' => 'required|unique:projects']);
        $request->request->add(['created_by' => Auth::user()->id]);
        $Project = Project::create($request->all());
        //Create Table
            $ScoreSheet = \App\ScoreSheet::find($request->score_sheet_id);
            $TableName  = $ScoreSheet->name.'_'.$Project->id;

            $array = array();
            foreach ($ScoreSheet->Category as $Category) {
                foreach ($Category->Section as $Section) {
                    foreach ($Section->Attribute as $Attribute) {
                        $array['description_id_'.$Attribute->id] = 'attribute_option_id_'.$Attribute->id;
                    }
                }    
            }
            if (Schema::hasTable($TableName))
            {
            }else{
                Schema::create($TableName, function($table) use($array){
                    $table->engine = 'InnoDB';
                    $table->increments('id')->comment('disable');
                    $table->integer('monitor_id')->unsigned()->index();
                    $table->foreign('monitor_id')->references('id')->on('monitors')->onUpdate('cascade')->onDelete('cascade');
                    foreach($array as $key => $value ){
                        $table->integer($value);
                        $table->text($key);
                    }
                });
                Schema::create('additional_'.$TableName, function($table) use($array){
                    $table->engine = 'InnoDB';
                    $table->increments('id')->comment('disable');
                    $table->integer('additional_id')->unsigned()->index();
                    $table->foreign('additional_id')->references('id')->on('additionals')->onUpdate('cascade')->onDelete('cascade');
                    foreach($array as $key => $value ){
                        $table->integer($value);
                        $table->text($key);
                    }
                });  
            }
        return redirect()->route('Project.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return redirect()->route('Project.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $myModal    = new Project;
        $EditData   = Project::find($id);
        return view('Headcount.Project.edit',compact('myModal','EditData'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, ['name' => 'required|unique:projects,name,'.$id]);
        $Project = Project::findOrFail($id);
        $request->request->add(['updated_by' => Auth::user()->id]);
        $Project = $Project->update($request->all());
        //Edit Table
            $ScoreSheet = \App\ScoreSheet::find($request->score_sheet_id);
            $TableName  = $ScoreSheet->name.'_'.$id;
            $array = array();
            foreach ($ScoreSheet->Category as $Category) {
                foreach ($Category->Section as $Section) {
                    foreach ($Section->Attribute as $Attribute) {
                        $array['description_id_'.$Attribute->id] = 'attribute_option_id_'.$Attribute->id;
                    }
                }    
            }
            //     if (Schema::hasTable($TableName))
            //     {
            //         Schema::table($TableName, function ($table) {
            //             $table->dropForeign(['monitor_id']);
            //         });
            //         Schema::rename($TableName, $TableName.'_'.date('Ymdhis'));
            //         Schema::create($TableName, function($table) use($array){
            //             $table->engine = 'InnoDB';
            //             $table->increments('id')->comment('disable');
            //             $table->integer('monitor_id')->unsigned()->index();
            //             $table->foreign('monitor_id')->references('id')->on('monitors')->onUpdate('cascade')->onDelete('cascade');
            //             foreach($array as $key => $value ){
            //                 $table->integer($value);
            //                 $table->text($key);
            //             }
            //         });
            //     }else{
            //         Schema::create($TableName, function($table) use($array){
            //             $table->engine = 'InnoDB';
            //             $table->increments('id')->comment('disable');
            //             $table->integer('monitor_id')->unique();
            //             foreach($array as $key => $value ){
            //                 $table->integer($value);
            //                 $table->text($key);
            //             }
            //         });
            //     }
            // if (Schema::hasTable('additional_'.$TableName))
            // {
            //     Schema::table('additional_'.$TableName, function ($table) {
            //             $table->dropForeign(['additional_id']);
            //         });
            //     Schema::rename('additional_'.$TableName, 'additional_'.$TableName.'_'.date('Ymdhis'));
            //     Schema::create('additional_'.$TableName, function($table) use($array){
            //         $table->engine = 'InnoDB';
            //         $table->increments('id')->comment('disable');
            //         $table->integer('additional_id')->unsigned()->index();
            //         $table->foreign('additional_id')->references('id')->on('additionals')->onDelete('cascade');
            //         foreach($array as $key => $value ){
            //             $table->integer($value);
            //             $table->text($key);
            //         }
            //     });
            // }else{
            //     Schema::create('additional_'.$TableName, function($table) use($array){
            //         $table->engine = 'InnoDB';
            //         $table->increments('id')->comment('disable');
            //         $table->integer('additional_id')->unsigned()->index();
            //         $table->foreign('additional_id')->references('id')->on('additionals')->onDelete('cascade');
            //         foreach($array as $key => $value ){
            //             $table->integer($value);
            //             $table->text($key);
            //         }
            //     });  
            // }
        
        return redirect()->route('Project.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Project::whereId($id)->delete($id);
        return Response()->json($id);
    }
}
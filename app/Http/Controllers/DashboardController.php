<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;
use Validator;
use App\Monitor;
use App\Coaching;
use App\Sheet;
use DB;
use App\Http\Controllers\Controller;

use App\Project;
use App\Manager;
use App\Supervisor;
use App\Teamleader;
use App\User;
use App\Senior;
use App\Agent;
class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $Monitor      = \App\Monitor::whereRaw('created_at >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)')->count();
        $Coaching     = \App\Coaching::whereRaw('created_at >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)')->count();
        $fail         = \App\Monitor::where('result','fail')->whereRaw('created_at >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)')->count();
        $success      = \App\Monitor::where('result','success')->whereRaw('created_at >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)')->count();

       // $test =User::find(31);

        $username = \Auth::user()->name;
        $manger = Manager::with('supervisor')->where('name',$username)->get();
        $supervisor = Supervisor::with('teamleader')->where('name',$username)->get();
        $teamleader  = Teamleader::with('senior')->where('name',$username)->get();
        $senior  = Senior::with('agent')->where('name',$username)->get();
        $agent = Agent::with('senior')->where('name',$username)->get();
     // $supervisor = Supervisor::all();
      // $supervisor  = \App\Supervisor::all()->where('name','ahmed');
        

        
      
       // $manger = Manager::has('supervisor')->get();
       
       // $leader = Manager::with('Supervisor')->where('supervisor', '=', 1)->get();
        // $manger = Manager::find(1)->Supervisor()->orderBy('name')->get();
        //return $ActiveEmploye

         // $manger = Manager::find(9)->Supervisor()->get();
      // $manger  = DB::table('managers')->where()
         //   ->get();

        // foreach ($supervisor as  $sop) {
       // $manger = Manager::find($sop->manger_id);
        //echo $sop->name .'belong to'.$manger->name.'<br>';

               //   }

        return view('dashboard',compact('success','Monitor','fail','Coaching','supervisor','teamleader','manger','agent','senior'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

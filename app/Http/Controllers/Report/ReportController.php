<?php

namespace App\Http\Controllers\Report;
use Illuminate\Http\Request;
use DB;
use Auth;
use Excel;
use App\Project;
use App\ScoreSheet;
use Validator;
use App\Report;
use App\Http\Controllers\Controller;


class ReportController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects   =  \App\Project::all();
        $supers     =  \App\Supervisor::all();
        $teamleaders=  \App\Teamleader::all();
        $agents     =  \App\Agent::all();
        $user       =  \App\User::all();
        $monitortype= array(''=>'Select','Standard'=>'Standard','additional'=>'additional');
        $observetype= array(''=>'Select','Remotely'=>'Remotely','Side-by-Side'=>'Side-by-Side');
        $array      = array(''=>'Select an option','projects'=>'project','users'=>'Consistancy','departments' => 'Analysis','Full Analysis','employees' =>'agent','Repaeted fail' =>'Repaeted fail','Additional'=>'Additional','Close Coaching'=>'Close Coaching');
        return view('Report.Report.index',compact('array','projects','supers','teamleaders','agents','user','monitortype','observetype'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        //
    }

    /**
     * Quality a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // virable
            //$seach          = $request->searchvalue;
            $from           = $request->startdate;
            $to             = $request->enddate;
            $sheetName      = $request->TypeOfReport;
        //conditions
            $conditions = array();
            if(!empty($request->project)){
                $conditions['monitors.project_id']=$request->project;
            }
            if(!empty($request->super)){
                $conditions['monitors.supervisor_id']=$request->super;
            }
            if(!empty($request->leader)){
                $conditions['monitors.teamleader_id']=$request->leader;
            }
            if(!empty($request->agent)){
                $conditions['monitors.agent_id']=$request->agent;
            }
            if(!empty($request->observers)){
                $conditions['monitors.created_by']=$request->observers;
            }
            if(!empty($request->monitoring)){
                $conditions['monitors.monitoring_type']=$request->monitoring;
            }
            if(!empty($request->observing)){
                $conditions['monitors.observe_type']=$request->observing;
            }
        if($sheetName == 'project'){
            return $this->ProjectReport($from,$to,$sheetName,$conditions);
        }elseif ($sheetName == 'Consistancy') {
            return $this->ConsistancyReport($from,$to,$sheetName,$conditions);
        }elseif ($sheetName == 'Analysis') {
            return $this->newAnalysisReport($from,$to,$sheetName,$conditions);
        }elseif ($sheetName == 'agent') {
            return $this->AgentReport($from,$to,$sheetName,$conditions);
        }elseif ($sheetName == 'Repaeted fail') {
            return $this->RepaetedfailReport($from,$to,$sheetName,$conditions);
        }

        elseif ($sheetName == 'Additional') {
            return $this->Additional($from,$to,$sheetName,$conditions);
        }
        elseif ($sheetName == 'Close Coaching') {
            return $this->CloseCoaching($from,$to,$sheetName,$conditions);
        }
        elseif ($sheetName == 'Full Analysis') {
            return $this->FullAnalysisReport($from,$to,$sheetName,$conditions);
        }        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        //
        $Modal      ='\App\\'.$request->TypeOfReport;
        $myModal    = new $Modal;  
        $DataTable  = $Modal::orderBy('id','desc')->paginate(10);
        return view('Report.Report.table',compact('myModal','DataTable'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        //
        
    }
    //Done
        function ProjectReport($from,$to,$sheetName,$conditions){
            $data  = Report::Where(function ($query) use ($from,$to,$sheetName,$conditions) {$query->whereBetween('monitors.created_at', [$from, $to]);})
                //conditions Done
                    ->Where(function ($query) use ($conditions) 
                    {
                        if(!empty($conditions)) {
                            foreach ($conditions as $key => $value) {
                                    $query->whereIn($key,explode( ',', $value));
                            }
                        }
                    })
                ->join('projects','projects.id','=','monitors.project_id')->join('agents','agents.id','=','agent_id')
                ->select('projects.name as Project','agents.name as Agent')->selectRaw('count(monitors.id) as  "# of Std Call"')->selectRaw('SEC_TO_TIME(SUM(call_duration)) as  "Total Calls Duration"')->selectRaw('SEC_TO_TIME(SUM(hold_duration)) as  "Total Hold Duration"')->selectRaw('SUM(bce) as "# of BCE"')->selectRaw('SUM(ece) as "# of ECE"')->selectRaw('SUM(nce) as "# of NCE"')->selectRaw('CONCAT(ROUND((SUM(total_score) / count(total_score)),2)," %") as "Score Of agents"')->selectRaw('COUNT(CASE WHEN bce > 0 THEN 1 END) AS "Accuracy of BCE"')->selectRaw('COUNT(CASE WHEN ece > 0 THEN 1 END) AS "Accuracy of ECE"')                ->selectRaw('COUNT(CASE WHEN bce > 0 THEN 1 END)  + COUNT(CASE WHEN ece > 0 THEN 1 END) AS "% of HPD"')->selectRaw('COUNT(CASE WHEN nce > 0 THEN 1 END) AS "Accuracy of NCE"')
                ->orderBy('monitors.id','desc')
                ->groupBy('agents.id')
                ->get();

            $total  = Report::Where(function ($query) use ($from,$to,$sheetName,$conditions) {$query->whereBetween('monitors.created_at', [$from, $to]);})
                //conditions Done
                    ->Where(function ($query) use ($conditions) 
                    {
                        if(!empty($conditions)) {
                            foreach ($conditions as $key => $value) {
                                    $query->whereIn($key,explode( ',', $value));
                            }
                        }
                    })
                ->join('projects','projects.id','=','monitors.project_id')->join('agents','agents.id','=','agent_id')
                ->selectRaw('Concat(SUBSTRING(1,0), " ") as A1')->selectRaw('Concat(SUBSTRING(1,0), "Total #") AS Total')->selectRaw('count(monitors.id) as  "# of Std Call"')->selectRaw('Concat(SUBSTRING(1,0), " ") as A2')->selectRaw('Concat(SUBSTRING(1,0), " ") as A3')->selectRaw('SUM(bce) as "# of BCE"')->selectRaw('SUM(ece) as "# of ECE"')->selectRaw('SUM(nce) as "# of NCE"')->selectRaw('CONCAT(ROUND((SUM(total_score) / count(total_score)),2)," %") as "Score Of agents"')->selectRaw('COUNT(CASE WHEN bce > 0 THEN 1 END) AS "Accuracy of BCE"')->selectRaw('COUNT(CASE WHEN ece > 0 THEN 1 END) AS "Accuracy of ECE"')->selectRaw('COUNT(CASE WHEN bce > 0 THEN 1 END) + COUNT(CASE WHEN ece > 0 THEN 1 END) AS "% of HPD"')->selectRaw('COUNT(CASE WHEN nce > 0 THEN 1 END) AS "Accuracy of NCE"')
                ->orderBy('monitors.id','desc')
                ->get();

            $Percent  = Report::Where(function ($query) use ($from,$to,$sheetName,$conditions) {$query->whereBetween('monitors.created_at', [$from, $to]);})
                    //conditions Done
                        ->Where(function ($query) use ($conditions) 
                        {
                            if(!empty($conditions)) {
                                foreach ($conditions as $key => $value) {
                                        $query->whereIn($key,explode( ',', $value));
                                }
                            }
                        })
                    ->join('projects','projects.id','=','monitors.project_id')->join('agents','agents.id','=','agent_id')
                    ->selectRaw('Concat(SUBSTRING(1,0), " ") as A1')->selectRaw('Concat(SUBSTRING(1,0), "Total %") AS Total')->selectRaw('Concat(SUBSTRING(1,0), " ") as A2')->selectRaw('Concat(SUBSTRING(1,0), " ") as A3')->selectRaw('Concat(SUBSTRING(1,0), " ") as A4')->selectRaw('Concat(SUBSTRING(1,0), " ") as A5')->selectRaw('Concat(SUBSTRING(1,0), " ") as A6')->selectRaw('Concat(SUBSTRING(1,0), " ") as A7')->selectRaw('Concat(SUBSTRING(1,0), " ") as A8')->selectRaw('Concat(ROUND(100-( COUNT(CASE WHEN bce > 0 THEN 1 END) / count(monitors.id))*100,2)," %")  as "#BCE"')->selectRaw('Concat(ROUND(100-( COUNT(CASE WHEN ece > 0 THEN 1 END) / count(monitors.id))*100,2)," %")  as "#ECE"')->selectRaw('Concat(ROUND(((COUNT(CASE WHEN bce > 0 THEN 1 END)+ COUNT(CASE WHEN ece > 0 THEN 1 END)) / count(monitors.id))*100,2)," %")  as "#HPD"')->selectRaw('Concat(ROUND(100-( COUNT(CASE WHEN nce > 0 THEN 1 END) / count(monitors.id))*100,2)," %")  as "#NCE"')
                    ->orderBy('monitors.id','desc')
                    ->get();                    
            
            Excel::create($sheetName.'_', function($excel) use($data,$total,$Percent)
            {
                $excel->sheet('Sheet1', function($sheet) use($data,$total,$Percent) 
                {
                    $sheet->cells('A1:M1', function($cells) {
                        // manipulate the cell
                        $cells->setBorder('solid', 'solid', 'solid', 'solid');
                        $cells->setBackground('#2f75b5');
                        $cells->setFontColor('#ffffff');
                    });
                    $sheet->setColumnFormat(array(
                        'I' => '0%',
                    ));
                    
                    $sheet->setAutoFilter();
                    $sheet->fromArray($data);
                    $sheet->fromArray($total, null, 'A1', false, false);
                    $sheet->fromArray($Percent, null, 'A1', false, false);
                });
            })->export('xls');
        }

        function ConsistancyReport($from,$to,$sheetName,$conditions){
            $data  = Report::Where(function ($query) use ($from,$to,$sheetName,$conditions) {$query->whereBetween('monitors.created_at', [$from, $to]);})
                //conditions Done
                    ->Where(function ($query) use ($conditions) 
                    {
                        if(!empty($conditions)) {
                            foreach ($conditions as $key => $value) {
                                    $query->whereIn($key,explode( ',', $value));
                            }
                        }
                    })
                ->join('users','users.id','=','monitors.created_by')
                ->join('actives','actives.id','=','users.active')
                ->select('users.name as Observer')
                ->selectRaw('count(monitors.id) as  "# of Std Call"')
                ->selectRaw('SUM(bce) as "# of BCE"')
                ->selectRaw('SUM(ece) as "# of ECE"')
                ->selectRaw('SUM(nce) as "# of NCE"')
                ->selectRaw('CONCAT(ROUND((SUM(bce) / count(monitors.id)),2) * 100," %") as "% of BCE"')
                ->selectRaw('CONCAT(ROUND((SUM(ece) / count(monitors.id)),2) * 100," %") as "% of ECE"')
                ->selectRaw('CONCAT(ROUND((SUM(nce) / count(monitors.id)),2) * 100," %") as "% of NCE"')
                ->selectRaw('actives.stats as Status')
                ->groupBy('monitors.created_by')
                ->get();

            Excel::create($sheetName.'_', function($excel) use($data)
            {
                $excel->sheet('Sheet1', function($sheet) use($data) 
                {
                    $sheet->cells('A1:I1', function($cells) {
                        // manipulate the cell
                        $cells->setBorder('solid', 'solid', 'solid', 'solid');
                        $cells->setBackground('#2f75b5');
                        $cells->setFontColor('#ffffff');
                    }); 
                    $sheet->setAutoFilter();
                    $sheet->fromArray($data);
                    
                });
            })->export('xls');
        }

       function newAnalysisReport($from,$to,$sheetName,$conditions)
       {
             $project_id = $conditions['monitors.project_id'];
             $project= Project::find($project_id);
             $scoresheet= ScoreSheet::find($project['score_sheet_id']);
             $score_table = $scoresheet['name'].'_'.$project_id;
                   
                   $test = DB::table($score_table)->where('monitor_id',4819)->get();
        $results=DB::table('monitors')
            ->join('agents', 'monitors.agent_id', '=', 'agents.id')
            ->join('users', 'users.id','=','monitors.created_by')
            ->join('projects', 'monitors.project_id', '=', 'projects.id')
            ->join('supervisors', 'monitors.supervisor_id', '=', 'supervisors.id')
            ->join('teamleaders', 'monitors.teamleader_id', '=', 'teamleaders.id')
            ->select('agents.*','monitors.*','projects.score_sheet_id','projects.name as project','supervisors.name as super','teamleaders.name as leader','users.name as Observer_Name')
            ->whereBetween('monitors.created_at', [$from, $to])
            ->where('monitors.project_id', $project_id)
            ->orderBy('agents.id')
            ->orderBy('call_date', 'DESC')
            ->get();
      //  dd($results);
                        $DataTable  = \App\Project::find($conditions['monitors.project_id']);

                return view('Report.Report.analisysreport',compact('results','DataTable'));
       }

        function FullAnalysisReport($from,$to,$sheetName,$conditions){
            $Report  = Report::Where(function ($query) use ($from,$to,$sheetName,$conditions) {$query->whereBetween('monitors.created_at', [$from, $to]);})
                //conditions Done
                    ->Where(function ($query) use ($conditions) 
                    {
                        if(!empty($conditions)) {
                            foreach ($conditions as $key => $value) {
                                    $query->whereIn($key,explode( ',', $value));
                            }
                        }
                    })
                ->Select('monitors.id as id','projects.name as project','supervisors.name as supervisor','teamleaders.name as teamleader','agents.id_staff as id_staff','agents.crm_user as CRM_User','agents.name as agent','agents.agent_type','observe_type as Kind_of_observe','call_nature as Nature_of_the_call','Call_Type','Customer_Name','monitors.Phone_Number as Phone_Number','monitors.call_date as Call_Date','monitors.call_time','monitors.call_duration','monitors.hold_duration','monitors.fcr','monitors.fcr_reason','monitors.customer_satisfaction','users.name as Observer_Name','bce AS BCE','ece AS ECE','nce AS NCE','monitors.total_score as Results_of_calls','monitors.result as Overall','monitors.created_at')
                ->selectRaw('count(CASE WHEN monitors.monitoring_type = "Standard" THEN 1 END) AS "Standard"')->selectRaw('count(CASE WHEN monitors.monitoring_type = "additional" THEN 1 END) AS "additional"')
                ->join ('projects','projects.id','=','monitors.project_id')->join ('users','users.id','=','monitors.created_by')->join ('supervisors','supervisors.id','=','monitors.supervisor_id')->join ('teamleaders','teamleaders.id','=','monitors.teamleader_id')->join('seniors','seniors.id','=','monitors.senior_id')->join('agents','agents.id','=','monitors.agent_id')
                ->groupBy('monitors.id')
                ->orderBy('agents.id')
                ->get();
            $DataTable  = \App\Project::find($conditions['monitors.project_id']);
            return view('Report.Report.report',compact('Report','DataTable'));  
        }

        function AgentReport($from,$to,$sheetName,$conditions){
            $data  = Report::Where(function ($query) use ($from,$to,$sheetName,$conditions) {$query->whereBetween('monitors.created_at', [$from, $to]);})
                //conditions Done
                    ->Where(function ($query) use ($conditions) 
                    {
                        if(!empty($conditions)) {
                            foreach ($conditions as $key => $value) {
                                    $query->whereIn($key,explode( ',', $value));
                            }
                        }
                    })
                ->join('projects','projects.id','=','monitors.project_id')->join('agents','agents.id','=','agent_id')->join('users','users.id','=','monitors.created_by')
                ->select('projects.name as Project','agents.name as Agent')
                ->selectRaw('count(monitors.id) as  "# of Std Call"')->selectRaw('SEC_TO_TIME(SUM(call_duration)) as  "Total Calls Duration"')->selectRaw('SEC_TO_TIME(SUM(hold_duration)) as  "Total Hold Duration"')->selectRaw('SUM(bce) as "# of BCE"')->selectRaw('SUM(ece) as "# of ECE"')->selectRaw('SUM(nce) as "# of NCE"')->selectRaw('CONCAT(ROUND((SUM(total_score) / count(total_score)),2)," %") as "Score Of agents"')->selectRaw('users.name as "Name of Observer"')
                ->orderBy('monitors.id','desc')
                ->groupBy('monitors.id','monitors.agent_id')
                ->get();

            $total  = Report::Where(function ($query) use ($from,$to,$sheetName,$conditions) {$query->whereBetween('monitors.created_at', [$from, $to]);})
                //conditions Done
                    ->Where(function ($query) use ($conditions) 
                    {
                        if(!empty($conditions)) {
                            foreach ($conditions as $key => $value) {
                                    $query->whereIn($key,explode( ',', $value));
                            }
                        }
                    })

                ->join('projects','projects.id','=','monitors.project_id')->join('agents','agents.id','=','agent_id')
                ->selectRaw('Concat(SUBSTRING(1,0), " ") as A1')->selectRaw('Concat(SUBSTRING(1,0), "Total #") AS Total')->selectRaw('count(monitors.id) as  "# of Std Call"')->selectRaw('SEC_TO_TIME(SUM(call_duration)) as  "Total Calls Duration"')->selectRaw('SEC_TO_TIME(SUM(hold_duration)) as  "Total Hold Duration"')->selectRaw('SUM(bce) as "# of BCE"')->selectRaw('SUM(ece) as "# of ECE"')->selectRaw('SUM(nce) as "# of NCE"')->selectRaw('CONCAT(ROUND((SUM(total_score) / count(total_score)),2)," %") as "Score Of agents"')->selectRaw('Concat(SUBSTRING(1,0), " ") as A2')
                ->orderBy('monitors.id','desc')
                ->groupBy('agent_id')
                ->get();
            
            Excel::create($sheetName.'_', function($excel) use($data,$total)
            {
                $excel->sheet('Sheet1', function($sheet) use($data,$total) 
                {
                    $sheet->cells('A1:J1', function($cells) {
                        // manipulate the cell
                        $cells->setBorder('solid', 'solid', 'solid', 'solid');
                        $cells->setBackground('#2f75b5');
                        $cells->setFontColor('#ffffff');
                    });
                    $sheet->setColumnFormat(array(
                        'I' => '0%',
                    ));
                    
                    $sheet->setAutoFilter();
                    $sheet->fromArray($data);
                    $sheet->fromArray($total, null, 'A1', false, false);
                });
            })->export('xls');
        }

        function RepaetedfailReport($from,$to,$sheetName,$conditions){
            
            $Report  = Report::Where(function ($query) use ($from,$to,$sheetName,$conditions) {$query->Where('monitors.globalfail','>','0')->whereBetween('monitors.created_at', [$from, $to]);})
                //conditions Done
                    ->Where(function ($query) use ($conditions) 
                    {
                        if(!empty($conditions)) {
                            foreach ($conditions as $key => $value) {
                                    $query->whereIn($key,explode( ',', $value));
                            }
                        }
                    })
                ->Select('monitors.id as id','agents.name as agent','projects.name as project','agents.idPbx as idPbx','agents.id_staff as id_staff','teamleaders.name as teamleader','supervisors.name as supervisor')
                ->selectRaw('GROUP_CONCAT(monitors.globalfail) as "failnumber"')->selectRaw('GROUP_CONCAT(monitors.created_at) as "faildate"')
                ->join ('projects','projects.id','=','monitors.project_id')->join ('users','users.id','=','monitors.created_by')->join ('supervisors','supervisors.id','=','monitors.supervisor_id')->join ('teamleaders','teamleaders.id','=','monitors.teamleader_id')->join('seniors','seniors.id','=','monitors.senior_id')->join('agents','agents.id','=','monitors.agent_id')
                ->groupBy('monitors.agent_id')
                ->get();
            return view('Report.Report.failreport',compact('Report'));
        }


        function Additional($from,$to,$sheetName,$conditions)
        {
                       $data  = Report::Where(function ($query) use ($from,$to,$sheetName,$conditions) {$query->whereBetween('monitors.created_at', [$from, $to]);})
                //conditions Done
                    ->Where(function ($query) use ($conditions) 
                    {
                        if(!empty($conditions)) {
                            foreach ($conditions as $key => $value) {
                                    $query->whereIn($key,explode( ',', $value));
                            }
                        }
                    })
                ->join('projects','projects.id','=','monitors.project_id')->join('agents','agents.id','=','agent_id')->join('additionals','additionals.monitor_id','=','monitors.id')->join('managers','managers.id','=','additionals.manager_id')->join('supervisors','supervisors.id','=','additionals.supervisor_id')->join('teamleaders','teamleaders.id','=','additionals.teamleader_id')->join('seniors','seniors.id','=','additionals.senior_id')
                ->select('projects.name as Project','agents.name as Agent')
                ->selectRaW('managers.name as Manager')
                ->selectRaw('supervisors.name as Supervisor')
                ->selectRaw('teamleaders.name as Temleader')
                ->selectRaw('seniors.name as Senior')
                ->selectRaw('agents.name as Agent')
                ->selectRaw('additionals.monitoring_type as MonitorType')
                ->selectRaw('additionals.observe_type as ObserveType')
                ->selectRaw('additionals.call_nature as CallNature')
                ->selectRaw('additionals.call_type as CallNType')
                ->selectRaw('additionals.customer_name as CustomerName')
                ->selectRaw('additionals.phone_number as PhoneNumber')
                ->selectRaw('additionals.call_duration as CallDuration')
                ->selectRaw('additionals.hold_duration as Hold')
                ->selectRaw('additionals.call_date as Date')
                ->selectRaw('additionals.call_time as Time')
                ->selectRaw('additionals.fcr as FCR')
                ->selectRaw('additionals.fcr_reason as FCRreason')
                ->selectRaw('additionals.customer_satisfaction as CustomerSatisfaction')
                ->selectRaw('additionals.bce as BCE')
                ->selectRaw('additionals.ece as ECE')
                ->selectRaw('additionals.nce as NCE')
                ->selectRaw('additionals.total_score as TotalScore')
                ->selectRaw('additionals.result as Result')
                ->orderBy('additionals.id','desc')
                ->groupBy('agents.id')
                ->get();

                Excel::create($sheetName.'_', function($excel) use($data)
            {
                $excel->sheet('Sheet1', function($sheet) use($data) 
                {
                    $sheet->cells('A1:X1', function($cells) {
                        // manipulate the cell
                        $cells->setBorder('solid', 'solid', 'solid', 'solid');
                        $cells->setBackground('#2f75b5');
                        $cells->setFontColor('#ffffff');
                    });
                    $sheet->setColumnFormat(array(
                        'I' => '0%',
                    ));
                    
                    $sheet->setAutoFilter();
                    $sheet->fromArray($data);
                   // $sheet->fromArray($total, null, 'A1', false, false);
                });
            })->export('xls');
        }


function CloseCoaching($from,$to,$sheetName,$conditions)
        {
                       $data  = Report::Where(function ($query) use ($from,$to,$sheetName,$conditions) {$query->whereBetween('monitors.created_at', [$from, $to]);})
                //conditions Done
                    ->Where(function ($query) use ($conditions) 
                    {
                        if(!empty($conditions)) {
                            foreach ($conditions as $key => $value) {
                                    $query->whereIn($key,explode( ',', $value));
                            }
                        }
                    })
                ->join('projects','projects.id','=','monitors.project_id')->join('agents','agents.id','=','agent_id')->join('coachings','coachings.monitor_id','=','monitors.id')->where('coachings.status','close')
                ->select('projects.name as Project','agents.name as Agent')
                ->selectRaw('coachings.positive_notes1 as PositivNots1')
                ->selectRaw('coachings.positive_notes2 as PositivNots2')
                ->selectRaw('coachings.positive_notes3 as PositivNots3')
                ->selectRaw('coachings.negative_notes1 as NegativeNots1')
                ->selectRaw('coachings.negative_notes2 as NegativeNots2')
                ->selectRaw('coachings.negative_notes3 as NegativeNots3')
                ->selectRaw('coachings.mistake1 as Mistake1')
                ->selectRaw('coachings.mistake2 as Mistake2')
                ->selectRaw('coachings.mistake3 as Mistake3')
                ->selectRaw('coachings.roote_cause1 as RootCause1')
                ->selectRaw('coachings.roote_cause2 as RootCause2')
                ->selectRaw('coachings.roote_cause3 as RootCause3')
                ->selectRaw('coachings.action1 as Action1')
                ->selectRaw('coachings.action2 as Action2')
                ->selectRaw('coachings.action3 as Action3')
                ->selectRaw('coachings.time_fram1 as TimeFram1')
                ->selectRaw('coachings.time_fram2 as TimeFram2')
                ->selectRaw('coachings.time_fram3 as TimeFram3')
                ->selectRaw('coachings.business_action2 as NegativeNots2')
                ->selectRaw('coachings.business_action2 as NegativeNots2')                
                ->selectRaw('coachings.call_positive_feedback1 as PositiveFeedback1')
                ->selectRaw('coachings.call_positive_feedback2 as PositiveFeedback2')
                ->selectRaw('coachings.call_positive_feedback3 as PositiveFeedback3')
                ->selectRaw('coachings.call_positive_feedback4 as PositiveFeedback4')
                ->selectRaw('coachings.call_positive_feedback5 as PositiveFeedback5')
                ->selectRaw('coachings.call_positive_feedback6 as PositiveFeedback6')
                ->selectRaw('coachings.negative_feedback1 as NigativeFeedback1')
                ->selectRaw('coachings.negative_feedback2 as NigativeFeedback2')
                ->selectRaw('coachings.negative_feedback3 as NigativeFeedback3')
                ->selectRaw('coachings.negative_feedback4 as NigativeFeedback4')
                ->selectRaw('coachings.negative_feedback5 as NigativeFeedback5')
                ->selectRaw('coachings.negative_feedback6 as NigativeFeedback6')
                ->selectRaw('coachings.caoching_validation_status as ValidationStatus')
                ->selectRaw('coachings.description   as Comment')
                ->selectRaw('coachings.status as Status')
                ->orderBy('coachings.id','desc')
                ->groupBy('agents.id')
                ->get();

                Excel::create($sheetName.'_', function($excel) use($data)
            {
                $excel->sheet('Sheet1', function($sheet) use($data) 
                {
                    $sheet->cells('A1:AI1', function($cells) {
                        // manipulate the cell
                        $cells->setBorder('solid', 'solid', 'solid', 'solid');
                        $cells->setBackground('#2f75b5');
                        $cells->setFontColor('#ffffff');
                    });
                    $sheet->setColumnFormat(array(
                        'I' => '0%',
                    ));
                    
                    $sheet->setAutoFilter();
                    $sheet->fromArray($data);
                   // $sheet->fromArray($total, null, 'A1', false, false);
                });
            })->export('xls');
        }
    
}


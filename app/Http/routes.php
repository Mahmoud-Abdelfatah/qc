<?php
ini_set('xdebug.max_nesting_level', 120);

Route::get('500', function()
{
    return 'ww';
});


if(version_compare(PHP_VERSION, '7.2.0', '>=')) {
    error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
}


/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/', function () {
    return redirect('Dashboard');
});




use App\User;
 Route::get('users',function(){
 	return view('test')->with('users',User::paginate(5));
 });

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});

Route::group(['middleware' => 'web'], function () {
    //load Auth 
    Route::auth();
    //Dashboard all
        //
        Route::resource('Dashboard',        'DashboardController');
    //User      admin
        Route::group(['middleware' => 'role:admin || Sadmin || vodadmin'], function () {
                Route::resource('User',        'Auth\UserController');
                                Route::get('uploadUser',         array('as'=>'User.upload'  ,'uses'=>'Auth\UserController@upload'));
               Route::post('uploadUser',         array('as'=>'User.uploaded','uses'=>'Auth\UserController@uploaded'));
        });
    //Headcount admin || qmanger || qsuper
        Route::group(['middleware' => 'role:admin || qmanger || qsuperqobserver || Sadmin || vodadmin' ], function () {
            Route::resource('Project',          'Headcount\ProjectController');
            Route::resource('Manager',          'Headcount\ManagerController');
            Route::resource('Supervisor',       'Headcount\SupervisorController');
            Route::resource('Teamleader',       'Headcount\TeamleaderController');
            Route::resource('Senior',           'Headcount\SeniorController');
            Route::resource('Agent',            'Headcount\AgentController');
            Route::get('uploadAgent',         array('as'=>'Agent.upload'  ,'uses'=>'Headcount\AgentController@upload'));
            Route::post('uploadAgent',        array('as'=>'Agent.uploaded','uses'=>'Headcount\AgentController@uploaded'));
        });
    //Seeting   admin || qmanger || qsuper
        Route::group(['middleware' => 'role:admin || qmanger || qsuper || Sadmin || vodadmin'], function () {
            Route::resource('Location',         'Seeting\LocationController');
            Route::resource('Department',       'Seeting\DepartmentController');
            Route::resource('Deduction',        'Seeting\DeductionController');
            Route::resource('AttributeOption',  'Seeting\AttributeOptionController');
            Route::resource('Attribute',        'Seeting\AttributeController');
            Route::resource('Section',          'Seeting\SectionController');
            Route::resource('Category',         'Seeting\CategoryController');
            Route::resource('ScoreSheet',       'Seeting\ScoreSheetController');
        });
    //Monitor   admin || qmanger || qsupers
        Route::group(['middleware' => 'role:admin || qmanger || oleader || qsuper || qobserver || qspecia || qobserver || oagent || Sadmin || vodadmin'], function () {
            Route::resource('Monitor',          'Quality\MonitorController');
            Route::resource('Coaching',         'Quality\CoachingController');
            Route::get('CloseCoaching',     array('as'=>'Coaching.close','uses'=>'Quality\CoachingController@close'));
            Route::resource('Additional',       'Quality\AdditionalController');
            Route::get('CloseAdditional',   array('as'=>'Additional.close','uses'=>'Quality\AdditionalController@close'));
        });
    //Ajax Controllers
        Route::post('divajax'   , array('as'=>'divajax','uses'=>'AjaxController@divajax'));
        Route::get('Ajaxrelationlist'   , array('as'=>'Ajaxrelationlist','uses'=>'AjaxController@Ajaxrelationlist'));
        Route::get('AjaxDropDownList',  array('as'=>'Ajax.AjaxDropDownList','uses'=>'AjaxController@DropDownList'));
        Route::get('AjaxDropDown',      array('as'=>'Ajax.AjaxDropDown','uses'=>'AjaxController@DropDown'));
        Route::get('AjaxVildation',     array('as'=>'Ajax.AjaxVildation','uses'=>'AjaxController@AjaxVildation'));
        Route::get('AjaxScoreSheet',    array('as'=>'Ajax.AjaxScoreSheet','uses'=>'AjaxController@AjaxScoreSheet'));
        Route::get('Ajaxtable',         array('as'=>'Ajax.Ajaxtable','uses'=>'AjaxController@Ajaxtable'));
        Route::post('Ajaxrow',          array('as'=>'Ajax.Ajaxrow','uses'=>'AjaxController@Ajaxrow'));
        Route::get('ADDLF',             array('as'=>'Ajax.ADDLF','uses'=>'AjaxController@ADDLF'));
                    Route::get('/searches',             array('as'=>'Monitor.test','uses'=>'Quality\MonitorController@test'));
            Route::get('serach/{id}{search}','Quality\MonitorController@serach');
    //Report   admin || qmanger || qsuper
        Route::group(['middleware' => 'role:admin || qmanger || qsuper || oleader || osuper|| omanger || qspecia || qobserver || oagent || Sadmin || training || vodadmin'], function () {
                Route::resource('Report',           'Report\ReportController');
                 Route::resource('Vreport',          'Report\VReportController');
        });

    
    
    



});

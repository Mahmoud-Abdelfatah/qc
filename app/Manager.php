<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Manager extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    //Table Name
        protected $table    = 'managers';
    //COLUMNS which show to Tables
        protected $casts    = ['id','name','Projects_List','created_by'];
    //this COLUMNS to search, insert and update
        protected $fillable = ['id','name','created_by','updated_by'];
    //get model name
        protected $appends  = ['model_name'];
    //SoftDeletes
        use SoftDeletes;
        protected $dates    = ['deleted_at'];
        
    //Stander for any models get name and COLUMNS
        function getModelNameAttribute(){ return 'Manager'; }
        function getTablColumns()       {  
            $array      =   DB::select( DB::raw('SHOW full COLUMNS FROM managers'));
            $array[]    =   (object) array('Field' => 'Projects_List','Comment'=>'list','Type'=>'','Null'=>'NO',);
            return $array;
        }
    //User relationship
        function created_name(){ return $this->hasOne('App\User', 'id', 'created_by');}
        function updated_name(){ return $this->hasOne('App\User', 'id', 'updated_by');}
    //Other relationship
    public function Supervisor(){return $this->belongsToMany('App\Supervisor')->withPivot('manager_id','supervisor_id');}
    public function getSupervisorsListAttribute($value){return $this->Supervisor()->lists('supervisors.id','name')->toArray();}
    
    public function Project(){return $this->belongsToMany('App\Project')->withPivot('project_id','manager_id');}
    public function getProjectsListAttribute($value){return $this->Project()->lists('projects.id','name')->toArray();}
}
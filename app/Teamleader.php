<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Teamleader extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    //Table Name
        protected $table    = 'teamleaders';
    //COLUMNS which show to Tables
        protected $casts    = ['id','name','Supervisors_List','created_by'];
    //this COLUMNS to search, insert and update
        protected $fillable = ['id','name','created_by','updated_by'];
    //get model name
        protected $appends  = ['model_name'];
    //SoftDeletes
        use SoftDeletes;
        protected $dates    = ['deleted_at'];
        
    //Stander for any models get name and COLUMNS
        function getModelNameAttribute(){ return 'Teamleader'; }
        function getTablColumns()       { 
            $array      =   DB::select( DB::raw('SHOW full COLUMNS FROM teamleaders'));
            $array[]    =   (object) array('Field' => 'Supervisors_List','Comment'=>'list','Type'=>'','Null'=>'NO',);
            return $array;

        }
    //User relationship
        function created_name(){ return $this->hasOne('App\User', 'id', 'created_by');}
        function updated_name(){ return $this->hasOne('App\User', 'id', 'updated_by');}
    //Other relationship
    public function Supervisor(){return $this->belongsToMany('App\Supervisor')->withPivot('supervisor_id','teamleader_id');}
    public function getSupervisorsListAttribute($value){return $this->Supervisor()->lists('supervisors.id','name')->toArray();}   

    public function Senior(){return $this->belongsToMany('App\Senior')->withPivot('teamleader_id','senior_id');}
    public function getSeniorsListAttribute($value){return $this->Senior()->lists('seniors.id','name')->toArray();}
}
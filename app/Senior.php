<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Senior extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    //Table Name
        protected $table    = 'seniors';
    //COLUMNS which show to Tables
        protected $casts    = ['id','name','Teamleaders_List','created_by'];
    //this COLUMNS to search, insert and update
        protected $fillable = ['id','name','created_by','updated_by'];
    //get model name
        protected $appends  = ['model_name'];
    //SoftDeletes
        use SoftDeletes;
        protected $dates    = ['deleted_at'];
        
    //Stander for any models get name and COLUMNS
        function getModelNameAttribute(){ return 'Senior'; }
        function getTablColumns()       { 
            $array      =   DB::select( DB::raw('SHOW full COLUMNS FROM seniors'));
            $array[]    =   (object) array('Field' => 'Teamleaders_List','Comment'=>'list','Type'=>'','Null'=>'NO',);
            return $array;

        }
    //User relationship
        function created_name(){ return $this->hasOne('App\User', 'id', 'created_by');}
        function updated_name(){ return $this->hasOne('App\User', 'id', 'updated_by');}
    //Other relationship
    public function Teamleader(){return $this->belongsToMany('App\Teamleader')->withPivot('senior_id','teamleader_id');}
    public function getTeamleadersListAttribute($value){return $this->Teamleader()->lists('teamleaders.id','name')->toArray();}


    public function Agent(){return $this->belongsToMany('App\Agent')->withPivot('agent_id','senior_id');}
    public function getAgentsListAttribute($value){return $this->Agent()->lists('agents.id','name')->toArray();}
}
<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;
class SheetAdditional extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table    = 'sheet_additionals';
    protected $fillable = ['additional_id','attribute_id','attribute_option_id','description',];
    
    public function Coaching()
    {
        return $this->belongsTo('App\Additional');
    }
}

<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;
class Attribute extends Model
{
    protected $table    = 'attributes';
    protected $casts    = ['id','name','rank','Attributeoptions_List','created_by','updated_by',];
    protected $fillable = ['name','rank','created_by','updated_by',];
    protected $appends  = ['model_name'];
    //get table and columns
    function getModelNameAttribute(){return 'Attribute';}
    function getTablColumns()       { 
        $array      =   DB::select( DB::raw('SHOW full COLUMNS FROM attributes'));
        $array[]    =   (object) array('Field' => 'Attributeoptions_List','Comment'=>'list','Type'=>'','Null'=>'NO',);
        return $array;
    }
    //relationship
    function created_name(){ return $this->hasOne('App\User', 'id', 'created_by');}
    function updated_name(){ return $this->hasOne('App\User', 'id', 'updated_by');}    
    
    function AttributeOption(){return $this->belongsToMany('App\AttributeOption');}
    function getAttributeoptionsListAttribute(){return $this->AttributeOption()->lists('attribute_options.id','name')->toArray();}
    function getOptionAttribute(){return $this->AttributeOption()->lists('name','score')->toArray();}

}

<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;
class Coaching extends Model
{
    //
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    //table
        protected $table    = 'coachings';
        protected $casts    = [
            'project_id',
            'observe_type_Modelrelationship',
            'call_type_Modelrelationship',
            'call_nature_Modelrelationship',
            'bce_Modelrelationship',
            'ece_Modelrelationship',
            'nce_Modelrelationship',
            'total_score_Modelrelationship',
            'result_Modelrelationship',
            'created_at',
            'created_by',
            'updated_at',
            'updated_by'
        ];
        protected $fillable = [
            'monitor_id',
            'positive_notes1',
            'positive_notes2',
            'positive_notes3',
            'negative_notes1',
            'negative_notes2',
            'negative_notes3',
            'mistake1',
            'mistake2',
            'mistake3',
            'roote_cause1',
            'roote_cause2',
            'roote_cause3',
            'action1',
            'action2',
            'action3',
            'time_fram1',
            'time_fram2',
            'time_fram3',
            'business_action1',
            'business_action2',
            'call_positive_feedback1',
            'call_positive_feedback2',
            'call_positive_feedback3',
            'call_positive_feedback4',
            'call_positive_feedback5',
            'call_positive_feedback6',
            'negative_feedback1',
            'negative_feedback2',
            'negative_feedback3',
            'negative_feedback4',
            'negative_feedback5',
            'negative_feedback6',
            'caoching_validation_status',
            'status',
            'description',
            'created_by',
            'updated_by'
        ];
        protected $appends  = ['model_name'];
        function getModelNameAttribute(){return 'Coaching';}
        function getTablColumns(){return DB::select( DB::raw('show full columns from coachings'));}
        function created_name(){ return $this->hasOne('App\User', 'id', 'created_by');}
        function updated_name(){ return $this->hasOne('App\User', 'id', 'updated_by');}
    //Monitor
        //
        function Monitor(){return $this->belongsTo('App\Monitor');}
        function project(){return $this->Monitor->project();}
        //function observe_type() {return $this->Monitor->observe_type;}
    //Sheet
        public function score_sheet()   {return $this->belongsTo('App\ScoreSheet');}
        public function Section()       {return $this->belongsTo('App\Section');}
}

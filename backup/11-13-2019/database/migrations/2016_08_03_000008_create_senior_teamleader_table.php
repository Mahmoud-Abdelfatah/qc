<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeniorTeamleaderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('senior_teamleader', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('senior_id')->unsigned()->index();
            $table->foreign('senior_id')->references('id')->on('seniors')->onDelete('cascade');
            $table->integer('teamleader_id')->unsigned()->index();
            $table->foreign('teamleader_id')->references('id')->on('teamleaders')->onDelete('cascade');
            $table->integer('created_by')->comment('disable');
            $table->integer('updated_by')->comment('disable');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
         Schema::drop('senior_teamleader');
    }
}

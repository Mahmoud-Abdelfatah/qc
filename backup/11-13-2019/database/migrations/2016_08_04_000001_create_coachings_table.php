<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoachingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coachings', function (Blueprint $table) {
            $table->increments('id')->comment('disable');
            $table->integer('monitor_id')->unsigned()->index()->comment('hidden');
            $table->foreign('monitor_id')->references('id')->on('monitors')->onDelete('cascade');
            $table->string('positive_notes1');
            $table->string('positive_notes2');
            $table->string('positive_notes3');
            $table->string('negative_notes1');
            $table->string('negative_notes2');
            $table->string('negative_notes3');
            $table->string('mistake1');
            $table->string('mistake2');
            $table->string('mistake3');
            $table->string('roote_cause1');
            $table->string('roote_cause2');
            $table->string('roote_cause3');
            $table->string('action1');
            $table->string('action2');
            $table->string('action3');
            $table->string('time_fram1');
            $table->string('time_fram2');
            $table->string('time_fram3');
            $table->string('business_action1');
            $table->string('business_action2');
            $table->string('call_positive_feedback1');
            $table->string('call_positive_feedback2');
            $table->string('call_positive_feedback3');
            $table->string('call_positive_feedback4');
            $table->string('call_positive_feedback5');
            $table->string('call_positive_feedback6');
            $table->string('negative_feedback1');
            $table->string('negative_feedback2');
            $table->string('negative_feedback3');
            $table->string('negative_feedback4');
            $table->string('negative_feedback5');
            $table->string('negative_feedback6');
            $table->string('caoching_validation_status');
            $table->enum('status', ['close','open'])->default('open');
            $table->text('description')->nullable();
            $table->integer('created_by')->comment('disable');
            $table->integer('updated_by')->comment('disable');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('coachings');
    }
}

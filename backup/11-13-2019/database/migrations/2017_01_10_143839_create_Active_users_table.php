<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActiveUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('actives', function (Blueprint $table) {
            $table->increments('id')->comment('disable');
            $table->string('stats');
            $table->integer('created_by')->comment('disable');
            $table->integer('updated_by')->comment('disable');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('actives');
    }
}

<?php

use Illuminate\Database\Seeder;

class A_08_03_009_AgentSeniorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('agent_senior')->insert([
            ['agent_id' => '1','senior_id'   => '1'],
            ['agent_id' => '2','senior_id'   => '1'],
            ['agent_id' => '3','senior_id'   => '2'],
            ['agent_id' => '4','senior_id'   => '2'],
            ['agent_id' => '5','senior_id'   => '3'],
            ['agent_id' => '6','senior_id'   => '3'],
            ['agent_id' => '7','senior_id'   => '4'],
            ['agent_id' => '8','senior_id'   => '4'],
            ['agent_id' => '9','senior_id'   => '5'],
            ['agent_id' => '10','senior_id'   => '5'],
            ['agent_id' => '11','senior_id'   => '6'],
            ['agent_id' => '12','senior_id'   => '6'],
            ['agent_id' => '13','senior_id'   => '7'],
            ['agent_id' => '14','senior_id'   => '7'],
            ['agent_id' => '15','senior_id'   => '8'],
            ['agent_id' => '16','senior_id'   => '8'],
            ['agent_id' => '17','senior_id'   => '9'],
            ['agent_id' => '18','senior_id'   => '9'],
            ['agent_id' => '19','senior_id'   => '10'],
            ['agent_id' => '20','senior_id'   => '10'],
            ['agent_id' => '21','senior_id'   => '11'],
            ['agent_id' => '22','senior_id'   => '11'],
            ['agent_id' => '23','senior_id'   => '12'],
            ['agent_id' => '24','senior_id'   => '12'],
            ['agent_id' => '25','senior_id'   => '13'],
            ['agent_id' => '26','senior_id'   => '13'],
            ['agent_id' => '27','senior_id'   => '14'],
            ['agent_id' => '28','senior_id'   => '14'],
            ['agent_id' => '29','senior_id'   => '15'],
            ['agent_id' => '30','senior_id'   => '15'],
            ['agent_id' => '31','senior_id'   => '16'],
            ['agent_id' => '32','senior_id'   => '16']
        ]);
    }
}

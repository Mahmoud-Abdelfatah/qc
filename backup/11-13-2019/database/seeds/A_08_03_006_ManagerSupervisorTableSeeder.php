<?php

use Illuminate\Database\Seeder;

class A_08_03_006_ManagerSupervisorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('manager_supervisor')->insert([
            ['manager_id' => '1','supervisor_id'   => '1'],
            ['manager_id' => '1','supervisor_id'   => '2'],
            ['manager_id' => '2','supervisor_id'   => '3'],
            ['manager_id' => '2','supervisor_id'   => '4']
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class A_08_02_001_ScoreSheetsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('score_sheets')->insert([
            'name'          => 'Vodafone_sheet'
        ]);
    }
}

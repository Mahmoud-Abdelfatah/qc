<?php

use Illuminate\Database\Seeder;

class A_08_02_005_AttributesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('attributes')->insert([
            ['name' => 'Mentioned the wrong name of company in the opining/closing','rank'   => '70'],
            ['name' => 'Take a wrong action or didn\'t take action or add more the action on CRM for the same request. adding the requirements data unclear such as email or survey or troubleshooting and cost of service confirmation and ect....','rank'   => '70'],
            ['name' => 'Major update as per Business request','rank'   => '70'],
            ['name' => 'Follows the basics of the sales call/ Breaking the ice','rank'   => '70'],
            ['name' => 'Conducting any require survey','rank'   => '70'],
            ['name' => 'Unnecessarily extended the call','rank'   => '70'],
            ['name' => 'Hang up during the call','rank'   => '70'],
            ['name' => 'Gave the inaccurate/uncompleted/wrong information or troubleshooting','rank'   => '70'],
            ['name' => 'Gave the inaccurate/uncompleted/wrong troubleshooting','rank'   => '70'],
            ['name' => 'Getting or Confirming all requirement data','rank'   => '70'],
            ['name' => 'Incomplete/ Wrong info. or action regarding customer inquiry or problem/status or contact data.  Didn\'t create ticket for customer request','rank'   => '70'],
            ['name' => 'Used oriented language and deal with customer by professional way','rank'   => '70'],
            ['name' => 'Mistreating the customer.  & Dealing with the customer with using a aggressive way ','rank'   => '70'],
            ['name' => 'Used appropriate salutation','rank'   => '4'],
            ['name' => 'Used company name','rank'   => '4'],
            ['name' => 'Used associate\'s full name','rank'   => '4'],
            ['name' => 'Bouncing opening','rank'   => '4'],
            ['name' => 'predict what customer will say & make sure of the information received from customer','rank'   => '4'],
            ['name' => 'Identify customer needs','rank'   => '4'],
            ['name' => 'listen attentively & Dead area Mute','rank'   => '4'],
            ['name' => 'Concentration','rank'   => '4'],
            ['name' => 'Interruption','rank'   => '4'],
            ['name' => ' Breaking the ice/Addressing feelings/be courtesy ','rank'   => '4'],
            ['name' => 'Talking with other agent , laughing and singing.','rank'   => '4'],
            ['name' => 'Absorb customer anger','rank'   => '4'],
            ['name' => 'Used customer name more than once with title & entered his title correctly','rank'   => '4'],
            ['name' => 'Avoids slang and internal business terminology','rank'   => '4'],
            ['name' => 'Speed','rank'   => '4'],
            ['name' => 'Volume','rank'   => '4'],
            ['name' => 'Accent (clarity & pronunciation)','rank'   => '4'],
            ['name' => 'Tone','rank'   => '4'],
            ['name' => 'Asked permission to put on hold or transfer','rank'   => '4'],
            ['name' => 'Clarify and right reason behind transfer','rank'   => '4'],
            ['name' => 'Thanks customer for waiting','rank'   => '4'],
            ['name' => 'Follow Appropriate Call Sequence','rank'   => '4'],
            ['name' => 'Hang up the call before the customer without customer approval','rank'   => '4'],
            ['name' => 'Offers additional help','rank'   => '4'],
            ['name' => 'Thanks customer','rank'   => '4'],
            ['name' => 'Bouncing closing','rank'   => '4']

        ]);
    }
}

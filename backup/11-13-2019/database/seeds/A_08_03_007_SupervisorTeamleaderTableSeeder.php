<?php

use Illuminate\Database\Seeder;

class A_08_03_007_SupervisorTeamleaderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('supervisor_teamleader')->insert([
            ['supervisor_id' => '1','teamleader_id'   => '1'],
            ['supervisor_id' => '1','teamleader_id'   => '2'],
            ['supervisor_id' => '2','teamleader_id'   => '3'],
            ['supervisor_id' => '2','teamleader_id'   => '4'],
            ['supervisor_id' => '3','teamleader_id'   => '4'],
            ['supervisor_id' => '3','teamleader_id'   => '5'],
            ['supervisor_id' => '4','teamleader_id'   => '6'],
            ['supervisor_id' => '4','teamleader_id'   => '7']
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //user login with rolles
            $this->call(A_08_01_000_UsersTableSeeder::class);
            $this->call(A_08_01_001_RolesTableSeeder::class);
            $this->call(A_08_01_002_RoleUserTableSeeder::class);
        // //build system
            $this->call(A_08_02_000_LocationsTableSeeder::class);
            $this->call(A_08_02_001_ScoreSheetsTableSeeder::class);
            $this->call(A_08_02_002_ProjectsTableSeeder::class);
            $this->call(A_08_02_003_CategoriesTableSeeder::class);
            $this->call(A_08_02_004_SectionsTableSeeder::class);
            $this->call(A_08_02_005_AttributesTableSeeder::class);
            $this->call(A_08_02_006_AttributesTableSeeder::class);
            $this->call(A_08_02_007_CategoryScoreSheetTableSeeder::class);
            $this->call(A_08_02_008_CategorySectionTableSeeder::class);
            $this->call(A_08_02_009_AttributeSectionTableSeeder::class);
            $this->call(A_08_02_010_AttributeAttributeOptionTableSeeder::class);
        // //headcount
            $this->call(A_08_03_000_ManagersTableSeeder::class);
            $this->call(A_08_03_001_SupervisorsTableSeeder::class);
            $this->call(A_08_03_002_TeamleadersTableSeeder::class);
            $this->call(A_08_03_003_SeniorsTableSeeder::class);
            $this->call(A_08_03_004_AgentsTableSeeder::class);
            $this->call(A_08_03_005_ManagerProjectTableSeeder::class);
            $this->call(A_08_03_006_ManagerSupervisorTableSeeder::class);
            $this->call(A_08_03_007_SupervisorTeamleaderTableSeeder::class);
            $this->call(A_08_03_008_SeniorTeamleaderTableSeeder::class);
            $this->call(A_08_03_009_AgentSeniorTableSeeder::class);
        //final
            $this->call(A_08_09_000_DeductionsTableSeeder::class);
            $this->call(A_08_09_001_DepartmentsTableSeeder::class);

    }
}

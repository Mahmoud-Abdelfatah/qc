<?php

use Illuminate\Database\Seeder;

class A_08_02_006_AttributesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('attribute_options')->insert([
            ['name' => 'pass','score'   =>1],
            ['name' => 'fail','score'   =>0]
        ]);
    }
}

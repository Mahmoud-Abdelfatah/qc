<?php

use Illuminate\Database\Seeder;

class A_08_03_002_TeamleadersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('Teamleaders')->insert([
            ['name'    =>'Teamleaders Zone A_1_1'],
            ['name'    =>'Teamleaders Zone A_1_2'],
            ['name'    =>'Teamleaders Zone A_2_1'],
            ['name'    =>'Teamleaders Zone A_2_2'],
            ['name'    =>'Teamleaders Zone B_1_1'],
            ['name'    =>'Teamleaders Zone B_1_2'],
            ['name'    =>'Teamleaders Zone B_2_1'],
            ['name'    =>'Teamleaders Zone B_2_2']
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class A_08_03_000_ManagersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('managers')->insert([
            ['name'    =>'Manager Zone A'],
            ['name'    =>'Manager Zone B']
        ]);
    }
}

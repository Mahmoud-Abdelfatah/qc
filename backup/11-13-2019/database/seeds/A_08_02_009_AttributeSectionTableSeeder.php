<?php

use Illuminate\Database\Seeder;

class A_08_02_009_AttributeSectionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('attribute_section')->insert([
            ['attribute_id' => '5','section_id'   => '1'],
            ['attribute_id' => '6','section_id'   => '2'],
            ['attribute_id' => '1','section_id'   => '3'],
            ['attribute_id' => '2','section_id'   => '4'],
            ['attribute_id' => '3','section_id'   => '5'],
            ['attribute_id' => '4','section_id'   => '6'],
            ['attribute_id' => '11','section_id'   => '7'],
            ['attribute_id' => '12','section_id'   => '8'],
            ['attribute_id' => '13','section_id'   => '9'],
            ['attribute_id' => '7','section_id'   => '10'],
            ['attribute_id' => '8','section_id'   => '11'],
            ['attribute_id' => '9','section_id'   => '12'],
            ['attribute_id' => '10','section_id'   => '13'],
            ['attribute_id' => '35','section_id'   => '14'],
            ['attribute_id' => '23','section_id'   => '15'],
            ['attribute_id' => '25','section_id'   => '16'],
            ['attribute_id' => '24','section_id'   => '17'],
            ['attribute_id' => '27','section_id'   => '18'],
            ['attribute_id' => '36','section_id'   => '19'],
            ['attribute_id' => '26','section_id'   => '20'],
            ['attribute_id' => '18','section_id'   => '21'],
            ['attribute_id' => '19','section_id'   => '21'],
            ['attribute_id' => '32','section_id'   => '22'],
            ['attribute_id' => '33','section_id'   => '22'],
            ['attribute_id' => '34','section_id'   => '22'],
            ['attribute_id' => '20','section_id'   => '23'],
            ['attribute_id' => '21','section_id'   => '23'],
            ['attribute_id' => '22','section_id'   => '23'],
            ['attribute_id' => '37','section_id'   => '24'],
            ['attribute_id' => '38','section_id'   => '24'],
            ['attribute_id' => '39','section_id'   => '24'],
            ['attribute_id' => '28','section_id'   => '25'],
            ['attribute_id' => '29','section_id'   => '25'],
            ['attribute_id' => '30','section_id'   => '25'],
            ['attribute_id' => '31','section_id'   => '25'],
            ['attribute_id' => '14','section_id'   => '26'],
            ['attribute_id' => '16','section_id'   => '26'],
            ['attribute_id' => '17','section_id'   => '26'],
            ['attribute_id' => '15','section_id'   => '26']
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class A_08_03_004_AgentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('agents')->insert([
            ['name' =>'Agents Zone A_1_1_1_1','crm_user' => '01' ,'id_staff' => '01' ,'idPbx' => '01' ,'windows_account' => '01'],
            ['name' =>'Agents Zone A_1_1_1_2','crm_user' => '02' ,'id_staff' => '02' ,'idPbx' => '02' ,'windows_account' => '02'],
            ['name' =>'Agents Zone A_1_1_1_1','crm_user' => '03' ,'id_staff' => '03' ,'idPbx' => '03' ,'windows_account' => '03'],
            ['name' =>'Agents Zone A_1_1_1_2','crm_user' => '04' ,'id_staff' => '04' ,'idPbx' => '04' ,'windows_account' => '04'],
            ['name' =>'Agents Zone A_1_1_2_1','crm_user' => '05' ,'id_staff' => '05' ,'idPbx' => '05' ,'windows_account' => '05'],
            ['name' =>'Agents Zone A_1_1_2_2','crm_user' => '06' ,'id_staff' => '06' ,'idPbx' => '06' ,'windows_account' => '06'],
            ['name' =>'Agents Zone A_1_2_1_1','crm_user' => '07' ,'id_staff' => '07' ,'idPbx' => '07' ,'windows_account' => '07'],
            ['name' =>'Agents Zone A_1_2_1_2','crm_user' => '08' ,'id_staff' => '08' ,'idPbx' => '08' ,'windows_account' => '08'],
            ['name' =>'Agents Zone A_1_2_2_1','crm_user' => '09' ,'id_staff' => '09' ,'idPbx' => '09' ,'windows_account' => '09'],
            ['name' =>'Agents Zone A_1_2_2_2','crm_user' => '10' ,'id_staff' => '10' ,'idPbx' => '10' ,'windows_account' => '10'],
            ['name' =>'Agents Zone A_2_1_1_1','crm_user' => '11' ,'id_staff' => '11' ,'idPbx' => '11' ,'windows_account' => '11'],
            ['name' =>'Agents Zone A_2_1_1_2','crm_user' => '12' ,'id_staff' => '12' ,'idPbx' => '12' ,'windows_account' => '12'],
            ['name' =>'Agents Zone A_2_1_2_1','crm_user' => '13' ,'id_staff' => '13' ,'idPbx' => '13' ,'windows_account' => '13'],
            ['name' =>'Agents Zone A_2_1_2_2','crm_user' => '14' ,'id_staff' => '14' ,'idPbx' => '14' ,'windows_account' => '14'],
            ['name' =>'Agents Zone A_2_2_1_1','crm_user' => '15' ,'id_staff' => '15' ,'idPbx' => '15' ,'windows_account' => '15'],
            ['name' =>'Agents Zone A_2_2_1_2','crm_user' => '16' ,'id_staff' => '16' ,'idPbx' => '16' ,'windows_account' => '16'],
            ['name' =>'Agents Zone A_2_2_2_1','crm_user' => '17' ,'id_staff' => '17' ,'idPbx' => '17' ,'windows_account' => '17'],
            ['name' =>'Agents Zone A_2_2_2_2','crm_user' => '18' ,'id_staff' => '18' ,'idPbx' => '18' ,'windows_account' => '18'],
            ['name' =>'Agents Zone B_1_1_1_1','crm_user' => '19' ,'id_staff' => '19' ,'idPbx' => '19' ,'windows_account' => '19'],
            ['name' =>'Agents Zone B_1_1_1_2','crm_user' => '20' ,'id_staff' => '20' ,'idPbx' => '20' ,'windows_account' => '20'],
            ['name' =>'Agents Zone B_1_1_2_1','crm_user' => '21' ,'id_staff' => '21' ,'idPbx' => '21' ,'windows_account' => '21'],
            ['name' =>'Agents Zone B_1_1_2_2','crm_user' => '22' ,'id_staff' => '22' ,'idPbx' => '22' ,'windows_account' => '22'],
            ['name' =>'Agents Zone B_1_2_1_1','crm_user' => '23' ,'id_staff' => '23' ,'idPbx' => '23' ,'windows_account' => '23'],
            ['name' =>'Agents Zone B_1_2_1_2','crm_user' => '24' ,'id_staff' => '24' ,'idPbx' => '24' ,'windows_account' => '24'],
            ['name' =>'Agents Zone B_1_2_2_1','crm_user' => '25' ,'id_staff' => '25' ,'idPbx' => '25' ,'windows_account' => '25'],
            ['name' =>'Agents Zone B_1_2_2_2','crm_user' => '26' ,'id_staff' => '26' ,'idPbx' => '26' ,'windows_account' => '26'],
            ['name' =>'Agents Zone B_2_1_1_1','crm_user' => '27' ,'id_staff' => '27' ,'idPbx' => '27' ,'windows_account' => '27'],
            ['name' =>'Agents Zone B_2_1_1_2','crm_user' => '28' ,'id_staff' => '28' ,'idPbx' => '28' ,'windows_account' => '28'],
            ['name' =>'Agents Zone B_2_1_2_1','crm_user' => '29' ,'id_staff' => '29' ,'idPbx' => '29' ,'windows_account' => '29'],
            ['name' =>'Agents Zone B_2_1_2_2','crm_user' => '30' ,'id_staff' => '30' ,'idPbx' => '30' ,'windows_account' => '30'],
            ['name' =>'Agents Zone B_2_2_1_1','crm_user' => '31' ,'id_staff' => '31' ,'idPbx' => '31' ,'windows_account' => '31'],
            ['name' =>'Agents Zone B_2_2_1_2','crm_user' => '32' ,'id_staff' => '32' ,'idPbx' => '32' ,'windows_account' => '32'],
            ['name' =>'Agents Zone B_2_2_2_1','crm_user' => '33' ,'id_staff' => '33' ,'idPbx' => '33' ,'windows_account' => '33'],
            ['name' =>'Agents Zone B_2_2_2_2','crm_user' => '34' ,'id_staff' => '34' ,'idPbx' => '34' ,'windows_account' => '34']
        ]);
    }
}

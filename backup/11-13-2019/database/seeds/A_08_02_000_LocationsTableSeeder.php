<?php

use Illuminate\Database\Seeder;

class A_08_02_000_LocationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('locations')->insert([
            'name'            => 'Abbasya',
            'description'     => '',
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class A_08_03_008_SeniorTeamleaderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('senior_teamleader')->insert([
            ['senior_id' => '1','teamleader_id'   => '1'],
            ['senior_id' => '2','teamleader_id'   => '1'],
            ['senior_id' => '3','teamleader_id'   => '2'],
            ['senior_id' => '4','teamleader_id'   => '2'],
            ['senior_id' => '5','teamleader_id'   => '3'],
            ['senior_id' => '6','teamleader_id'   => '3'],
            ['senior_id' => '7','teamleader_id'   => '4'],
            ['senior_id' => '8','teamleader_id'   => '4'],
            ['senior_id' => '9','teamleader_id'   => '5'],
            ['senior_id' => '10','teamleader_id'   => '5'],
            ['senior_id' => '11','teamleader_id'   => '6'],
            ['senior_id' => '12','teamleader_id'   => '6'],
            ['senior_id' => '13','teamleader_id'   => '7'],
            ['senior_id' => '14','teamleader_id'   => '7'],
            ['senior_id' => '15','teamleader_id'   => '8'],
            ['senior_id' => '16','teamleader_id'   => '8']
        ]);
    }
}

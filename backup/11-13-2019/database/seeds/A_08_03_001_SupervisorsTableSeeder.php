<?php

use Illuminate\Database\Seeder;

class A_08_03_001_SupervisorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('supervisors')->insert([
            ['name'    =>'supervisors Zone A_1'],
            ['name'    =>'supervisors Zone A_2'],
            ['name'    =>'supervisors Zone B_1'],
            ['name'    =>'supervisors Zone B_2']
        ]);
    }
}

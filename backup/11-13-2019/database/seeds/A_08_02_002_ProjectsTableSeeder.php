<?php

use Illuminate\Database\Seeder;

class A_08_02_002_ProjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('projects')->insert([
            'name'          => 'Vodafone',
            'location_id'   => 1,
            'score_sheet_id'=> 1,
        ]);
    }
}

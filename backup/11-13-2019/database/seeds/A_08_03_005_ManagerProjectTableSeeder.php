<?php

use Illuminate\Database\Seeder;

class A_08_03_005_ManagerProjectTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('manager_project')->insert([
            ['manager_id' => '1','project_id'   => '1'],
            ['manager_id' => '2','project_id'   => '1']
        ]);
    }
}

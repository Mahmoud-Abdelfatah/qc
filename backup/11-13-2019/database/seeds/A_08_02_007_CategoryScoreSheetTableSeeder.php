<?php

use Illuminate\Database\Seeder;

class A_08_02_007_CategoryScoreSheetTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('category_score_sheet')->insert([
          
            ['category_id'=>1,'score_sheet_id'=>1],
            ['category_id'=>2,'score_sheet_id'=>1],
            ['category_id'=>3,'score_sheet_id'=>1]
            
        ]);
    }
}

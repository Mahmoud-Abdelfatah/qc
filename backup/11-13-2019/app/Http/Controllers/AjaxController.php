<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//adds
use DB;
use App\Employee;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AjaxController extends Controller
{
    //
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
        $this->middleware('auth');
    }
    

    public function divajax(Request $request)
    {
        $conditions = array();
            if(!empty($request->project)){$conditions['project_id']=$request->project;}
            if(!empty($request->supervisor)){$conditions['supervisor_id']=$request->supervisor;}
            if(!empty($request->leader)){$conditions['teamleader_id']=$request->leader;}
            if(!empty($request->agent)){$conditions['agent_id']=$request->agent;}
        $Monitor  = \App\Monitor::Where(function  ($query) use ($conditions) {$query->whereRaw('created_at >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)');})->Where(function ($query) use ($conditions){if(!empty($conditions)) {foreach ($conditions as $key => $value) {$query->whereIn($key,explode( ',', $value ));}}})->count();
        $Coaching = \App\Coaching::Where(function ($query) use ($conditions) {$query->whereRaw('coachings.created_at >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)');})->Where(function ($query) use ($conditions){if(!empty($conditions)) {foreach ($conditions as $key => $value) {$query->whereIn($key,explode( ',', $value ));}}})->join('monitors','monitors.id','=','coachings.monitor_id')->count();
        $fail     = \App\Monitor::Where(function  ($query) use ($conditions) {$query->whereRaw('created_at >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)')->where('result','fail');})->Where(function ($query) use ($conditions){if(!empty($conditions)) {foreach ($conditions as $key => $value) {$query->whereIn($key,explode( ',', $value ));}}})->count();
        $success  = \App\Monitor::Where(function  ($query) use ($conditions) {$query->whereRaw('created_at >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)')->where('result','success');})->Where(function ($query) use ($conditions){if(!empty($conditions)) {foreach ($conditions as $key => $value) {$query->whereIn($key,explode( ',', $value ));}}})->count();
        return Response()->json(array('Monitor' => $Monitor,'Coaching' => $Coaching,'fail' => $fail,'success' => $success));
    }


    
    public function DropDown(Request $request)
    {
        if($request->ajax() || 1==1)
        {
            $table      = $request->input('table');
            $appto      = $request->input('appendtolist');
            $data       = DB::table($table)->get();
            return Response()->json($data);
        }
    }

    public function DropDownList(Request $request)
    {
        if($request->ajax() || 1==1)
        {
            $id    		= $request->input('id');
            $table_id   = $request->input('table_id');
            $table  	= $request->input('table');
            $appto  	= $request->input('appendtolist');
            $data		= DB::table($table)->where($table_id,$id)->get();
            return Response()->json($data);
        }
    }

    public function AjaxVildation(Request $request)
    {
        if($request->ajax() || 1==1)
        {
            $table      = $request->input('table');
            $col1       = $request->input('col1');
            $search1    = $request->input('search1');
            $col2       = $request->input('col2');
            $search2    = $request->input('search2');
            $data       = DB::table($table)->where($col1,$search1)->where($col2,$search2)->get();
            return Response()->json($data);
        }
    }        
    public function Ajaxtable(Request $request)
    {
        if($request->ajax() || 1==1)
        {
            //get element
                $rows       = $request->rows;
                $model      = $request->model;
                $search     = $request->search;
                $orderby    = $request->orderby;
                $ordertype  = $request->ordertype;
                $page       = $request->page;
                $conditions = $request->conditions;
                $columns    = $request->columns;
                $groupby    = $request->groupby;
                $key        = $request->key;
                $path       = $request->path;
            //back page and model name
                $backto     = $path.'.'.$request->model.'.pageid';
            //check the Role & permission path
                if($request->model == 'Permission' || $request->model == 'Role'){
                    $App        = '\Bican\Roles\Models\\'.$request->model;
                }else{
                    $App        = '\App\\'.$request->model;    
                }
                $myModal    = new $App;
            //get DataTables
                $DataTable  = $App::orWhere(function ($query) use ($search,$App) 
                    {

                        //Get Table Header
                            $table    = new $App;

                            $table = $table->getFillable();
                            $count = count($table)-1;
                            for ($x = 0; $x <= $count ; $x++) {
                                $query->orwhere($table[$x], "like", '%'.$search.'%');
                            }
                    })
                    //conditions Done
                    ->Where(function ($query) use ($conditions) 
                    {
                        if(!empty($conditions)) {
                            foreach ($conditions as $key => $value) {
                                    $query->whereIn($key,explode( ',', $value ));
                            }
                        }
                    })
                    //Search in columns By columns
                    ->Where(function ($query) use ($columns,$key) 
                    {
                        if(!empty($columns) and $columns != null) {
                            foreach ($columns as $keys => $values) {
                                if (strpos($keys, '_List') == false){
                                    $query->where($keys, '=', $values);
                                }
                            }
                        }else{$query->whereNotNull($key);}
                    })
                    ->orderBy($orderby,$ordertype)
                    ->paginate($rows);
            //back to view 
                    
                $data   = view($backto,compact($DataTable,'DataTable',$myModal,'myModal'))->render();
                return Response()->json($data);    
        }
    }
    public function Ajaxrow(Request $request)
    {
            $id = $request->id;
            $model        = '\App\\'.$request->model;
            return $model::find($id);
    }

    public function AjaxScoreSheet(Request $request)
    {
        if($request->ajax() || 1==1)
        {
            $id         = $request->input('id');
            $table_id   = $request->input('table_id');
            $table      = $request->input('table');
            $DataTable  = \App\Project::find($id);
            //$DataTable  = \App\Project::find(2);
            // foreach ($DataTable->score_sheet->category as $value) {
            //     foreach ($value->section as $section) {
            //         echo var_dump($section->name);
            //     }
            // }
            $data       = view('layouts.sheet',compact('DataTable','Category'))->render();
            return Response()->json($data);
        }
    } 
    public function Ajaxrelationlist(Request $request)
    {
        $id         =   $request->id;
        $App        =   '\App\\'.$request->model;
        $method     =   $request->method;
        $data       =   $App::find($id)->$method;
        return Response()->json($data);
       
    }    
}

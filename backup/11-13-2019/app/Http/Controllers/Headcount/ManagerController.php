<?php

namespace App\Http\Controllers\Headcount;

use Illuminate\Http\Request;
use DB;
use Auth;
use Validator;
use Schema;
use App\Manager;
use App\Http\Controllers\Controller;

class ManagerController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $myModal    = new Manager;
        $DataTable  = Manager::orderBy('id','desc')->paginate(10);
        return view('Headcount.Manager.index',compact('myModal','DataTable'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $myModal    = new Manager;
        return view('Headcount.Manager.create',compact('myModal'));
    }

    /**
     * Quality a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, ['name' => 'required|unique:managers']);
        $request->request->add(['created_by' => Auth::user()->id]);
        $Manager = Manager::create($request->except('Projects_List'));
        $Manager->Project()->attach($request->Projects_List);
        return redirect()->route('Manager.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return redirect()->route('Manager.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $myModal    = new Manager;
        $EditData   = Manager::find($id);
        return view('Headcount.Manager.edit',compact('myModal','EditData'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, ['name' => 'required|unique:managers,name,'.$id]);
        $Manager = Manager::find($id);
        $request->request->add(['updated_by' => Auth::user()->id]);
        $update  = $Manager->update($request->except('Projects_List'));
        $Manager->Project()->sync($request->Projects_List);
        return redirect()->route('Manager.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Manager::whereId($id)->delete($id);
        return Response()->json($id);
    }
}

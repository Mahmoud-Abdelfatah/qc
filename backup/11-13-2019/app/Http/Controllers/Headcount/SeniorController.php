<?php

namespace App\Http\Controllers\Headcount;

use Illuminate\Http\Request;
use DB;
use Auth;
use Validator;
use Schema;
use App\Senior;
use App\Http\Controllers\Controller;

class SeniorController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $myModal    = new Senior;
        $DataTable  = Senior::orderBy('id','desc')->paginate(10);
        return view('Headcount.Senior.index',compact('myModal','DataTable'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $myModal    = new Senior;
        return view('Headcount.Senior.create',compact('myModal'));
    }

    /**
     * Quality a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, ['name' => 'required|unique:seniors']);
        $request->request->add(['created_by' => Auth::user()->id]);
        $Senior = Senior::create($request->except('Teamleaders_List'));
        $Senior->Teamleader()->attach($request->Teamleaders_List);
        return redirect()->route('Senior.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return redirect()->route('Senior.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $myModal    = new Senior;
        $EditData   = Senior::find($id);
        return view('Headcount.Senior.edit',compact('myModal','EditData'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, ['name' => 'required|unique:seniors,name,'.$id]);
        $Senior = Senior::find($id);
        $request->request->add(['updated_by' => Auth::user()->id]);
        $update  = $Senior->update($request->except('Teamleaders_List'));
        $Senior->Teamleader()->sync($request->Teamleaders_List);
        return redirect()->route('Senior.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Senior::whereId($id)->delete($id);
        return Response()->json($id);
    }
}

<?php
namespace App\Http\Controllers\Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use App\User;
use Validator;
use Bican\Roles\Models\Role;
use App\Http\Controllers\Controller;
use DB;
use Schema;
use Input;
use Excel;
use Carbon;

class UserController extends Controller
{
    // use auth  middleware
	public function __construct()
	{
        //
		$this->middleware('auth');
	}

    public function index()
    {
        $myModal    = new user;
        $DataTable  = user::orderBy('id','desc')->paginate(10);
        return view('auth.User.index',compact('myModal','DataTable'));
    }

    public function create()
    {
        $myModal = new User;
        return view('auth.User.create',compact('myModal'));
    }

    public function store(Request $request)
    {
        $this->validate($request, ['name'=>'required','password'=>'required','email'=>'required|email|unique:users,email','roles_list'=>'required']);
        $user = User::create($request->except('roles_list'));
        $user->attachRole($request->roles_list);
        return redirect()->route('User.index');
    }

    public function show($id)
    {
        //
        return redirect()->route('User.index');
    }

    public function edit($id)
    {
    	$myModal    = new User;
        $EditData   = User::find($id);
    	return view('auth.User.edit',compact('myModal','EditData'));
    }

    public function update(Request $request , $id)
    {
        $this->validate($request, ['name'=>'required','password'=>'required','email'=>'required|email|unique:users,email,'.$id,'roles_list'=>'required']);
        $user   = User::find($id);
        $update = $user->update($request->all());
        $user->roles()->sync($request->roles_list);
        return redirect()->route('User.index');
    }

    public function destroy($id)
    {
        User::whereId($id)->delete($id);
        return Response()->json($id);
    }

    public function upload()
    {
        //
        return view('auth.User.upload');
    }

        public function uploaded()
    {
        if (Input::hasFile('file')) 
        {
            $file = Input::file('file');
            Excel::load($file, function ($reader) use (&$counter,&$allarray){
                $results = $reader->get();
                $counter=0;
                $allarray = array();

                //return dd($results->all());
                foreach ($results as $result) 
                {
                    //validator
                    $validator = Validator::make($result->all(), [      
                        'name'              => 'required',
                        'password'              => 'required',
                        'email'          => 'required|email|unique:users,email',
                        //'roles_list'          => 'required',
                        
                    ]);
                    if ($validator->fails()) {
                        $array = array
                        (
                            'name'         =>  $result->name,
                            'password'     =>  $result->password,
                            'email'          =>  $result->email,
                           // 'roles_list'     =>  $result->roles_list,
                            'Error'             =>  $validator->errors()->all()[0],
                        );
                        array_push ($allarray, $array);
                    }else{
                        //insert
                        $counter++;
                        $record_id = DB::table('users')->insertGetId(array(
                            'name'              =>  $result->name,
                            'password'     => bcrypt($result->password),
                            'email'          =>  $result->email,
                            'active'      =>$result->active,
                            'created_by'        =>  Auth::user()->id,
                            'created_at'        =>  Carbon\Carbon::now()
                        ));
                        $relationship = DB::table('role_user')->insertGetId(array(
                            'user_id'          =>  $record_id,
                            'role_id'         =>  $result->role,
                            'created_at'        =>  Carbon\Carbon::now()
                        ));
                    }
                }
            },'UTF-8');
            if(!empty($allarray)){
                Excel::create('Errors', function($excel) use($allarray) 
                {
                    $excel->sheet('Errors', function($sheet) use($allarray) 
                    {
                        $sheet->fromArray($allarray);
                    });
                })->export('xls');
            }else{
                return redirect()->route('User.index');
            }
        }
    }
    
}

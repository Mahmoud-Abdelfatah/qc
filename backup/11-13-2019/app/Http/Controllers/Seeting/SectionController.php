<?php

namespace App\Http\Controllers\Seeting;
use Illuminate\Http\Request;
use Auth;
use Validator;
use App\Section;
use DB;
use App\Http\Controllers\Controller;

class SectionController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $myModal    = new Section;
        $DataTable  = Section::orderBy('id','desc')->paginate(10);
        return view('Seeting.Section.index',compact('myModal','DataTable'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $myModal    = new Section;
        return view('Seeting.Section.create',compact('myModal'));
    }

    /**
     * Seeting a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, ['name' => 'required|unique:sections']);
        $request->request->add(['created_by' => Auth::user()->id]);
        $Section = Section::create($request->except('Attributes_List'));
        $Section->Attribute()->attach($request->Attributes_List);
        return redirect()->route('Section.index');   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $myModal    = new Section;
        $ShowData = Section::find($id);
        return view('Seeting.Section.show',compact('ShowData','myModal'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $myModal    = new Section;
        $EditData   = Section::find($id);
        return view('Seeting.Section.edit',compact('myModal','EditData'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, ['name' => 'required|unique:sections,name,'.$id]);
        $Section = Section::findOrFail($id);
        $request->request->add(['updated_by' => Auth::user()->id]);
        $update = $Section->update($request->all());
        $Section->Attribute()->sync($request->Attributes_List);
        return redirect()->route('Section.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Section::whereId($id)->delete($id);
        return Response()->json($id);
    }
}

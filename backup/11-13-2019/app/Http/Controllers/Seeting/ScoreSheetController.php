<?php

namespace App\Http\Controllers\Seeting;
use Illuminate\Http\Request;
use Auth;
use Validator;
use App\ScoreSheet;
use DB;
use App\Http\Controllers\Controller;

class ScoreSheetController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $myModal    = new Scoresheet;
        $DataTable  = Scoresheet::orderBy('id','desc')->paginate(10);
        return view('Seeting.ScoreSheet.index',compact('myModal','DataTable'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $myModal    = new Scoresheet;
        return view('Seeting.ScoreSheet.create',compact('myModal'));
    }

    /**
     * Seeting a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, ['name' => 'required|unique:score_sheets']);
        $request->request->add(['created_by' => Auth::user()->id]);
        $Scoresheet = Scoresheet::create($request->all());
        $Scoresheet->Category()->attach($request->Categories_List);
        return redirect()->route('ScoreSheet.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $myModal    = new Scoresheet;
        $ShowData = Scoresheet::find($id);
        return view('Seeting.ScoreSheet.show',compact('ShowData','myModal'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $myModal    = new Scoresheet;
        $EditData   = Scoresheet::find($id);
        return view('Seeting.ScoreSheet.edit',compact('myModal','EditData'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, ['name' => 'required|unique:score_sheets,name,'.$id]);
        $Scoresheet = Scoresheet::findOrFail($id);
        $request->request->add(['updated_by' => Auth::user()->id]);
        $update     = $Scoresheet->update($request->all());
        $Scoresheet->Category()->sync($request->Categories_List);
        return redirect()->route('ScoreSheet.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Scoresheet::whereId($id)->delete($id);
        return Response()->json($id);
    }
}

<?php

namespace App\Http\Controllers\Report;
use Illuminate\Http\Request;
use Auth;
use Validator;
use App\Report;
use DB;
use Excel;
use App\Http\Controllers\Controller;


class ReportController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects   =  \App\Project::all();
        $supers     =  \App\Supervisor::all();
        $teamleaders=  \App\Teamleader::all();
        $agents     =  \App\Agent::all();
        $user       =  \App\User::all();
        $monitortype= array(''=>'Select','Standard'=>'Standard','additional'=>'additional');
        $observetype= array(''=>'Select','Remotely'=>'Remotely','Side-by-Side'=>'Side-by-Side');
        $array      = array(''=>'Select an option','projects'=>'project','users'=>'Consistancy','departments' => 'Analysis','employees' =>'agent');
        return view('Report.Report.index',compact('array','projects','supers','teamleaders','agents','user','monitortype','observetype'));

        
        



    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        //
    }

    /**
     * Quality a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // virable
            $sheetName      = $request->TypeOfReport;
            $seach          = $request->searchvalue;
            $from           = $request->startdate;
            $to             = $request->enddate;

        if($sheetName == 'project'){
            return $this->ProjectReport($sheetName,$seach,$from,$to);
        }elseif ($sheetName == 'Consistancy') {
            return $this->ConsistancyReport($sheetName,$seach,$from,$to);
        }elseif ($sheetName == 'Analysis') {
            return $this->AnalysisReport($sheetName,$seach,$from,$to);
        }elseif ($sheetName == 'agent') {
            return $this->AgentReport($sheetName,$seach,$from,$to);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        //
        $Modal      ='\App\\'.$request->TypeOfReport;
        $myModal    = new $Modal;  
        $DataTable  = $Modal::orderBy('id','desc')->paginate(10);
        return view('Report.Report.table',compact('myModal','DataTable'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        
    }
    //Done
        function ProjectReport($sheetName,$seach,$from,$to){
            $data   = Report::where('monitors.project_id',$seach)
                    ->join('projects','projects.id','=','monitors.project_id')
                    ->join('agents','agents.id','=','agent_id')
                    ->select('projects.name as Project','agents.name as Agent')
                    ->selectRaw('count(monitors.id) as  "# of Std Call"')
                    ->selectRaw('SEC_TO_TIME(SUM(call_duration)) as  "Total Calls Duration"')
                    ->selectRaw('SEC_TO_TIME(SUM(hold_duration)) as  "Total Hold Duration"')
                    ->selectRaw('SUM(bce) as "# of BCE"')
                    ->selectRaw('SUM(ece) as "# of ECE"')
                    ->selectRaw('SUM(nce) as "# of NCE"')
                    ->selectRaw('CONCAT(ROUND((SUM(total_score) / count(total_score)),2)," %") as "Score Of agents"')
                    ->selectRaw('COUNT(CASE WHEN bce > 0 THEN 1 END) AS "Accuracy of BCE"')
                    ->selectRaw('COUNT(CASE WHEN ece > 0 THEN 1 END) AS "Accuracy of ECE"')                
                    ->selectRaw('COUNT(CASE WHEN bce > 0 THEN 1 END)  + COUNT(CASE WHEN ece > 0 THEN 1 END) AS "% of HPD"')
                    ->selectRaw('COUNT(CASE WHEN nce > 0 THEN 1 END) AS "Accuracy of NCE"')
                    ->whereBetween('monitors.created_at', [$from, $to])
                    ->orderBy('monitors.id','desc')
                    ->groupBy('agents.id')
                    ->get();
            $total  = Report::where('monitors.project_id',$seach)
                    ->join('projects','projects.id','=','monitors.project_id')
                    ->join('agents','agents.id','=','agent_id')
                    ->selectRaw('Concat(SUBSTRING(1,0), " ") as A1')
                    ->selectRaw('Concat(SUBSTRING(1,0), "Total #") AS Total')
                    ->selectRaw('count(monitors.id) as  "# of Std Call"')
                    ->selectRaw('Concat(SUBSTRING(1,0), " ") as A2')
                    ->selectRaw('Concat(SUBSTRING(1,0), " ") as A3')
                    ->selectRaw('SUM(bce) as "# of BCE"')
                    ->selectRaw('SUM(ece) as "# of ECE"')
                    ->selectRaw('SUM(nce) as "# of NCE"')
                    ->selectRaw('CONCAT(ROUND((SUM(total_score) / count(total_score)),2)," %") as "Score Of agents"')
                    ->selectRaw('COUNT(CASE WHEN bce > 0 THEN 1 END) AS "Accuracy of BCE"')
                    ->selectRaw('COUNT(CASE WHEN ece > 0 THEN 1 END) AS "Accuracy of ECE"')
                    ->selectRaw('COUNT(CASE WHEN bce > 0 THEN 1 END) + COUNT(CASE WHEN ece > 0 THEN 1 END) AS "% of HPD"')                    
                    ->selectRaw('COUNT(CASE WHEN nce > 0 THEN 1 END) AS "Accuracy of NCE"')
                    ->whereBetween('monitors.created_at', [$from, $to])
                    ->orderBy('monitors.id','desc')
                    ->get();
            $Percent= Report::where('monitors.project_id',$seach)
                    ->join('projects','projects.id','=','monitors.project_id')
                    ->join('agents','agents.id','=','agent_id')
                    ->selectRaw('Concat(SUBSTRING(1,0), " ") as A1')
                    ->selectRaw('Concat(SUBSTRING(1,0), "Total %") AS Total')
                    ->selectRaw('Concat(SUBSTRING(1,0), " ") as A2')
                    ->selectRaw('Concat(SUBSTRING(1,0), " ") as A3')
                    ->selectRaw('Concat(SUBSTRING(1,0), " ") as A4')
                    ->selectRaw('Concat(SUBSTRING(1,0), " ") as A5')
                    ->selectRaw('Concat(SUBSTRING(1,0), " ") as A6')
                    ->selectRaw('Concat(SUBSTRING(1,0), " ") as A7')
                    ->selectRaw('Concat(SUBSTRING(1,0), " ") as A8')
                    ->selectRaw('Concat(ROUND(100-( COUNT(CASE WHEN bce > 0 THEN 1 END) / count(monitors.id))*100,2)," %")  as "#BCE"')
                    ->selectRaw('Concat(ROUND(100-( COUNT(CASE WHEN ece > 0 THEN 1 END) / count(monitors.id))*100,2)," %")  as "#ECE"')
                    ->selectRaw('Concat(ROUND(((COUNT(CASE WHEN bce > 0 THEN 1 END)+ COUNT(CASE WHEN ece > 0 THEN 1 END)) / count(monitors.id))*100,2)," %")  as "#HPD"')
                    ->selectRaw('Concat(ROUND(100-( COUNT(CASE WHEN nce > 0 THEN 1 END) / count(monitors.id))*100,2)," %")  as "#NCE"')
                    ->whereBetween('monitors.created_at', [$from, $to])
                    ->orderBy('monitors.id','desc')
                    ->get();                    
            Excel::create($sheetName.'_'.$seach, function($excel) use($data,$total,$Percent)
            {
                $excel->sheet('Sheet1', function($sheet) use($data,$total,$Percent) 
                {
                    $sheet->cells('A1:M1', function($cells) {
                        // manipulate the cell
                        $cells->setBorder('solid', 'solid', 'solid', 'solid');
                        $cells->setBackground('#2f75b5');
                        $cells->setFontColor('#ffffff');
                    });
                    $sheet->setColumnFormat(array(
                        'I' => '0%',
                    ));
                    
                    $sheet->setAutoFilter();
                    $sheet->fromArray($data);
                    $sheet->fromArray($total, null, 'A1', false, false);
                    $sheet->fromArray($Percent, null, 'A1', false, false);
                });
            })->export('xls');
        }

        function ConsistancyReport($sheetName,$seach,$from,$to){


            $data  = Report::whereBetween('monitors.created_at', [$from, $to])
                ->join('users','users.id','=','monitors.created_by')
                ->select('users.name as Observer')
                ->selectRaw('count(monitors.id) as  "# of Std Call"')
                ->selectRaw('SUM(bce) as "# of BCE"')
                ->selectRaw('SUM(ece) as "# of ECE"')
                ->selectRaw('SUM(nce) as "# of NCE"')
                ->selectRaw('CONCAT(ROUND((SUM(bce) / count(monitors.id)),2) * 100," %") as "% of BCE"')
                ->selectRaw('CONCAT(ROUND((SUM(ece) / count(monitors.id)),2) * 100," %") as "% of ECE"')
                ->selectRaw('CONCAT(ROUND((SUM(nce) / count(monitors.id)),2) * 100," %") as "% of NCE"')
                ->groupBy('monitors.created_by')
                ->get();

            Excel::create($sheetName.'_'.$seach, function($excel) use($data)
            {
                $excel->sheet('Sheet1', function($sheet) use($data) 
                {
                    $sheet->cells('A1:H1', function($cells) {
                        // manipulate the cell
                        $cells->setBorder('solid', 'solid', 'solid', 'solid');
                        $cells->setBackground('#2f75b5');
                        $cells->setFontColor('#ffffff');
                    }); 
                    $sheet->setAutoFilter();
                    $sheet->fromArray($data);
                    
                });
            })->export('xls');
        }

        function AnalysisReport($sheetName,$seach,$from,$to){

            $Report  = Report::whereBetween('monitors.created_at', [$from, $to])
                    ->Select(
                            'monitors.id as id',
                            'projects.name as project',
                            'supervisors.name as supervisor',
                            'teamleaders.name as teamleader',
                            'agents.id_staff as id_staff',
                            'agents.crm_user as CRM_User',
                            'agents.name as agent',
                            'observe_type as Kind_of_observe',
                            'call_nature as Nature_of_the_call',
                            'Call_Type',
                            'Customer_Name',
                            'monitors.Phone_Number as Phone_Number',
                            'monitors.call_date as Call_Date',
                            'monitors.call_time',
                            'monitors.call_duration',
                            'monitors.hold_duration',
                            'monitors.fcr',
                            'monitors.fcr_reason',
                            
                            'users.name as Observer_Name',
                            'bce AS BCE',
                            'ece AS ECE',
                            'nce AS NCE',
                            'monitors.total_score as Results_of_calls',
                            'monitors.result as Overall'
                    )
                    ->selectRaw('count(CASE WHEN monitors.monitoring_type = "Standard" THEN 1 END) AS "Standard"')
                    ->selectRaw('count(CASE WHEN monitors.monitoring_type = "additional" THEN 1 END) AS "additional"')
                    ->join ('projects'                  ,'projects.id'  ,'=', 'monitors.project_id')
                    ->join ('users'                     ,'users.id'     ,'=', 'monitors.created_by')
                    ->join ('supervisors'   ,'supervisors.id','=', 'monitors.supervisor_id')
                    ->join ('teamleaders'   ,'teamleaders.id','=', 'monitors.teamleader_id')      
                    ->join ('seniors'       ,'seniors.id'    ,'=', 'monitors.senior_id')
                    ->join ('agents'        ,'agents.id'     ,'=', 'monitors.agent_id')
                    ->groupBy('monitors.id')
                    ->get();
            $DataTable  = \App\Project::find(1);
            return view('Report.Report.report',compact('Report','DataTable'));  
        }

        function AgentReport($sheetName,$seach,$from,$to){

            $data   = Report::where('monitors.agent_id',$seach)
                ->join('projects','projects.id','=','monitors.project_id')
                ->join('agents','agents.id','=','agent_id')
                ->join('users','users.id','=','monitors.created_by')            
                ->select('projects.name as Project','agents.name as Agent')
                ->selectRaw('count(monitors.id) as  "# of Std Call"')
                ->selectRaw('SEC_TO_TIME(SUM(call_duration)) as  "Total Calls Duration"')
                ->selectRaw('SEC_TO_TIME(SUM(hold_duration)) as  "Total Hold Duration"')
                ->selectRaw('SUM(bce) as "# of BCE"')
                ->selectRaw('SUM(ece) as "# of ECE"')
                ->selectRaw('SUM(nce) as "# of NCE"')
                ->selectRaw('CONCAT(ROUND((SUM(total_score) / count(total_score)),2)," %") as "Score Of agents"')
                ->selectRaw('users.name as "Name of Observer"')
                ->whereBetween('monitors.created_at', [$from, $to])
                ->orderBy('monitors.id','desc')
                ->groupBy('monitors.id')
                ->get();
            
            $total  = Report::where('monitors.agent_id',$seach)
                ->join('projects','projects.id','=','monitors.project_id')
                ->join('agents','agents.id','=','agent_id')
                ->selectRaw('Concat(SUBSTRING(1,0), " ") as A1')
                ->selectRaw('Concat(SUBSTRING(1,0), "Total #") AS Total')
                ->selectRaw('count(monitors.id) as  "# of Std Call"')
                ->selectRaw('SEC_TO_TIME(SUM(call_duration)) as  "Total Calls Duration"')
                ->selectRaw('SEC_TO_TIME(SUM(hold_duration)) as  "Total Hold Duration"')
                ->selectRaw('SUM(bce) as "# of BCE"')
                ->selectRaw('SUM(ece) as "# of ECE"')
                ->selectRaw('SUM(nce) as "# of NCE"')
                ->selectRaw('CONCAT(ROUND((SUM(total_score) / count(total_score)),2)," %") as "Score Of agents"')
                ->selectRaw('Concat(SUBSTRING(1,0), " ") as A2')
                ->whereBetween('monitors.created_at', [$from, $to])
                ->orderBy('monitors.id','desc')
                ->groupBy('agent_id')
                ->get();
            
            Excel::create($sheetName.'_'.$seach, function($excel) use($data,$total)
            {
                $excel->sheet('Sheet1', function($sheet) use($data,$total) 
                {
                    $sheet->cells('A1:J1', function($cells) {
                        // manipulate the cell
                        $cells->setBorder('solid', 'solid', 'solid', 'solid');
                        $cells->setBackground('#2f75b5');
                        $cells->setFontColor('#ffffff');
                    });
                    $sheet->setColumnFormat(array(
                        'I' => '0%',
                    ));
                    
                    $sheet->setAutoFilter();
                    $sheet->fromArray($data);
                    $sheet->fromArray($total, null, 'A1', false, false);
                });
            })->export('xls');
        }
    
}


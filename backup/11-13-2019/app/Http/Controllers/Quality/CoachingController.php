<?php

namespace App\Http\Controllers\Quality;
use Illuminate\Http\Request;
use Auth;
use Validator;
use App\Monitor;
use App\Coaching;
use App\Sheet;
use App\Additional;
use App\SheetCoaching;

use DB;
use App\Http\Controllers\Controller;
   

    use App\Project;
    use App\Manager;
    use App\Supervisor;
    use App\Teamleader;
    use App\Senior;
    use App\Agent;
    use App\User;

    class CoachingController extends Controller
    {
        /**
         * Create a new controller instance.
         *
         * @return void
         */
        public function __construct()
        {
            //
            $this->middleware('auth');
        }
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {

         $user_role = \Auth::user()->getRoles()->lists('name');
         $username = \Auth::user()->name;
         $manger = Manager::with('supervisor')->where('name',$username)->get();
         $supervisor = Supervisor::with('teamleader')->where('name',$username)->get();
         $teamleader  = Teamleader::with('senior')->where('name',$username)->get();
         $senior  = Senior::with('agent')->where('name',$username)->get();
         $agent = Agent::with('senior')->where('name',$username)->get();
         $array = array();
     //  user as Admin   
         if($user_role == '["Admin"]')
         {
           $myModal    = new Coaching;
           $DataTable  = Coaching::orderBy('id','desc')->where('status','open')->paginate(6); 
           return view('Quality.Coaching.index',compact('myModal','DataTable'));
       }

     // user as Quality Manger
       if($user_role == '["Quality Manger"]')
       {
        
          $array = array();
          foreach ($manger as  $maneger) 
          {
            foreach ($maneger->Supervisor as $super) 
            {
                foreach ($super->Teamleader as $value) 
                {
                    $array[] = $maneger->name;
                    $array[] = $super->name;
                    $array[] =$value->name;  
                }

            }
        }
    }

     // user as Quality Supervisor
    if($user_role == '["Quality Supervisor"]')
    {
        
      $array = array();
      foreach ($supervisor as  $super) 
      {
        foreach ($super->Teamleader as $value) 
        {
            $array[] = $super->name;
            $array[] =$value->name; 
        }
    }
    }
    // user as Operation Team Leaders 
    if($user_role == '["Operation Team Leaders"]')
    {
      $teamleader  = Teamleader::with('senior')->where('name',$username)->get();
      $array = array();
      foreach ($teamleader as  $leader) 
      {
        foreach ($leader->Senior as $value) 
        {
            $array[] = $value->name;
            $array[] =$leader->name; 
        }
    }
    }

           // user as Quality Specialist
    if($user_role == '["Quality Specialist"]')
    {
        $array = array();
        $array[] = $username; 
    }
    // user as Quality observers
    if($user_role == '["Quality observers"]')
    {
        $array = array();
        $array[] = $username; 
    }

             // print_r($array);
    $users = User::whereIn('name',$array)->get();
    $array = array();
    foreach ($users as  $value) 
    {
      echo $array[] = $value->id;
    }
     

      //print_r($array);
    $myModal    = new Coaching;
    $DataTable  = Coaching::orderBy('id','desc')->whereIn('created_by',$array)->where('status','open')->paginate(6); 
    return view('Quality.Coaching.index',compact('myModal','DataTable'));
    }

    public function close()
    {
        $myModal    = new Coaching;
        $DataTable  = Coaching::orderBy('id','desc')->where('status','close')->paginate(10);
        return view('Quality.Coaching.close',compact('myModal','DataTable'));
    }
        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            //
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
            //
        }

        /**
         * Display the specified resource.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            //
            $myModal    = new Coaching;
            $EditData   = Coaching::find($id);
            $DataTable  = \App\Project::find($EditData->Monitor->project_id);
            $tabel = $DataTable->score_sheet->name.'_'.$DataTable->id;
            $Sheet = DB::table($tabel)->where('monitor_id',$EditData->monitor_id)->first();
            return view('Quality.Coaching.edit',compact('myModal','EditData','DataTable','Sheet'));
        }

        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            //
            $request->request->add(['updated_by' => Auth::user()->id]);
            $request->request->add(['status' => 'close']);
            $Coaching   = Coaching::find($id);
            $update     = $Coaching->update($request->all());
            $Monitor    = Coaching::find($id)->Monitor;
            unset($Monitor['id']);
            $Monitor['monitor_id'] = $Coaching->monitor_id;
            $create  = Additional::create($Monitor->toArray());        
            return redirect()->route('Coaching.index');
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            //
        }
    }

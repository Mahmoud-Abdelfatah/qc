<?php

namespace App\Http\Controllers\Quality;
use Illuminate\Http\Request;
use Auth;
use Validator;
use App\Monitor;
use App\Coaching;
use App\Sheet;
use DB;
use App\Http\Controllers\Controller;



    use App\Project;
    use App\Manager;
    use App\Supervisor;
    use App\Teamleader;
    use App\User;
    use App\Senior;
    use App\Agent;
    class MonitorController extends Controller
    {

        /**
         * Create a new controller instance.
         *
         * @return void
         */
        public function __construct()
        {
            //
            $this->middleware('auth');
        }
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
           $user_role = \Auth::user()->getRoles()->lists('name');
           $username = \Auth::user()->name;
           $manger = Manager::with('supervisor')->where('name',$username)->get();
           $supervisor = Supervisor::with('teamleader')->where('name',$username)->get();
           $teamleader  = Teamleader::with('senior')->where('name',$username)->get();
           $senior  = Senior::with('agent')->where('name',$username)->get();
           $agent = Agent::with('senior')->where('name',$username)->get();
           $array = array();
     //  user as Admin   
         if($user_role == '["Admin"]' || $user_role == '["vodadmin"]' || $user_role == '["SAdmin"]')
         {
           $myModal    = new Monitor;
           $DataTable  = Monitor::orderBy('id','desc')->paginate(10); 
           return view('Quality.Monitor.index',compact('myModal','DataTable'));
        }
  
        // user as Quality Manger
       if($user_role == '["Quality Manger"]')
       {

        //   $array = array();
        //   foreach ($manger as  $maneger) 
        //   {
        //     foreach ($maneger->Supervisor as $super) 
        //     {
        //         foreach ($super->Teamleader as $value) 
        //         {
        //             $array[] = $maneger->name;
        //             $array[] = $super->name;
        //             $array[] =$value->name;  
        //         }

        //     }
        // }

    $manger = Manager::where('name',$username)->get();  // get supervisor which his name = to login user name 
    
    foreach ($manger as $value) {
       $managerid= $value->id;   // geting supervisor id 
    }
    $DataTable  = Monitor::orderBy('id','desc')->where('manager_id',$managerid)->paginate(10);  // geting all calls or monitors wich supervisor name = to  user login name using superisor id 
    }

     // user as Quality Supervisor
    if($user_role == '["Quality Supervisor"]')
    {

    //   $array = array();
    //   foreach ($supervisor as  $super) 
    //   {
    //     foreach ($super->Teamleader as $value) 
    //     {
    //         $array[] = $super->name;
    //         $array[] =$value->name; 
    //     }
    // }
    $super = Supervisor::where('name',$username)->get();  // get supervisor which his name = to login user name 
    
    foreach ($super as $value) {
       $superid= $value->id;   // geting supervisor id 
    }
    $DataTable  = Monitor::orderBy('id','desc')->where('supervisor_id',$superid)->paginate(10);  // geting all calls or monitors wich supervisor name = to  user login name using superisor id 

    }
    // user as Operation Team Leaders 
if($user_role == '["Operation Team Leaders"]')
{
//   $teamleader  = Teamleader::with('senior')->where('name',$username)->get();
//   $array = array();
//   foreach ($teamleader as  $leader) 
//   {
//     foreach ($leader->Senior as $value) 
//     {
//         $array[] = $value->name;
//         $array[] =$leader->name; 
//     }
// }
   $Leader = Teamleader::where('name',$username)->get();  // get supervisor which his name = to login user name 
    
    foreach ($Leader as $value) {
       $leaderid= $value->id;   // geting supervisor id 
    }
    $DataTable  = Monitor::orderBy('id','desc')->where('teamleader_id',$leaderid)->paginate(10);  // geting all calls or monitors wich supervisor name = to  user login name using superisor id 

}

    // user as Quality Specialist
if($user_role == '["Quality Specialist"]')
{
    $array = array();
    $array[] = $username; 
}
    // user as Quality observers
if($user_role == '["Quality observers"]')
{
    $array = array();
    $array[] = $username; 
}
             // print_r($array);
$users = User::whereIn('name',$array)->get();
$array = array();
foreach ($users as  $value) 
{
  $array[] = $value->id;
}


if($user_role == '["Agent"]')
{
    $agent = Agent::with('senior')->where('name',$username)->get();
    foreach ($agent as $value) {
        $agentid = $value->id;
        $DataTable  = Monitor::orderBy('id','desc')->where('agent_id',$agentid)->paginate(10);
    }
}


      //print_r($array);
$myModal    = new Monitor;
//$DataTable  = Monitor::orderBy('id','desc')->whereIn('created_by',$array)->paginate(6); 



return view('Quality.Monitor.index',compact('myModal','DataTable'));
}

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
         public function create()
        {
            //
            $myModal    = new Monitor;
            $user_role = \Auth::user()->getRoles()->lists('name');
            $username = \Auth::user()->name;
            $manger = Manager::with('supervisor')->where('name',$username)->get();
            $supervisor = Supervisor::with('teamleader')->where('name',$username)->get();
            $teamleader  = Teamleader::with('senior')->where('name',$username)->get();
            $senior  = Senior::with('agent')->where('name',$username)->get();
            $agent = Agent::with('senior')->where('name',$username)->get();

            return view('Quality.Monitor.create',compact('myModal','manger','supervisor','teamleader','senior','agent','username','user_role'));
        }
        /**
         * Quality a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
            //globalfail
            if($request->result == 'fail'){
                $globalfail = Monitor::where('agent_id','=',$request->agent_id)->where('created_at','<=','now()')->where('result','=','fail')->whereRaw('created_at >= DATE_SUB(CURDATE(), INTERVAL 90 DAY)')->count();
                $globalfail++;
            }else{
                $globalfail =0;
                $globalfail;
            }
            //create Monitor row
            $request->request->add(['created_by' => Auth::user()->id]);
            $request->request->add(['globalfail' => $globalfail]);
            $Monitor= Monitor::create($request->all());
            //Coaching
            if($request->result == 'fail'){
                $Coaching = Coaching::create(array('monitor_id' => $Monitor->id,'created_by' => Auth::user()->id));
            }
            //ScoreSheet details
            $TableName  = $request->sheetname.'_'.$request->project_id;
            $request->request->add(['monitor_id' =>$Monitor->id]);
            $ScoreSheet = DB::table($TableName)->insert([$request->except('_token','manager_id','project_id','supervisor_id','teamleader_id','senior_id','agent_id','monitoring_type','observe_type','call_nature','call_type','customer_name','phone_number','call_duration','hold_duration','call_date','call_time','fcr','customer_satisfaction','bce','ece','nce','total_score','result','attribute_id','created_by','globalfail','fcr_reason','sheetname')]);
            return redirect()->route('Monitor.index');
        }



             public function test()
        {

            return view('Quality.Monitor.test');
        }

        public function serach(Request $request)
        {
         $user_role = \Auth::user()->getRoles()->lists('name');

         if($request->ajax())
         {


           $a_id=array();
           $search=$_GET['search'];
           $id = $_GET['id'];

           switch ($id) {
               case 'agents':

               $Agent = DB::table('agents')->where('name','like','%'.$search.'%')->get();
               foreach ($Agent as $key => $value) {
                  $a_id[]=$value->id;
                     //$a_id=$value->id;
                  //print_r( $a_id)
                  $agent[]=$value->name;
              }
               //dd($request);
              $output="";
              $Monitor = DB::table('monitors')->whereIn('agent_id',$a_id)->get();
             //$Monitor = DB::table('monitors')->where('agent_id',$a_id)->get();
              foreach ($Monitor as $monitor) {
                $project = DB::table('projects')->where('id', $monitor->project_id)->first();
                $superisor=DB::table('supervisors')->where('id', $monitor->supervisor_id)->first();
                $teamleader=DB::table('teamleaders')->where('id', $monitor->teamleader_id)->first();
                $senior=DB::table('seniors')->where('id', $monitor->senior_id)->first();
                $agent=DB::table('agents')->where('id', $monitor->agent_id)->first();

                if($user_role == '["Admin"]')
                {
                   $output.='<tr>'.
                   '<td>'.$monitor->id.'</td>'.
                   '<td>'.$project->name.'</td>'.
                   '<td>'. $superisor->name.'</td>'.
                   '<td>'. $teamleader->name.'</td>'.
                   '<td>'. $senior->name.'</td>'.
                   '<td>'. $agent->name.'</td>'.
                   '<td>'.$monitor->monitoring_type.'</td>'.
                     '<td>'.$monitor->observe_type.'</td>'.
                     '<td>'.$monitor->call_type.'</td>'.
                     '<td>'.$monitor->call_nature.'</td>'.
                     '<td>'.$monitor->created_at.'</td>'.
                     '<td>'.$monitor->bce.'</td>'.
                     '<td>'.$monitor->ece.'</td>'.
                     '<td>'.$monitor->nce.'</td>'.
                     '<td>'.$monitor->total_score.'</td>'.
                     '<td>'.$monitor->result.'</td>'.
                   '<td class="text-center col-action col-md-1">'.
                   '<a            href="http://192.168.40.13:9090/Monitor/'.$monitor->id .'/edit" class="" >'.'<i class="glyphicon glyphicon-pencil">'.'</i>'.'</a>'.
                   '<a data-route="http://192.168.40.13:9090/Monitor/'.$monitor->id .'" id="'.$monitor->id .'" data-token="'. csrf_token().'" type="button" class="destroy">'.'<i class="glyphicon glyphicon-remove">'.'</i>'.'</a>'.
                   '</td>'.
                   '</tr>';

                                    // $output=[$agent,$project]
                   ;
               }elseif ($user_role == '["Operation Team Leaders"]') {

                  $output.='<tr>'.
                  '<td>'.$monitor->id.'</td>'.
                  '<td>'.$project->name.'</td>'.
                  '<td>'. $superisor->name.'</td>'.
                  '<td>'. $teamleader->name.'</td>'.
                  '<td>'. $senior->name.'</td>'.
                  '<td>'. $agent->name.'</td>'.
                  '</tr>';
              }
              
          }
          break;

          case 'teamleaders':
               
              $Teamleader = DB::table('teamleaders')->where('name','like','%'.$search.'%')->get();
               foreach ($Teamleader as $key => $value) {
                  $a_id[]=$value->id;
                     //$a_id=$value->id;
                  //print_r( $a_id)
                  $teamleader[]=$value->name;
              }
               //dd($request);
              $output="";
              $Monitor = DB::table('monitors')->whereIn('teamleader_id',$a_id)->get();
             //$Monitor = DB::table('monitors')->where('agent_id',$a_id)->get();
              foreach ($Monitor as $monitor) {
                $project = DB::table('projects')->where('id', $monitor->project_id)->first();
                $superisor=DB::table('supervisors')->where('id', $monitor->supervisor_id)->first();
                $teamleader=DB::table('teamleaders')->where('id', $monitor->teamleader_id)->first();
                $senior=DB::table('seniors')->where('id', $monitor->senior_id)->first();
                $agent=DB::table('agents')->where('id', $monitor->agent_id)->first();

                if($user_role == '["Admin"]')
                {
                   $output.='<tr>'.
                    '<td>'.$monitor->id.'</td>'.
                    '<td>'.$project->name.'</td>'.
                    '<td>'. $superisor->name.'</td>'.
                    '<td>'. $teamleader->name.'</td>'.
                    '<td>'. $senior->name.'</td>'.
                    '<td>'. $agent->name.'</td>'.
                     '<td>'.$monitor->monitoring_type.'</td>'.
                     '<td>'.$monitor->observe_type.'</td>'.
                     '<td>'.$monitor->call_type.'</td>'.
                     '<td>'.$monitor->call_nature.'</td>'.
                     '<td>'.$monitor->created_at.'</td>'.
                     '<td>'.$monitor->bce.'</td>'.
                     '<td>'.$monitor->ece.'</td>'.
                     '<td>'.$monitor->nce.'</td>'.
                     '<td>'.$monitor->total_score.'</td>'.
                     '<td>'.$monitor->result.'</td>'.
                    '<td class="text-center col-action col-md-1">'.
                    '<a            href="http://192.168.65.80:9090/Monitor/'.$monitor->id .'/edit" class="" >'.'<i class="glyphicon glyphicon-pencil">'.'</i>'.'</a>'.
                    '<a data-route="http://192.168.65.80:9090/Monitor/'.$monitor->id .'" id="'.$monitor->id .'" data-token="'. csrf_token().'" type="button" class="destroy">'.'<i class="glyphicon glyphicon-remove">'.'</i>'.'</a>'.
                    '</td>'.
                   '</tr>';

                                    // $output=[$agent,$project]
                   ;
               }elseif ($user_role == '["Operation Team Leaders"]') {

                  $output.='<tr>'.
                  '<td>'.$monitor->id.'</td>'.
                  '<td>'.$project->name.'</td>'.
                  '<td>'. $superisor->name.'</td>'.
                  '<td>'. $teamleader->name.'</td>'.
                  '<td>'. $senior->name.'</td>'.
                  '<td>'. $agent->name.'</td>'.
                  '</tr>';
              }
              
          }    
          break;

          default:
                         # code...
          break;
      }


      return Response()->json($output);
  }


}

        /**
         * Display the specified resource.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
            $myModal    = new Monitor;
            $ShowData = Monitor::find($id);
            $DataTable=\App\Project::find($ShowData->project_id);
            $tabel = $DataTable->score_sheet->name.'_'.$DataTable->id;
            $Sheet = DB::table($tabel)->where('monitor_id',$id)->first();
            //dd($ShowData->Sheet);
            return view('Quality.Monitor.show',compact('ShowData','myModal','DataTable','Sheet'));
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            //
            $myModal    = new Monitor;
            $EditData   = Monitor::find($id);
            $DataTable  = \App\Project::find($EditData->project_id);
            $tabel = $DataTable->score_sheet->name.'_'.$DataTable->id;
            $Sheet = DB::table($tabel)->where('monitor_id',$id)->first();
            return view('Quality.Monitor.edit',compact('myModal','EditData','DataTable','Sheet'));
        }

        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            //
            $request->request->add(['updated_by' => Auth::user()->id]);
            $Monitor= Monitor::find($id);
            $Monitor->Coaching()->delete();
            //globalfail
            if($request->result == 'success')
            {
                $request->request->add(['globalfail' => 0]);
                if ($Monitor->globalfail > 0) {
                    DB::statement("UPDATE monitors SET globalfail= CASE WHEN result='fail' THEN globalfail-1 ELSE globalfail END  where  created_at >= DATE_SUB(CURDATE(), INTERVAL 90 DAY) and agent_id   = '".$Monitor->agent_id."' and globalfail > '".$Monitor->globalfail."'");
                }
            }else{
                $globalfail = Monitor::where('id','!=',$id)
                ->where('created_at','<=','now()')
                ->where('agent_id',$request->agent_id)
                ->where('result','=','fail')
                ->whereRaw('created_at >= DATE_SUB(CURDATE(), INTERVAL 90 DAY)')
                ->count();
                $globalfail++;
                $request->request->add(['globalfail' => $globalfail]);
                $Coaching   = Coaching::create(array('monitor_id' => $Monitor->id,'created_by' => Auth::user()->id));
            }
            //Monitor
                //
            $update = $Monitor->update($request->all());
            //ScoreSheet details
            $TableName  = $request->sheetname.'_'.$request->project_id;
            $ScoreSheetRow = DB::table($TableName)->where('monitor_id', $id)->first();
            if (!empty($ScoreSheetRow)) {
                $ScoreSheet = DB::table($TableName)->where('monitor_id', $id)->update($request->except('_token','_method','manager_id','project_id','supervisor_id','teamleader_id','senior_id','agent_id','monitoring_type','observe_type','call_nature','call_type','customer_name','phone_number','call_duration','hold_duration','call_date','call_time','fcr','customer_satisfaction','bce','ece','nce','total_score','result','attribute_id','created_by','fcr_reason','globalfail','sheetname','updated_by'));
            }else{
                $request->request->add(['monitor_id' => $id]);
                $ScoreSheet = DB::table($TableName)->insert([$request->except('_token','_method','manager_id','project_id','supervisor_id','teamleader_id','senior_id','agent_id','monitoring_type','observe_type','call_nature','call_type','customer_name','phone_number','call_duration','hold_duration','call_date','call_time','fcr','customer_satisfaction','bce','ece','nce','total_score','result','attribute_id','created_by','globalfail','sheetname','fcr_reason','updated_by')]);

            }
            return redirect()->route('Monitor.index');
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            //
            $Monitor= Monitor::find($id);
            $myfile = fopen("log.txt", "a") or die("Unable to open file!");
            $name = 'User = '.\Auth::user()->name.'    Monitor Id = '.$Monitor['id'].'    Created At = '.$Monitor['created_at'].'    Deleted At = '.date('Y-m-d H:i:s').PHP_EOL;
            fwrite($myfile, $name);
            fclose($myfile);      
            DB::select("INSERT INTO dmonitors (id,project_id,manager_id,supervisor_id,teamleader_id,senior_id,agent_id,monitoring_type,observe_type,call_nature,call_type, 
customer_name,phone_number,call_duration,hold_duration,call_date,call_time,fcr,fcr_reason,customer_satisfaction,bce,ece,nce,total_score,result,globalfail,description,created_by,updated_by,created_at,updated_at) select id,project_id,manager_id,supervisor_id,teamleader_id,senior_id,agent_id,monitoring_type,observe_type,call_nature,call_type, 
customer_name,phone_number,call_duration,hold_duration,call_date,call_time,fcr,fcr_reason,customer_satisfaction,bce,ece,nce,total_score,result,globalfail,description,created_by,updated_by,created_at,updated_at from monitors  where id=".$id." ");      
            $Monitor->Additional()->delete();
            $Monitor->Coaching()->delete();
            Monitor::whereId($id)->delete($id);
            return Response()->json($id);

        }
    }

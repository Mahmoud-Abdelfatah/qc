<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Project extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    //Tables
        protected $table    = 'projects';
        protected $casts    = ['id','name','location_id','score_sheet_id','created_at','created_by','updated_by'];
        protected $fillable = ['name','location_id','score_sheet_id','created_by','updated_by','description'];
        protected $appends  = ['model_name'];
    //SoftDeletes
        use SoftDeletes;
        protected $dates    = ['deleted_at'];
    //get table and columns
        function getModelNameAttribute(){return 'Project';}
        function getTablColumns(){return DB::select( DB::raw('show full columns from projects'));}
    //User relationship
        function created_name(){ return $this->hasOne('App\User', 'id', 'created_by');}
        function updated_name(){ return $this->hasOne('App\User', 'id', 'updated_by');}
    //Other relationship
    function location(){return $this->belongsTo('App\Location');}
    function score_sheet(){return $this->belongsTo('App\ScoreSheet');}
    public function Manager(){return $this->belongsToMany('App\Manager')->withPivot('manager_id','project_id');}
    public function getSubcategoryListAttribute($value){return $this->Manager()->lists('managers.id','name')->toArray();}  
}


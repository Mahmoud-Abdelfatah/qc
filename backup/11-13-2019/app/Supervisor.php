<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Supervisor extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    //Table Name
        protected $table    = 'supervisors';
    //COLUMNS which show to Tables
        protected $casts    = ['id','name','Managers_List','created_by'];
    //this COLUMNS to search, insert and update
        protected $fillable = ['id','name','created_by','updated_by'];
    //get model name
        protected $appends  = ['model_name'];
    //SoftDeletes
        use SoftDeletes;
        protected $dates    = ['deleted_at'];
        
    //Stander for any models get name and COLUMNS
        function getModelNameAttribute(){ return 'Supervisor'; }
        function getTablColumns()       { 
            $array      =   DB::select( DB::raw('SHOW full COLUMNS FROM supervisors'));
            $array[]    =   (object) array('Field' => 'Managers_List','Comment'=>'list','Type'=>'','Null'=>'NO',);
            return $array;

        }
    //User relationship
        function created_name(){ return $this->hasOne('App\User', 'id', 'created_by');}
        function updated_name(){ return $this->hasOne('App\User', 'id', 'updated_by');}
    //Other relationship
    public function Manager(){return $this->belongsToMany('App\Manager')->withPivot('supervisor_id','manager_id');}
    public function getManagersListAttribute($value){return $this->Manager()->lists('managers.id','name')->toArray();}

    public function Teamleader(){return $this->belongsToMany('App\Teamleader')->withPivot('supervisor_id','teamleader_id');}
    public function getTeamleadersListAttribute($value){return $this->Teamleader()->lists('teamleaders.id','name')->toArray();}
}
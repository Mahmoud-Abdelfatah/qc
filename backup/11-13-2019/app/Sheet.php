<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;
class Sheet extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table    = 'sheets';
    protected $fillable = ['monitor_id','attribute_id','attribute_option_id','description',];
    
    public function Monitor()
    {
        return $this->belongsTo('App\Monitor');
    }
}

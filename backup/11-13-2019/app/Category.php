<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;
class Category extends Model
{
    protected $table    = 'categories';
    protected  $casts   = ['id','name','sections_List','created_by',];
    protected $fillable = ['name','description','created_by','updated_by',];
    protected $appends  = ['model_name'];
    //get table and columns
    function getModelNameAttribute(){return 'Category';}
    function getTablColumns()       { 
        $array      =   DB::select( DB::raw('SHOW full COLUMNS FROM categories'));
        $array[]    =   (object) array('Field' => 'Sections_List','Comment'=>'list','Type'=>'','Null'=>'NO',);
        return $array;
    }
    //relationship
    function created_name(){ return $this->hasOne('App\User', 'id', 'created_by');}
    function updated_name(){ return $this->hasOne('App\User', 'id', 'updated_by');}
    function Section(){return $this->belongsToMany('App\Section')->orderBy('sort');}
    function getSectionsListAttribute(){return $this->Section()->lists('sections.id','name')->toArray();}
}


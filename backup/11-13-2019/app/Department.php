<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;
class Department extends Model
{
    protected $table        = 'departments';
    protected $casts        = ['id','name','created_by',];
    protected $fillable     = ['name','created_by','updated_by','description'];
    protected $appends      = ['model_name'];
    //get table and columns
    function getModelNameAttribute(){return 'Department';}
    function getTablColumns(){return DB::select( DB::raw('show full columns from departments'));}
    //relationship
    function created_name(){ return $this->hasOne('App\User', 'id', 'created_by');}
    function updated_name(){ return $this->hasOne('App\User', 'id', 'updated_by');}

}


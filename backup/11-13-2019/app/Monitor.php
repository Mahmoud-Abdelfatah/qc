<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;
class Monitor extends Model
{
    //
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    //table
        protected $table    = 'monitors';
        protected $casts    = ['id','project_id','supervisor_id','teamleader_id','senior_id','agent_id','monitoring_type','observe_type','call_type','call_nature','call_date','bce','ece','nce','total_score','result','Additional_count','globalfail','deduction_id','created_at','created_by','updated_at','updated_by'];
        protected $fillable = ['project_id','manager_id','supervisor_id','teamleader_id','senior_id','agent_id','monitoring_type','observe_type','call_nature','call_type','customer_name','phone_number','call_duration','hold_duration','call_date','call_time','fcr','fcr_reason','customer_satisfaction','bce','ece','nce','total_score','result','globalfail','description','created_by','updated_by'];
        protected $appends  = ['model_name'];
        function getModelNameAttribute(){return 'Monitor';}
        function getTablColumns(){return DB::select( DB::raw('show full columns from monitors'));}
    //user relationship
        function created_name(){ return $this->hasOne('App\User', 'id', 'created_by');}
        function updated_name(){ return $this->hasOne('App\User', 'id', 'updated_by');}
    //Headcount 
        public function project()       {return $this->belongsTo('App\Project');}
        public function manager()       {return $this->belongsTo('App\Manager');}
        public function supervisor()    {return $this->belongsTo('App\Supervisor');}
        public function teamleader()    {return $this->belongsTo('App\Teamleader');}
        public function senior()        {return $this->belongsTo('App\Senior');}
        public function agent()         {return $this->belongsTo('App\Agent');}    
    //Monitor Coaching Additional
        function Coaching()     {return $this->hasOne('App\Coaching');}
        function Additional()   {return $this->hasMany('App\Additional');}
        function deduction()    {return $this->belongsTo('App\Deduction','globalfail','level');}
    //score_sheet
        //
        function score_sheet()   {return $this->belongsTo('App\ScoreSheet');}
}

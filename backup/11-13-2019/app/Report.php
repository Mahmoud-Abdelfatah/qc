<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;
class Report extends Model
{
    //
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table    = 'monitors';
    protected $casts    = ['agent_id','counttime_call_duration','counttime_hold_duration','sum_bce','sum_ece','sum_nce','count_bce','count_ece','count_nce'/*,'created_at','updated_by'*/];
    protected $fillable = ['globalfail','id','project_id','supervisor_id','teamleader_id','senior_id','agent_id','monitoring_type','observe_type','call_nature','call_type','customer_name','phone_number','call_duration','hold_duration','call_date','call_time','fcr','fcr_reason','customer_satisfaction','bce','ece','nce','total_score','result','created_by','created_at','updated_by','updated_at',];
    //protected $appends  = ['model_name'];


    //function getModelNameAttribute(){return 'Report';}
    function getTablColumns(){return DB::select( DB::raw('show full columns from monitors'));}
    function created_name(){ return $this->hasOne('App\User', 'id', 'created_by');}
    function updated_name(){ return $this->hasOne('App\User', 'id', 'updated_by');}
    function project(){ return $this->belongsTo('App\Project');}


    
    function Coaching(){return $this->belongsToMany('App\Coaching','coaching_monitor', 'coaching_id', 'monitor_id');}
    
    
    function test(){return $this->belongsTo('App\Test','globalfail','level');}
    
    public function score_sheet()   {return $this->belongsTo('App\ScoreSheet');}
    public function Section()       {return $this->belongsTo('App\Section');}
    public function supervisor()    {return $this->hasOne('App\Employee','id','supervisor_id');}
    public function teamleader()    {return $this->hasOne('App\Employee','id','teamleader_id');}
    public function senior()        {return $this->hasOne('App\Employee','id','senior_id');}
    public function agent()         {return $this->hasOne('App\Employee','id','agent_id');}    
    public function Sheet()         {return $this->hasMany('App\Sheet');}

}

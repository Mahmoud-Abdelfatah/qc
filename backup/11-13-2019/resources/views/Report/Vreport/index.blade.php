@extends('layouts.app')
@section('content')
<br/>
<div class="container-fluid">
	<!--Index-->
	<form action="{{URL::route('Vreport.store')}}" data-toggle="validator"  id='myForm' role="form" method="POST">
		{!! csrf_field() !!}
		<div class="col-md-8 col-md-push-2">
			<div class="panel panel-default">
				<!-- Default panel contents -->
				<div class="panel-heading">
					<div class="row">
						<div class="col-lg-6"><h5>Adherence Report</h5></div>
					</div>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-4">
					        <div class="form-group">
					            <div class='input-group date' id='startdate'>
					                <input type='text' class="form-control" name="startdate" required="required"/>
					                <span class="input-group-addon">
					                    <span class="glyphicon glyphicon-calendar"></span>
					                </span>
					            </div>
					        </div>
					    </div>
					    <div class="col-md-4">
					        <div class="form-group">
					            <div class='input-group date' id='enddate'>
					                <input type='text' class="form-control" name="enddate" required="required"/>
					                <span class="input-group-addon">
					                    <span class="glyphicon glyphicon-calendar"></span>
					                </span>
					            </div>
					        </div>
					    </div>
			        	<div class="col-md-4">
			        		<div class="form-group">
				                <select name="TypeOfReport" id="TypeOfReport" class="form-control" required="required">  <!-- onchange="DropDown('TypeOfReport','searchvalue')" -->
				                    @role('osuper | oleader | osenior | oagent')
				                	@foreach($array as $key => $value)
				                	 @if($value == 'project')
										<option id="{{$key}}">{{$value}}</option>
									 @endif	
									@endforeach
                                     @endrole

                                     @role('admin | qmanger | qsuper | qspecia | qobserver')
				                	<option></option>
				                	@foreach($array as $key => $value)
										<option id="{{$key}}">{{$value}}</option>
									@endforeach
                                    @endrole

				                </select>
					        </div>
			        	</div>
			        </div>
			        <div class="row">
			        	<div class="col-md-3">
			        		<div class="form-group">
			        			<label for="project">project</label>
				                <select name="project" id="project" class="form-control">
				                	<option></option>
				                	@foreach($projects as $value)
										<option value="{{$value->id}}">{{$value->name}}</option>
									@endforeach
				                </select>
					        </div>
			        	</div>
			        	<div class="col-md-3">
			        		<div class="form-group">
			        			<label for="super">super</label>
				                <select name="super" id="super" class="form-control">
				                	<option></option>
				                	@foreach($supers as $value)
										<option value="{{$value->id}}">{{$value->name}}</option>
									@endforeach
				                </select>
					        </div>
			        	</div>
			        	<div class="col-md-3">
			        		<div class="form-group">
			        			<label for="leader">leader</label>
				                <select name="leader" id="leader" class="form-control" >
				                	<option></option>
				                	@foreach($teamleaders as $value)
										<option value="{{$value->id}}">{{$value->name}}</option>
									@endforeach
				                </select>
					        </div>
					    </div>
			        	<div class="col-md-3">
			        		<div class="form-group">
			        			<label for="agent">agent</label>
				                <select name="agent" id="agent" class="form-control" >
				                	<option></option>
				                	@foreach($agents as $value)
										<option value="{{$value->id}}">{{$value->name}}</option>
									@endforeach
				                </select>
					        </div>
			        	</div>
					</div>
					@role('admin || qmanger || qsuper')
					<div class="row">
			        	<div class="col-md-3">
			        		<div class="form-group">
			        			<label for="observers">observers</label>
				                <select name="observers" id="observers" class="form-control">
				                	<option></option>
				                	@foreach($user as $value)
										<option value="{{$value->id}}">{{$value->name}}</option>
									@endforeach
				                </select>
					        </div>
			        	</div>
			        	<div class="col-md-3">
			        		<div class="form-group">
			        			<label for="monitoring">monitoring type</label>
				                <select name="monitoring" id="monitoring" class="form-control" >
				                	<option></option>
				                	@foreach($monitortype as $key => $value)
										<option id="{{$key}}">{{$value}}</option>
									@endforeach
				                </select>
					        </div>
			        	</div>
			        	<div class="col-md-3">
			        		<div class="form-group">
			        			<label for="observing">observing call</label>
				                <select name="observing" id="observing" class="form-control">
				                	<option></option>
				                	@foreach($observetype as $key => $value)
										<option id="{{$key}}">{{$value}}</option>
									@endforeach
				                </select>
					        </div>
					    </div>
					</div>
					@endrole
				</div>
				<div class="panel-footer text-right">
					<button type="submit" class="btn btn-primary">View Report</button>

					
				</div>
			</div>
		</div>	
	</form>
	
	<!--End Index-->
</div>

<script type="text/javascript">
    $(function () {
        $('#startdate').datetimepicker({
        	format: 'YYYY-MM-DD HH:mm:ss',
        	defaultDate: ' {{date("Y-m-d")}} 00:00:00'
        });
        $('#enddate').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            defaultDate: ' {{date("Y-m-d")}} 23:59:59',
            useCurrent: false //Important! See issue #1075
        });
        $("#startdate").on("dp.change", function (e) {
            $('#enddate').data("DateTimePicker").minDate(e.date);
        });
        $("#enddate").on("dp.change", function (e) {
            $('#startdate').data("DateTimePicker").maxDate(e.date);
        });
    });

    $('#TypeOfReport').change(function () 
	{
		var val = $(this).val();
		switch (val) 
		{
			case "":
			case "project":
			case "Consistancy":
			case "agent":
			case "Repaeted fail":
				document.getElementById("project").required = false;
			break;
			case "Analysis":
				document.getElementById("project").required = true;
			break;
			case "Non critical":
				document.getElementById("project").required = true;
			break;
		}
	});
    // function DropDown(name,appendtolist) {
    //     //var table = $('#'+name+' :selected').val();
    //     var table = $('#'+name+' :selected').attr( "id" );
    //     //remove _i from value
    //     $.ajax({
    //             url     :"{{url('AjaxDropDown?')}}",
    //             data    :{table:table,appendtolist:appendtolist},
    //             dataType:'json',
    //             type    :'get',
    //             success : function(data){
    //                 //$('.classComplaint').html(data);
    //                 //alert(appendtolist);
    //                 var $option = $('#'+appendtolist);
    //                 $option.empty();
    //                 $option.append('<option value="" selected>Select</option>');
    //                 $.each(data, function(index, value) {
                        
    //                     $option.append('<option value="'+value.id+'" id="'+value.id+'">'+value.name+'</option>');
    //                 });
    //                 $('#'+appendtolist).trigger('chosen:updated');
    //             }
    //     });
    //     return false;
    // };
</script>
@endsection







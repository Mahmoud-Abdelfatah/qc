<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
    table{
        width:100%; 
        border-collapse:collapse; 
    }
    /* provide some minimal visual accomodation for IE8 and below */
    table th{
    background:#366092;
    color: #FFFFFF;
    }

    /*  Define the background color for all the ODD background rows  */
    table td:nth-child(odd){ 
        background:#CCCCCC;
    }
    /*  Define the background color for all the EVEN background rows  */
    table td:nth-child(even){
        background:#FFFFFF;
    }
</style>
</head>

<body>
<?php
echo '<table border="1">';
echo '<theader><tr>';
echo '<th rowspan="3">projects</th>';
        echo '<th rowspan="3">Agent</th>';
        echo '<th rowspan="3"># of Std Call</th>';
        echo '<th rowspan="3">Total Calls Duration</th>';
        echo '<th rowspan="3">Total Hold Duration</th>';
        echo '<th rowspan="3"># of BCE</th>';
        echo '<th rowspan="3"># of ECE</th>';
        echo '<th rowspan="3"># of NCE</th>';
        echo '<th rowspan="3">Score Of agentsr</th>';
        echo '<th rowspan="3">Accuracy of BCE</th>';
        echo '<th rowspan="3">Accuracy of ECE</th>';
        echo '<th rowspan="3">% of HPD</th>';
        echo '<th rowspan="3">Accuracy of NCE</th>';
         echo '</tr><tr>';
         echo '</tr><tr>';
        echo '</tr></theader>';  

        echo '<br>'; 
        echo '<tboody><tr>';

        foreach ($data  as $key => $value) {
            echo '<td>'.$value->Project.'</td>';
            echo '<td>'.$value->Agent.'</td>';
            echo '<td>'.$value->stdcalls.'</td>';
            echo '<td>'.$value->TotalCallsDuration.'</td>';
            echo '<td>'.$value->TotalHoldDuration.'</td>';
            echo '<td>'.$value->ofBCE.'</td>';
            echo '<td>'.$value->ofECE.'</td>';
            echo '<td>'.$value->ofNCE.'</td>';
            echo '<td>'.$value->ScoreOfagents.'</td>';
            echo '<td>'.$value->AccuracyofBCE.'</td>';
            echo '<td>'.$value->AccuracyofECE.'</td>';
            echo '<td>'.$value->ofHPD.'</td>';
            echo '<td>'.$value->AccuracyofNCE.'</td>';


                
         echo "</tr></tboody>";
        }

       

echo '</table>';
header('Content-type: application/vnd.ms-excel');
//header("Content-Disposition: attachment; filename='Analysis.xls'");
//$objWriter->save('php://output'); 
?>
</body>
</html>
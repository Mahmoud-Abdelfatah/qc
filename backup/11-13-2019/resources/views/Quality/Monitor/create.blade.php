@extends('layouts.app')
@section('content')
	@php 
        $project_id     = \App\Project::lists('name','id')->toArray(); 
        $projects   =  \App\Project::all();
        $manager_id     = array(); 
        $supervisor_id  = array();
        $teamleader_id  = array(); 
        $senior_id      = array();  
        $agent_id       = array(); 
        
        $mangers     =  \App\Manager::all();
    $supers     =  \App\Supervisor::all();
    $teamleaders=  \App\Teamleader::all();
    $seniors    =   \App\Senior::all();
    $agents     =  \App\Agent::all();

     
    @endphp    

  <?php  
global $userproj,$arraymg,$arraysu,$arrayld,$arrays,$arraya;

       if($user_role == '["Admin"]')
       {
         $userproj = array();
         foreach($projects as $project)
         {
             $userproj[] = $project;
         }
           $arraymg = array();
         foreach($mangers as $manger)
         {
            $arraymg[] = $manger;
         }

            $arraysu = array();
         foreach($supers as $suuper)
         {
            $arraysu[] = $suuper;
         }

             $arrayld = array();
         foreach($teamleaders as $leader)
         {
            $arrayld[] = $leader;
         }
              
                $arrays = array();
         foreach($seniors as $senior)
         {
            $arrays[] = $senior;
         }   

               $arraya = array();
         foreach($agents as $agent)
         {
            $arraya[] = $agent;
         }   

       }
    

        if($user_role == '["Quality Manger"]')
       {
         $arraymg = array();
         foreach ($manger as $mangaer) {
            $arraymg[] = $mangaer;
         }

         $arraysu = array();
         foreach ($manger as $manager) {
            foreach ($manager->Supervisor as $super){
                 $arraysu[] = $super;
            }
            }

          $arrayld =array();
          foreach ($manger as $manager) {
            foreach ($manager->Supervisor as $super){
                foreach ($super->Teamleader as $leader){
                   $arrayld[] = $leader;
                }
                
            }  
            }

            $arrays =array();
             foreach ($manger as $manager) {
                foreach ($manager->Supervisor as $Super){
                    foreach ($Super->Teamleader as $leader){
                        foreach ($leader->Senior as $senior){

                            $arrays[] =  $senior;
                        }

                    }
                 
             }
                }


                $arraya =array();
                foreach ($manger as $manager) {
                    foreach ($manager->Supervisor as $super){
                        foreach ($super->Teamleader as $leader){
                            foreach ($leader->Senior as $senior){
                                foreach ($senior->Agent as $agent){
                                    $arraya[] = $agent;
                                }

                            }


                        }

                    }
                    
                }


        }


     if($user_role == '["Quality Supervisor"]')
       {

        
         $userproj = array();
         foreach($supervisor as $super){
           foreach($super->Manager as $manger){
                foreach($manger->Project as $project)
         {
             $userproj[] = $project;
         }}}

        
           // print_r($userproj);
         $arraymg = array();
    foreach($supervisor as $super){
        foreach($super->Manager as $manger){
           $arraymg[] = $manger;

       }}
      //  print_r($array);
       $arraysu = array();
      foreach($supervisor as $super)
                {
                  $arraysu[]= $super;
                }


         $arrayld = array();
        foreach($supervisor as $super){
            foreach($super->teamleader as $leader)
                {

                  $arrayld[]= $leader;
                }}        
        
         $arrays = array();
         foreach($supervisor as $super){
            foreach($super->teamleader as $leader){
                foreach($leader->Senior as $senior)
                {

                  $arrays[]= $senior;
                }}} 


             $arraya = array();
            foreach($supervisor as $super){
            foreach($super->teamleader as $leader){
                foreach($leader->Senior as $senior){
                    foreach ($senior->Agent as $agent) {
                       
                $arraya[]= $agent;

                  
                }}}} 


            }     


            if($user_role == '["Operation Team Leaders"]')
             {
                
                    $arraymg = array();
                  foreach ($teamleader as $leader) {  
                  foreach($leader->Supervisor as $super){
                    foreach($super->Manager as $manger){
                              $arraymg[] = $manger;
                          
                    }}}

                   $arraysu = array();
                foreach ($teamleader as $leader) { 
                 foreach($leader->Supervisor as $super){
                     
                            $arraysu[] = $super;
                    }}

                         $arrayld = array();
                foreach ($teamleader as $leader) { 
                 
                            $arrayld[] = $leader;
                    }
                        $arrays = array();
                 foreach ($teamleader as $leader) { 
                    foreach ($leader->Senior as $senior) {
                 
                            $arrays[] = $senior;
                    } }

                  $arraya = array();
                 foreach ($teamleader as $leader) { 
                    foreach ($leader->Senior as $senior) {
                        foreach ($senior->Agent as $agent) {
                 
                            $arraya[] = $agent;
                    } }}

            }

       ?>


	<br/>

    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">You are working Score Sheet and the Time {{date('Y-M-d h:i:s')}}</div>
                <div class="panel-body">
	    		@include('layouts.form')
				<div id="well" class="col-md-6 ">
                    <div class="form-group">
                    	<div id="well" class="well">
                    		<!-- <i class="text-pramiray">Call Summary</i> -->
                    		<!-- <hr> -->
                    		<div class="row text-center">
                    			<div class="col-md-2">#bce</div>
                    			<div class="col-md-2">#ece</div>
                    			<div class="col-md-2">#nce</div>
                    			<div class="col-md-3">Total Score</div>
                    			<div class="col-md-3">Result</div>
                    		</div>
                    		<div class="row text-center">
                    			<div class="col-md-2" id="lbce">0</div>
                    			<div class="col-md-2" id="lece">0</div>
                    			<div class="col-md-2" id="lnce">0</div>
                    			<div class="col-md-3" id="ltotal">0</div>
                    			<div class="col-md-3" id="lresult">0</div>
                    		</div>
                    	</div>
		    			</div>
                    </div>    
                </div>
	    	</div>
	</div>
   	<div class="col-md-12" id="sheet">
		<div class="ScoreSheet" id="ScoreSheet">
		</div>
    </div>




<script type="text/javascript">
    $('.text-primary').remove();
    $( "#sheet" ).insertAfter( $( "#footer" ) );
    $( "#well" ).insertAfter( $( "#formbody .customer_satisfaction" ) );

	$(document).on('change','#fcr',function(){                
        var FCR        = $('#fcr :selected').val();
        //alert(FCR);
        if (FCR == 'Done') {
            $("#fcr_reason").prop('required',false);
            $("#fcr_reason").prop('disabled',true);
        }else{
            $("#fcr_reason").prop('required',true);
            $("#fcr_reason").prop('disabled',false);
        }
    });
    
    $(function () {
        $('#call_duration').datetimepicker({format: 'HH:mm:ss',});
        $('#hold_duration').datetimepicker({format: 'HH:mm:ss',});
        $('#call_date').datetimepicker({format: 'YYYY-MM-DD',defaultDate: ' {{date("Y-m-d")}}',useCurrent: false});
        $('#call_time').datetimepicker({format: 'hh:mm',defaultDate: ' {{date("Y-m-d")}} 00:00:00',});
    });

    $("#project_id").attr('onchange','relationlist("project_id","Project","Manager","manager_id")');
    $("#manager_id").attr('onchange','relationlist("manager_id","Manager","Supervisor","supervisor_id")');
    $("#supervisor_id").attr('onchange','relationlist("supervisor_id","Supervisor","Teamleader","teamleader_id")');
    
    $("#teamleader_id").attr('onchange','relationlist("teamleader_id","Teamleader","Senior","senior_id")');
    
    $("#senior_id").attr('onchange','relationlist("senior_id","Senior","Agent","agent_id")');
    $('#agent_id').attr('onchange','AjaxScoreSheet("project_id","id","projects")');
    function AjaxScoreSheet(name,table_id,table) {
        var id 		= $('#'+name+' :selected').attr( "value" );
    	$.ajax({
                url     :"{{url('AjaxScoreSheet?')}}",
                data    :{id:id,'table_id':table_id,table:table},
                dataType:'json',
                type    :'get',
                success : function(data){
                	$('.ScoreSheet').html(data);
                }
        });
        return false;
	};
    function relationlist(thislist,model,method,appendtolist) {
        var id = $('#'+thislist+' :selected').attr( "value" );
        $.ajax({
            url     :"{{url('Ajaxrelationlist?')}}",
            data    :{id:id,model:model,method:method},
            dataType:'json',
            type    :'get',
            success : function(data){
                var $option = $('#'+appendtolist);
                $option.empty();
                $option.append('<option value="" selected>Select</option>');
                if(appendtolist == 'manager_id'){
                   
                   
                    var user = <?php echo json_encode($arraymg, JSON_PRETTY_PRINT) ?>;
                $.each(user, function(index, value) {  
                 //  if(value.name == user.name)                      
                    $option.append('<option value="'+value["id"]+'"id="'+value.id+'">'+value.name+'</option>');
                });}

                if(appendtolist == 'supervisor_id'){
                      var user = <?php echo json_encode($arraysu, JSON_PRETTY_PRINT) ?>;
                $.each(user, function(index, value) { 
                    // if(value.name == user)                      
                    $option.append('<option value="'+value["id"]+'"id="'+value.id+'">'+value.name+'</option>');
                });}

               if(appendtolist == 'teamleader_id'){
                 var user = <?php echo json_encode($arrayld, JSON_PRETTY_PRINT) ?>;
                $.each(user, function(index, value) { 
                   // if(value.name == user)                       
                    $option.append('<option value="'+value["id"]+'"id="'+value.id+'">'+value.name+'</option>');
                });}

               if(appendtolist == 'senior_id'){
                var user = <?php echo json_encode($arrays, JSON_PRETTY_PRINT) ?>;
                $.each(user, function(index, value) { 
                  // if(value.name == user)                       
                    $option.append('<option value="'+value["id"]+'"id="'+value.id+'">'+value.name+'</option>');
                });}

                if(appendtolist == 'agent_id'){
                   var user = <?php echo json_encode($arraya, JSON_PRETTY_PRINT) ?>;
                $.each(user, function(index, value) {  
                   // if(value.name == user)                      
                    $option.append('<option value="'+value["id"]+'"id="'+value.id+'">'+value.name+'</option>');
                });}

                $('#'+appendtolist).trigger('chosen:updated');
            }
        });
        return false;
    };
</script>
@endsection

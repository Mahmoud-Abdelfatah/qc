@extends('layouts.app')
@section('content')
    @php 
        $project_id     = \App\Project::lists('name','id')->toArray(); 
        $manager_id     = array(); 
        $supervisor_id  = array();
        $teamleader_id  = array(); 
        $senior_id      = array();  
        $agent_id       = array(); 
    @endphp 
    <br/>
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">You are working on</div>
                <div class="panel-body">
                @include('layouts.form')
                <div id="well" class="col-md-6 ">
                    <div class="form-group">
                        <div id="well" class="well">
                            <!-- <i class="text-pramiray">Call Summary</i> -->
                            <!-- <hr> -->
                            <div class="row text-center">
                                <div class="col-md-2">#bce</div>
                                <div class="col-md-2">#ece</div>
                                <div class="col-md-2">#nce</div>
                                <div class="col-md-3">Total Score</div>
                                <div class="col-md-3">Result</div>
                            </div>
                            <div class="row text-center">
                                <div class="col-md-2" id="lbce">0</div>
                                <div class="col-md-2" id="lece">0</div>
                                <div class="col-md-2" id="lnce">0</div>
                                <div class="col-md-3" id="ltotal">0</div>
                                <div class="col-md-3" id="lresult">0</div>
                            </div>
                        </div>
                        </div>
                    </div>    
                </div>
            </div>
    </div>
    <div class="col-md-12" id="sheet">
        <div class="ScoreSheet" id="ScoreSheet">
        </div>
    </div>




<script type="text/javascript">
    $('.text-primary').remove();
    $( "#sheet" ).insertAfter( $( "#footer" ) );
    $( "#well" ).insertAfter( $( "#formbody .customer_satisfaction" ) );

    $(document).on('change','#fcr',function(){                
        var FCR        = $('#fcr :selected').val();
        //alert(FCR);
        if (FCR == 'Done') {
            $("#fcr_reason").prop('required',false);
            $("#fcr_reason").prop('disabled',true);
        }else{
            $("#fcr_reason").prop('required',true);
            $("#fcr_reason").prop('disabled',false);
        }
    });
    $(function () {
        $('#call_duration').datetimepicker({format: 'HH:mm',});
        $('#hold_duration').datetimepicker({format: 'HH:mm',});
        $('#call_date').datetimepicker({format: 'YYYY-MM-DD',defaultDate: ' {{date("Y-m-d")}}',useCurrent: false});
        $('#call_time').datetimepicker({format: 'hh:mm',defaultDate: ' {{date("Y-m-d")}} 00:00:00',});
    });


 $("#project_id").attr('onchange','relationlist("project_id","Project","Manager","manager_id")');
    $("#manager_id").attr('onchange','relationlist("manager_id","Manager","Supervisor","supervisor_id")');
    $("#supervisor_id").attr('onchange','relationlist("supervisor_id","Supervisor","Teamleader","teamleader_id")');
    
    $("#teamleader_id").attr('onchange','relationlist("teamleader_id","Teamleader","Senior","senior_id")');
    
    $("#senior_id").attr('onchange','relationlist("senior_id","Senior","Agent","agent_id")');
    $('#agent_id').attr('onchange','AjaxScoreSheet("project_id","id","projects")');

    // $('#project_id').attr('onchange','AjaxVildation("employees","project_id","project_id","title_id","2","supervisor_id")');
    // $('#supervisor_id').attr('onchange','AjaxVildation("employees","project_id","project_id","manager_id","supervisor_id","teamleader_id")');
    // $('#teamleader_id').attr('onchange','AjaxVildation("employees","project_id","project_id","manager_id","teamleader_id","senior_id")');
    // $('#senior_id').attr('onchange','AjaxVildation("employees","project_id","project_id","manager_id","senior_id","agent_id")');
    // $('#agent_id').attr('onchange','AjaxScoreSheet("project_id","id","projects")');
    function AjaxScoreSheet(name,table_id,table) {
        var id      = $('#'+name+' :selected').attr( "value" );
        $.ajax({
                url     :"{{url('AjaxScoreSheet?')}}",
                data    :{id:id,'table_id':table_id,table:table},
                dataType:'json',
                type    :'get',
                success : function(data){
                    $('.ScoreSheet').html(data);
                }
        });
        return false;
    };
    // function AjaxVildation(table,col1,search1,col2,search2,appendtolist) {
    //     var search1         = $('#'+search1+' :selected').attr( "value" );
    //     if(search2 == 2 ){
    //     }else{
    //         var search2         = $('#'+search2+' :selected').attr( "value" );
    //     }
    //     $.ajax({
    //             url     :"{{url('AjaxVildation?')}}",
    //             data    :{table:table,col1:col1,'search1':search1,col2:col2,'search2':search2},
    //             dataType:'json',
    //             type    :'get',
    //             success : function(data){
    //                 var $option = $('#'+appendtolist);
    //                 $option.empty();
    //                 $option.append('<option value="" selected>Select</option>');
    //                 $.each(data, function(index, value) {
                        
    //                     $option.append('<option value="'+value.id+'">'+value.name+'</option>');
    //                 });
    //                 $('#'+appendtolist).trigger('chosen:updated');
    //             }
    //     });
    //     return false;
    // };

function relationlist(thislist,model,method,appendtolist) {
        var id = $('#'+thislist+' :selected').attr( "value" );
        $.ajax({
            url     :"{{url('Ajaxrelationlist?')}}",
            data    :{id:id,model:model,method:method},
            dataType:'json',
            type    :'get',
            success : function(data){
                var $option = $('#'+appendtolist);
                $option.empty();
                $option.append('<option value="" selected>Select</option>');
                $.each(data, function(index, value) {                        
                    $option.append('<option value="'+value["id"]+'"id="'+value.id+'">'+value.name+'</option>');
                });
                $('#'+appendtolist).trigger('chosen:updated');
            }
        });
        return false;
    };

</script>
@endsection

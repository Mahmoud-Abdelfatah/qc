@extends('layouts.app')
@section('content')

<style type="text/css">
 	.table {
        max-width: none;
        table-layout: fixed;
        /*word-wrap: break-word;*/
    }
    .table td{
       font-size: 11px;
       font-weight: bold;
    }
    .table th{
       font-size: 9px;
    }
</style>

<div class="container-fluid ">
	
	<!--Index-->
	<input type="hidden" id="key" 	 	 value="id">
	<input type="hidden" id="model"  	 value="Additional">
	<input type="hidden" id="groupby" 	 value="id">
	<input type="hidden" id="path" 	 	 value="Quality">
	<input type="hidden" id="conditions" value='{"status":"open"}' name="conditions">
	
 	@include('layouts.table')
	<!--End Index-->
</div>
<script type="text/javascript">
	$('.col-int').remove();
	//$('.destroy').remove();
	//$('.row .col-lg-1 a').remove();
</script>
@endsection

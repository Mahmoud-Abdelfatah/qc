<div class="container-fluid">
    @if (strpos(Request::route()->getName(), '.create') !== false)
            <h2 class="text-primary">Add {{$myModal->model_name}}</h2>
        @elseif(strpos(Request::route()->getName(), '.edit') !== false)
            <h2 class="text-primary">{{$myModal->model_name}} Details</h2>
    @elseif(strpos(Request::route()->getName(), '.show') !== false)
            <h2 class="text-primary">{{$myModal->model_name}} Details</h2>
        @else
    @endif
    <!-- display errors of validation -->
    @if ($errors->any())
      <div class="alert alert-danger">
          @foreach ($errors->all() as $error)
              {{ $error }}<br>        
          @endforeach
      </div>
    @endif

    @if (strpos(Request::route()->getName(), '.create') !== false)
       {!! Form::model($myModal,['data-toggle'=>'validator','id'=>'myForm','role'=>'form','method'=>'POST','route'=>str_replace(".create",".store",Request::route()->getName())])!!}
    @elseif(strpos(Request::route()->getName(), '.edit') !== false)
        {!! Form::model($EditData,['data-toggle'=>'validator','id'=>'myForm','role'=>'form','method'=>'PUT','route'=>  array(str_replace(".edit",".update",Request::route()->getName()),$EditData->id)])!!}
@elseif(strpos(Request::route()->getName(), '.show') !== false)
        {!! Form::model($ShowData,['data-toggle'=>'validator','id'=>'myForm','role'=>'form','method'=>'PUT','route'=>  array(str_replace(".show",".update",Request::route()->getName()),$ShowData->id)])!!}
    @else
    @endif
            <div id="formbody">

                @foreach($myModal->getTablColumns() as $key => $value)
                    @if     ($value->Comment    == 'disable')
                    @elseif ($value->Comment    == 'hidden')
                        <!--  -->
                        {!! Form::hidden($value->Field, null , ['class' => 'form-control','id' => $value->Field,'required' => 'required']) !!}
                    @elseif ($value->Comment    == 'relationship')
                        <div class="{{$value->Field}} col-md-3">
                            <div class="form-group">
                                <label for="{{$value->Field}}">{{ trans('form.'.$value->Field) }}</label> 


                         @role('Sadmin')          
                
                                <div class="check_{{$value->Field}}">
                                @if($value->Null == 'YES')
                                    {!! Form::select($value->Field,[null=>'Please Select'] + eval('return $'. $value->Field . ';'),null,['class' => 'form-control ','id' => $value->Field])!!}
                                @else
                                    {!! Form::select($value->Field,[null=>'Please Select'] + eval('return $'. $value->Field . ';'),null,['class' => 'form-control ','id' => $value->Field,'required' =>'required'])!!}
                                @endif
                                </div>
                        @endrole

                        
                         @role('admin || qmanger || qsuperqobserver') 
                            @if($value->Field == 'score_sheet_id')  

                                 <div class="check_{{$value->Field}}">
                                
                                    {!! Form::select($value->Field,[null=>'Please Select'] + eval('return $'. $value->Field . ';'),null,['class' => 'form-control ','id' => $value->Field,'required' =>'required','disabled' => 'disabled'])!!}
                          
                                </div>

                            @else                        
                                <div class="check_{{$value->Field}}">
                                @if($value->Null == 'YES')
                                    {!! Form::select($value->Field,[null=>'Please Select'] + eval('return $'. $value->Field . ';'),null,['class' => 'form-control ','id' => $value->Field])!!}
                                @else
                                    {!! Form::select($value->Field,[null=>'Please Select'] + eval('return $'. $value->Field . ';'),null,['class' => 'form-control ','id' => $value->Field,'required' =>'required'])!!}
                                @endif
                                </div>
                            @endif
                        @endrole 

                            </div>
                        </div>                    
                    @elseif ($value->Comment    == 'list')
                        <div class="{{$value->Field}} col-md-3">
                            <div class="form-group">
                                <label for="{{$value->Field}}">{{ trans('form.'.$value->Field) }}</label>                              
                                <div class="check_{{$value->Field}}">
                                    @if($value->Null == 'YES')
                                        {!! Form::select(''.$value->Field.'[]',eval('return $'. $value->Field . ';'),null,['class' => 'form-control chosen-select','id' => $value->Field,'multiple'=>'multiple'])!!}
                                    @else
                                        {!! Form::select(''.$value->Field.'[]',eval('return $'. $value->Field . ';'),null,['class' => 'form-control chosen-select','id' => $value->Field,'multiple'=>'multiple','required' =>'required'])!!}
                                    @endif
                                </div>
                            </div>
                        </div>                    
                    @elseif ($value->Comment    == 'file')
                        <div class="{{$value->Field}} col-md-3">
                            <div class="form-group">
                                <label for="{{$value->Field}}">{{ trans('form.'.$value->Field) }}</label>
                                <div class="check_{{$value->Field}}">
                                    @if($value->Null == 'YES')
                                        {!! Form::file($value->Field , ['class' => 'form-control','id' => $value->Field]) !!}
                                    @else
                                        {!! Form::file($value->Field , ['class' => 'form-control','id' => $value->Field,'required' => 'required']) !!}
                                    @endif
                                </div>
                            </div>
                        </div>  
                    @elseif ($value->Comment    =='password')
                        <div class="{{$value->Field}} col-md-3">
                            <div class="form-group">
                                <label for="{{$value->Field}}">{{ trans('form.'.$value->Field) }}</label>
                                <div class="check_{{$value->Field}}">
                                @if($value->Null == 'YES')
                                    {!! Form::password($value->Field, ['class' => 'form-control','id' => $value->Field]) !!}
                                @else
                                    {!! Form::password($value->Field, ['class' => 'form-control','id' => $value->Field,'required' => 'required']) !!}
                                @endif
                                </div>
                            </div>
                        </div>
                    @elseif ($value->Type       == 'date')
                        <div class="{{$value->Field}} col-md-3">
                            <div class="form-group">
                                <label for="{{$value->Field}}">{{ trans('form.'.$value->Field) }} </label>
                                <div class='check_{{$value->Field}} input-group' id='{{$value->Field}}'>
                                    @if($value->Null == 'YES')
                                        {!! Form::text($value->Field, null , ['class' => 'form-control','id' => 'date-'.$value->Field]) !!}
                                    @else
                                        {!! Form::text($value->Field, null , ['class' => 'form-control','id' => 'date-'.$value->Field,'required' => 'required']) !!}
                                    @endif
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            <script type="text/javascript">$(function () {$('#{{$value->Field}}').datetimepicker({format: 'YYYY-MM-DD',viewMode: 'years'});});</script>
                        </div>
                    @elseif ($value->Type       == 'text')
                        <div class="{{$value->Field}} col-md-3">
                            <div class="form-group">
                                <label for="{{$value->Field}}">{{ trans('form.'.$value->Field) }}</label>
                                <div class="check_{{$value->Field}}">
                                @if($value->Null == 'YES')
                                    {{ Form::textarea($value->Field, null, ['class' => 'form-control','id' => $value->Field,'size' => '30x5']) }}
                                @else                                    
                                    {{ Form::textarea($value->Field, null, ['class' => 'form-control','id' => $value->Field,'size' => '30x5','required' =>'required']) }}
                                @endif
                                </div>
                            </div>
                        </div> 
                    @elseif (strpos($value->Type, 'enum') !== false)
                        <?php
                        $remove = str_replace("enum(", "",$value->Type );
                        $remove = str_replace(")", "",$remove );
                        $remove = str_replace("'", "",$remove );
                        //$array  = explode(",",$remove);
                        $array = explode(',', $remove);
                        $array = array_combine($array, $array);
                        ?>
                        <div class="{{$value->Field}} col-md-3">
                            <div class="form-group">
                                <label for="{{$value->Field}}">{{ trans('form.'.$value->Field) }}</label>                              
                                <div class="check_{{$value->Field}}">
                                    @if($value->Null == 'YES')
                                        {!! Form::select($value->Field,[null=>'Please Select'] + $array,null,['class' => 'form-control ','id' => $value->Field])!!}
                                    @else
                                        {!! Form::select($value->Field,[null=>'Please Select'] + $array,null,['class' => 'form-control ','id' => $value->Field,'required' =>'required'])!!}
                                    @endif
                                </div>
                            </div>
                        </div>
                    @elseif (strpos($value->Type, 'int' ) !== false  || strpos($value->Type, 'decimal' ) !== false || strpos($value->Type, 'bigint' ) !== false)
                       <div class="{{$value->Field}} col-md-3">
                            <div class="form-group">
                                <label for="{{$value->Field}}">{{ trans('form.'.$value->Field) }}</label>
                                <div class="check_{{$value->Field}}">
                                    @if($value->Null == 'YES')
                                        {!! Form::text($value->Field, null , ['class' => 'number form-control','id' => $value->Field]) !!}
                                    @else
                                        {!! Form::text($value->Field, null , ['class' => 'number form-control','id' => $value->Field,'required' => 'required']) !!}
                                    @endif
                                </div>
                            </div>
                        </div>                                       
                    @elseif (1==1)
                        <div class="{{$value->Field}} col-md-3">
                            <div class="form-group">
                                <label for="{{$value->Field}}">{{ trans('form.'.$value->Field) }}</label>
                                <div class="check_{{$value->Field}}">
                                    @if($value->Null == 'YES')
                                        {!! Form::text($value->Field, null , ['class' => 'form-control','id' => $value->Field]) !!}
                                    @else
                                        {!! Form::text($value->Field, null , ['class' => 'form-control','id' => $value->Field,'required' => 'required']) !!}
                                    @endif

                                </div>
                            </div>
                        </div> 
                    @endif
                @endforeach  
            </div>
            <div class="col-md-12 text-right" id="footer">
                @if (strpos(Request::route()->getName(), '.create') !== false || strpos(Request::route()->getName(), '.edit') !== false)
                <button type="submit" class="btn btn-primary">{{ucfirst(substr(Request::route()->getName(), strpos(Request::route()->getName(),'.')+strlen('.')))}}</button>
                @endif
            </div>

</div>
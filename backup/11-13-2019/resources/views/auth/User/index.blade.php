@extends('layouts.app')
@permission('user.show')
@endpermission
	@section('content')
<a id="upload" type="button" data-route="{{URL::route('User.upload')}}" class="Show  btn btn-primary"><i class="fa fa-upload"></i></a>
		<div class="container-fluid ">
			<!--Index-->
			<input type="hidden" id="key" 	 	 value="id">
			<input type="hidden" id="model"  	 value="User">
			<input type="hidden" id="groupby" 	 value="id">
			<input type="hidden" id="path" 	 	 value="auth">
			<input type="hidden" id="conditions" value='{}' name="conditions"  >
		 	@include('layouts.table')
			<!--End Index-->
		</div>
		<script type="text/javascript">
			$('.col-int').remove();	
			$('#upload').insertBefore('.addbtn');
		</script>
	@endsection

@extends('layouts.app')
@section('content')
<style type="text/css">
    body { 
        padding-top: 0; !important;
    }   
</style>
<form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
    {!! csrf_field() !!}
    <table border="0px" cellspacing="0px" cellpadding="0px" height="100%" width="100%">
        <tbody>
            <tr>
                <td class="cssTopNavigation" height="100%" style="background: url('{{asset("public/img/login.jpg")}}') no-repeat left top; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;"></td>                
                <td style="width: 50px; vertical-align: top"></td>
                <td style="width: 400px; vertical-align: top">
                    <table border="0px" cellspacing="0px" cellpadding="0px" width="100%" align="center">
                        <tbody>
                        <tr height="50px">
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <table border="0px" cellspacing="0px" cellpadding="0px" width="100%" align="center">
                                    <tbody>
                                        <tr>
                                            <td class="cssLoginLogos" align="center"><img class="cssImageMainLogo" src="{{asset('public/img/logo.png')}}"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr height="30px">
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="cssLoginMainTitle">
                                <center><h3><span id="lblMainTitle" class="cssLabelLoginMainTitle">Quality Managements System</span></h3></center>
                            </td>
                        </tr>
                        <tr height="30px">
                            <td>&nbsp;</td>
                        </tr>
                        <tr height="30px">
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <table border="0px" cellspacing="0px" cellpadding="0px" width="100%" align="center">
                                    <tbody><tr>
                                        <td>
                                            <span id="lblUsername">Username</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                                        </td>
                                    </tr>
                                    <tr height="10px">
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span id="lblPassword">Password</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="password" class="form-control" name="password">
                                        </td>
                                    </tr>
                                    <tr height="20px">
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <button type="submit" class="btn btn-primary">Login</button>
                                        </td>
                                    </tr>
                                    <tr height="139px">
                                        <td>
                                            <span id="RequiredFieldValidator1" style="color:Red;">
                                            @if ($errors->any())
                                              @foreach ($errors->all() as $error)
                                                  {{ $error }}<br>        
                                              @endforeach
                                            @endif
                                            </span>
                                        </td>
                                    </tr>
                                </tbody></table>
                            </td>
                        </tr>
                    </tbody></table>

                </td>
                <td style="width: 50px; vertical-align: top"></td>
            </tr>
        </tbody>
        </table>

</form>
@endsection

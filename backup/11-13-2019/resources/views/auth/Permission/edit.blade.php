@extends('layouts.app')
	@section('content')
		
	    @include('layouts.form')
	   	
		@php $array = array(); @endphp
		@foreach (Route::getRoutes() as $value)
		        @php array_push($array,substr($value->getName(), 0, strpos($value->getName(), '.'))); @endphp
		@endforeach
<script type="text/javascript">
	$('#name').attr('onchange','Slug()');
	$('#model').attr('onchange','Slug()');
	function Slug(){
		var name  = $('#name :selected').text();
		var model = $('#model :selected').text();
		$('#slug').val(model+'.'+name);
	}
	$('#model').append('@foreach(array_unique($array) as $value) <option>{{$value}}</option>@endforeach')
</script>
@endsection
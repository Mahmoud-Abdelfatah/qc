@extends('layouts.app')
	@section('content')
		<!-- inject permissions object to this view -->
		@inject('permissions', 'Bican\Roles\Models\Permission')
		<!-- listing permissions array for select element -->
		@php $permissions = $permissions->lists('slug','id')->toArray(); @endphp
	    @include('layouts.form')
	   	<script type='text/javascript'>
			$('#formbody').append('<div class="col-md-3"><div class="form-group"><label for="role">{{ trans("form.permissions") }}</label><div class="permissions">{{ Form::select('permissions_list[]',$permissions,null,['class' => 'form-control chosen-select','id' => 'permission','required' =>'required','multiple' =>'multiple'])}}</div></div></div>');
		</script>
@endsection

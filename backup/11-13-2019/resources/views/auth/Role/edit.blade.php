@extends('layouts.app')
@section('content')
	<!-- inject permissions object to this view -->
	@inject('permissions', 'Bican\Roles\Models\Permission')
	<!-- listing permissions array for select element -->
	@php $permissions = $permissions->lists('slug','id')->toArray(); @endphp
    
    @include('layouts.form')
    <script type='text/javascript'>
		$('#formbody').append('<div class="col-md-3"><div class="form-group"><label for="permissions">{{ trans("form.Permissions_List") }}</label><div class="permission">{{ Form::select('permissions_list[]',$permissions,$EditData->Permissions_List,['class' => 'form-control chosen-select','id' => 'permissions','multiple' =>'multiple'])}}</div></div></div>');
	</script>
@endsection

@extends('layouts.app')
@section('content')
	@php 
		$location_id 		= App\Location::lists('name','id')->toArray();
		$score_sheet_id 	= App\ScoreSheet::lists('name','id')->toArray(); 
	@endphp
    @include('layouts.form')
@endsection

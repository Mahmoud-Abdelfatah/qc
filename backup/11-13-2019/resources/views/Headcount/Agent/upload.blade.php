<div class="col-sm-10 no-float">
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="{{URL::route('Agent.uploaded')}}" method="post" enctype="multipart/form-data" name="form1" id="form1"> 
        {!! csrf_field() !!}
        <div class="row">
            <div class="col-md-4">     
                <div class="form-group">
                    <input id="file" type="file" class="file" name="file"  >
                    <div id="errorBlock" class="help-block"></div>
                </div>
            </div>
            <div class="col-md-4">     
                <input type="submit" name="Submit" value="Submit" class="btn btn-primary" /> 
            </div>
        </div>
    </form>
</div>
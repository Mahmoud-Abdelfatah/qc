

function pageid(){
    //Get Table information
    var rows        = $('#rows :selected').text();
    var model       = $('#model').val();
    var search      = $('#search').val();
    var orderby     = $('#orderby').val();
    var ordertype   = $('#ordertype').val();
    var groupby     = $('#groupby').val();
    var key         = $('#key').val();

    if ($('.pagination > .active span').text() != null){var page= $('.pagination > .active span').text();}else {var page = null;}
    if ($('#conditions') != null){var conditions= JSON.parse($('#conditions').val());}else {var conditions = null;}
    //filtratoin
    var columns = new Object();
    var div     = document.getElementById("tfooter");
   
    var selects = div.getElementsByTagName("select");
    for ( var i = 0; i < selects.length; i++ ) {
        var name    = $(selects[i]).attr("id");
        var value   = $("#"+name).find(":selected").val();
        if (value.length > 0) {columns[name] = value;}
    }
    
    var inputs  = div.getElementsByTagName("input");
    for ( var i = 0; i < inputs.length; i++ ) {
        var name    = $(inputs[i]).attr("id");
        var value   = $(inputs[i]).val();
        if (value.length > 0) {columns[name] = value;}
    }
    // Page loading Icon
    $(".table").html("<center><img src='{{asset('img/ajax-loader.gif')}}'></center>");
    //Go to Ajax Controller
    $.ajax({
        url     :"{{url('Ajaxpageid?')}}",
        data    :{key:key,columns:columns,groupby:groupby,conditions:conditions,model:model,page:page,search:search,ordertype:ordertype,orderby:orderby,rows:rows},
        dataType:'json',
        type    :'get',
        success : function(data){$('.classPageID').html(data);}
    });
    return false;
}

$(document).on('click','.pagination a',function(e){
    e.preventDefault();
    var page        = $(this).attr('href').split('page=')[1];
    //Get Table information
    var rows        = $('#rows :selected').text();
    var model       = $('#model').val();
    var search      = $('#search').val();
    var orderby     = $('#orderby').val();
    var ordertype   = $('#ordertype').val();
    var groupby     = $('#groupby').val();
    var key         = $('#key').val();

    if ($('#conditions') != null){var conditions= JSON.parse($('#conditions').val());}else {var conditions = null;}
     //filtratoin
    var columns = new Object();
    var div     = document.getElementById("tfooter");
   
    var selects = div.getElementsByTagName("select");
    for ( var i = 0; i < selects.length; i++ ) {
        var name    = $(selects[i]).attr("id");
        var value   = $("#"+name).find(":selected").val();
        if (value.length > 0) {columns[name] = value;}
    }
  
    var inputs  = div.getElementsByTagName("input");
    for ( var i = 0; i < inputs.length; i++ ) {
        var name    = $(inputs[i]).attr("id");
        var value   = $(inputs[i]).val();
        if (value.length > 0) {columns[name] = value;}
    }
    // Page loading Icon
    $(".table").html("<center><img src='{{asset('img/ajax-loader.gif')}}'></center>");
    //Go to Ajax Controller
    $.ajax({
        url     :"{{url('Ajaxpageid?')}}",
        data    :{key:key,columns:columns,groupby:groupby,conditions:conditions,model:model,page:page,search:search,ordertype:ordertype,orderby:orderby,rows:rows},
        dataType:'json',
        type    :'get',
        success : function(data){$('.classPageID').html(data);}
    });
    return false;
});

$(document).on('click','.sort',function(e){                
    var sort = $('#ordertype').val();
    if (sort == 'desc') {
        $('#ordertype').val('asc');
    }else{
        $('#ordertype').val('desc');
    }
    $('#orderby').val($(this).attr('name'));
    pageid();
});

function saveAsExcel(id, fileName){
    var table_text="<table border='2px'><tr>"; //Table Intialization, CSS included
    var textRange; 
    var index=0; 
    var table = document.getElementById(id); // Read table using id
    /*
        Read Table Data and append to table_text
    */
    
    for(index = 0 ; index < table.rows.length ; index++) 
      {     
            table_text=table_text+table.rows[index].innerHTML+"</tr>";
            
      }

      table_text=table_text+"</table>"; // table close
      table_text= table_text.replace(/<a[^>]*>|<\/a>/g, ""); //removes links embedded in <td>
      table_text= table_text.replace(/<img[^>]*>/gi,"");  //removes images embeded in <td>
      table_text= table_text.replace(/<input[^>]*>|<\/input>/gi, ""); //removes input tag elements

      var userAgent = window.navigator.userAgent; //check client user agent to determine browser
      var msie = userAgent.indexOf("MSIE "); // If it is Internet Explorer user Aget will have string MSIE
      
     if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
      {
          //Since IE > 10 supports blob, check for blob support and use if we can
      if (typeof Blob !== "undefined") {
            //Bolb Data is ArrayStorage, convert to array
            table_text = [table_text];
            var blob = new Blob(table_text);
            window.navigator.msSaveBlob(blob, ''+fileName);
        }
        else{
            //If Blob is unsupported, create an iframe in HTML Page, and call that blank iframe
            
            textArea.document.open("text/html", "replace");
            textArea.document.write(table_text);
            textArea.document.close();
            textArea.focus();
            textArea.document.execCommand("SaveAs", true, fileName); 

        }
      }
      
        //Other Browsers         
       else  
           //Can use below statement if client machine has Excel Application installed
           //window.open('data:application/vnd.ms-excel,' + encodeURIComponent(table_text));  
           var a = document.createElement('a');
            //getting data from our div that contains the HTML table
            var data_type = 'data:application/vnd.ms-excel';
            var table_div = document.getElementById(id);
            var table_html = table_div.outerHTML.replace(/ /g, '%20');
            table_html = table_html.replace(/<a[^>]*>|<\/a>/g, "");
            a.href = data_type + ', ' + table_html;

    //setting the file name
    a.download = ''+fileName;
    //triggering the function
    a.click();
}

function DropDownlist(name,table_id,table,appendtolist,parameter1) {
    var id = $('#'+name+' :selected').attr( "id" );
    $.ajax({
            url     :"{{url('AjaxDropDownList?')}}",
            data    :{id:id,'table_id':table_id,table:table,appendtolist:appendtolist},
            dataType:'json',
            type    :'get',
            success : function(data){
                //$('.classComplaint').html(data);
                //alert(appendtolist);
                var $option = $('#'+appendtolist);
                $option.empty();
                $option.append('<option value="" selected>Select</option>');
                $.each(data, function(index, value) {
                    
                    $option.append('<option name="'+value[parameter1]+'" id="'+value.id+'">'+value.name+'</option>');
                });
                $('#'+appendtolist).trigger('chosen:updated');
            }
    });
    return false;
};

var config = {
  '.chosen-select'           : {},
  '.chosen-select-deselect'  : {allow_single_deselect:true},
  '.chosen-select-no-single' : {disable_search_threshold:10},
  '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
  '.chosen-select-width'     : {width:"95%"}
}
for (var selector in config) {
  $(selector).chosen(config[selector]);
}

$('#ShowEdit').on('show.bs.modal', function (event) {
    var button  = $(event.relatedTarget) // Button that triggered the modal
    var linkto  = button.data('link') // Extract info from data-* attributes
    var employee= button.data('employee') // Extract info from data-* attributes
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this)
    modal.find('.modal-title').text(employee)
    document.getElementById("myFrame").src = linkto;
    //modal.find('iframe').setAttribute("src", linkto)
    //modal.find('iframe').attr( "src", linkto );
})

$(document).on('click','.destroy',function(){ 
    var route   = $(this).data('route');
    var token   = $(this).data('token');
    $.confirm({
        icon                : 'glyphicon glyphicon-floppy-remove',
        animation           : 'rotateX',
        closeAnimation      : 'rotateXR',
        title               : 'OOps Delete Action',
        autoClose           : 'cancel|6000',
        content             : 'Are you sure you want to delete!',
        confirmButtonClass  : 'btn-danger',
        cancelButtonClass   : 'btn-primary',
        confirmButton       : 'Yes i agree',
        cancelButton        : 'NO never !',
        confirm: function () {
            $.ajax({
                url     : route,
                type    : 'post',
                data    : {_method: 'delete', _token :token},
                dataType:'json',           
                success : function(data){
                $("#"+data).parents("tr").remove();   
                }
            });
        },
    });
});

$(document).on('click','.Show',function(){
    var route   = $(this).data('route');
    //var title   = $(this).data('title');
    $.confirm({
        title           : 'show',
        columnClass     : 'col-md-12',
        closeIcon       : true,
        content         : 'url:'+route,
        animation       : 'top',
        closeAnimation  : 'bottom',
        
    });
});

$(".number").keydown(function (e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
         // Allow: Ctrl+A, Command+A
        (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
         // Allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40)) {
             // let it happen, don't do anything
             return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});

function limitText(field, maxChar)
{
    //
    $(field).attr('maxlength',maxChar);
}